<?php 
switch(isset($control[1])?$control[1]:"") {
    case "meta":
        include('meta.html');
        break; 
    case "body":
        include('body.html');
        break; 
    case "config_module_style":
        include('config_module_style.html');
        break;
    case "header":
        include('header/header.html');
        break;
    case "footer":
        include('footer/footer.html');
        break;    
    default:
        include('build.html');
        break;
}
?>