
var modules = {
  ui: function(){
      gotolist();
      $('#addBannerModal').on('shown.bs.modal', function () {
          selectolist();
        });
      // 關閉modal，清空表單內容
      $('#addBannerModal').on('hidden.bs.modal', function (event) {
          $("#addBannerForm")[0].reset();
          $("#preview_a_source").attr('src','https://via.placeholder.com/240x60/FFFFFF/000000?text=預覽圖片');
          $("#preview_e_source").attr('src','https://via.placeholder.com/240x60/FFFFFF/000000?text=預覽圖片');
      });
      $('#editBannerModal').on('hidden.bs.modal', function (event) {
          $("#editBannerForm")[0].reset();
          $("#preview_a_source").attr('src','https://via.placeholder.com/280x160/FFFFFF/000000?text=預覽圖片');
          $("#preview_e_source").attr('src','https://via.placeholder.com/280x160/FFFFFF/000000?text=預覽圖片');
      });
      $("#addBannerBT").click(function(e){
          e.preventDefault();
          $("#addBannerBT").button('loading');
            $('#addBannerModal').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
            $.ajax({
                type: 'POST',
                url: 'route/controller.php?action=addActBanner',
                data: new FormData($("#addBannerForm")[0]),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData:false
            }).complete(function(data) {
//                    console.log(JSON.stringify(data.responseText.replace(' ','')));
                $( "#loadingDiv" ).fadeOut(500, function() {
                      $( "#loadingDiv" ).remove(); //makes page more lightweight
                });
                switch(data.responseJSON.message){
                    case "failed":
                      swal({
                            title: "圖片資料上傳失敗！",
                            text: "Oh...NO!"+data.responseJSON.errmsg,
                            icon: "error",
                            default: true
                        });
                    break;
                    case "success": 
                      swal({
                            title: "圖片資料上傳完成!",
                            text: "Good Job!"+data.responseJSON.errmsg,
                            icon: "success",
                            default: true
                        }).then((value) => {                            
                            $("#addBannerForm")[0].reset();
                          $('#addBannerModal').modal('hide'); $("#preview_a_source").attr('src','https://via.placeholder.com/240x60/FFFFFF/000000?text=預覽圖片');
                        }); 
                        $('#bannerListTable').DataTable().destroy();
                        gotolist();
                    break;
                }

              });
      });
      
      
      $("#editBannerBT").click(function(event) {
        saveEvent();
      });
  }
};
function selectolist(){
    var str="";
    $("#addBannerBT").button('loading');
    $("#a_actname").html("");
    $.post('route/controller.php?action=getActList', function(data, textStatus, xhr) {
              switch(data.message){
                case "failed":
//                  alert("資料存取有誤，請聯繫管理員!");
                    swal({
                        title: "注意！",
                        text: "Oh...NO!資料存取有誤，請聯繫管理員!",
                        icon: "error",
                        default: true
                    });
                break;
                case "success":   
                  postData=data.data; 
                    $('#a_actname').append($("<option></option>").attr("value",'-1').text('請選擇'));
                    
                    postData.forEach(function(item){
                      $('#a_actname').append($("<option></option>").attr('data-id',item['SER']).attr("value",item['SER']).text(item['ACTTITLE']));
                    });
                    
                break;
                default:
                break;
              }                       
        },"json").done(function() {
        $("#addBannerBT").button('reset');
      });
}
function gotolist(){
    if ( $.fn.dataTable.isDataTable( '#bannerListTable' ) ) {
        $('#bannerListTable').DataTable({"bProcessing": false});
    }
    else
    {
        $('#bannerListTable').DataTable({
            "bProcessing": true,
            "sAjaxSource": "route/controller.php?action=getActBannerList",
            "aoColumns": [
                {"mData": "ID"},
                { "mData": "IMGURL",
                    "mRender":function(val,type,row){
                        return "<img class='img-thumbnail' style='max-width: 160px;max-height: 40px;' src='" + val+ "?"+(new Date()).getTime()+"' data-src='" + val+ "?"+(new Date()).getTime()+"'/>";
                    }
                },
                { "mData": "ACTTITLE"},
                { "mData": "ACTTYPE",
                    "mRender":function(val,type,row){
                    switch(val)
                    {
                        case '0':
                            return '<span class="label label-online">線上</span>';
                            break;
                        case '1':
                            return '<span class="label label-offline">線下</span>';
                            break;
                        default:
                            return false;
                            break;
                    }
                }},
                { "mData": "ACTSTARTDATE"},
                { "mData": "ACTENDDATE"},
                { "mData": "ID" ,
                      "mRender":function(val,type,row) {
                        return "<button class='btn btn-warning btn-circle' data-toggle='modal' data-target='#editBannerModal' onclick='loadEvent(\""+val.toString()+"\")'><span class='fa fa-pencil' aria-hidden='true'></span></button><button class='btn btn-danger btn-circle' onclick='deleteEvent(\""+val.toString()+"\", $(this))'><span class='fa fa-trash' aria-hidden='true'></span></button>";
                      }
                }
            ],
            bAutoWidth: false , 
            "order": [[ 0, "asc" ],[ 1, "asc" ]],
            "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                $("td:first", nRow).html(iDisplayIndex +1);
                $(nRow).attr("data-id",aData.SER).attr('data-sid',aData.S_ID);
                return nRow;
            },
            "language": {
                "processing": "資料載入中...",
                "lengthMenu": "每頁顯示 _MENU_ 筆",
                "zeroRecords": "資料庫中未有相關資料。",
                "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                "infoEmpty": "資料庫中未有相關資料。",
                "search":  "搜尋:",
                "paginate": {
                      "first":      "第一頁",
                      "last":       "最後一頁",
                      "next":       "下一頁",
                      "previous":   "上一頁"
                  }
            }
        });
        
    }
    
    
} 
function loadEvent(_id){
    $("#editBannerBT").button('loading');
  var dataAry = {
      ser:_id
    };
    $.post('route/controller.php?action=loadActBanner', dataAry, function(data, textStatus, xhr) {
            postData=data.data;
            switch(data.message){
              case "failed":
//                alert(data.errmsg);
                swal({
                    title: "圖片資料載入失敗！",
                    text: "Oh...NO!"+data.errmsg,
                    icon: "error",
                    default: true
                });
              break;
              case "success":   
                $("#e_actname").text(postData[0].ACTTITLE);
                $("#e_ser").val(postData[0].SER);
                $("#e_actser").val(postData[0].S_SER);
                $("#preview_e_source").attr('src', postData[0].IMGURL);
              break;
              default:
              break;
            }
      },"json").done(function() {
        $("#editBannerBT").button('reset');
      });
}
function saveEvent(){
    $("#editBannerBT").button('loading');
    $('#editBannerModal').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
  $.ajax({
        type: 'POST',
        url: 'route/controller.php?action=editActBanner',
        data: new FormData($("#editBannerForm")[0]),
        dataType: 'json',
        contentType: false,
        cache: false,
        processData:false
    }).complete(function(data) {
//                console.log(JSON.stringify(data.responseText.replace(' ','')));
        $( "#loadingDiv" ).fadeOut(500, function() {
              $( "#loadingDiv" ).remove(); //makes page more lightweight
        });
        switch(data.responseJSON.message){
            case "failed":
              swal({
                    title: "圖片資料修改失敗！",
                    text: "Oh...NO!"+data.responseJSON.errmsg,
                    icon: "error",
                    default: true
                }).then((value) => {                            
                    $("#editBannerForm")[0].reset();

                });
            break;
            case "success": 
              swal({
                    title: "圖片資料修改完成!",
                    text: "Good Job!"+data.responseJSON.errmsg,
                    icon: "success",
                    default: true
                }).then((value) => {                            
                    $("#editBannerForm")[0].reset();
                  $('#editBannerModal').modal('hide'); $("#preview_e_source").attr('src','https://via.placeholder.com/240x60/FFFFFF/000000?text=預覽圖片');
                }); 
                $('#bannerListTable').DataTable().destroy();
                gotolist();
            break;
        }

      });
}
function deleteEvent(_id,obj){
  if(confirm("確定要刪除嗎?不能後悔喔!"))
  {
    var dataAry = {
      ser:_id,
        s_ser:obj.parent().parent().attr('data-sid')
    };
    $.post('route/controller.php?action=deleteActBanner', dataAry, function(data, textStatus, xhr) {
            switch(data.message){
              case "failed":
                swal({
                    title: "圖片資料刪除失敗！",
                    text: "Oh...NO!",
                    icon: "error",
                    default: true
                }).then((value) => {                            
                    gotolist(); 

                });
              break;
              case "success":   
                swal({
                    title: "圖片資料刪除完成!",
                    text: "Good Job!",
                    icon: "success",
                    default: true
                }).then((value) => {                            
                       
                    $('#bannerListTable').DataTable().destroy();
                    gotolist(); 
                }); 
                 
              break;
              default:
              break;
            }
      },"json");
  }
}
function clearEvent(){
  if(confirm("確定要清空嗎?不能後悔喔!"))
  {
    $.post('route/controller.php?action=clearproductImage', "", function(data, textStatus, xhr) {
            switch(data.message){
              case "failed":
                alert(data.errmsg);
              break;
              case "success":   
                alert(data.errmsg);     
                $('#pImageTable').DataTable().destroy(); 
                gotolist();
              break;
              default:
              break;
            }
      },"json");
  }
}
function previewEvent(obj){
    if(obj[0].files && obj[0].files[0]){
        var readerimg = new FileReader();
        var img = new Image();
        var w=0,h=0,s=0;
        readerimg.onload = function (e) {
            img.src = e.target.result
            s = e.total;
        }
        
        readerimg.readAsDataURL(obj[0].files[0]);
        
        img.onload = function() {
            w = this.width;
            h = this.height;
            
            if((w/h) <= 4 && (w/h) >= 1 && w >= 1600 && h >=400)
            {
                $("#preview_a_source").attr('src', img.src);
                $("#preview_e_source").attr('src', img.src);
            }
            else 
            {            
                swal({
                    title: "圖片長寬(長："+w+",寬："+h+")不符，請重新上傳。",
                    text: "Oh...Sorry!",
                    icon: "error",
                    default: true
                });
                obj.replaceWith(obj.val('').clone(true));
            }
        }
        
      }
}
$(function(){
//    $('body').append('<div id="loadingDivfullS"><div class="loader">Loading...</div></div>');
//    setInterval(function(){ 
//        $( "#loadingDivfullS" ).fadeOut(500, function() {
//              $( "#loadingDivfullS" ).remove(); //makes page more lightweight 
//        }); 
//    }, 1500);
    modules.ui();
    
})

