var _id = localStorage.getItem("_id");
var modulesFunction = {
    ui: function(){        
        $('[data-toggle="tooltip"]').tooltip();
        modulesFunction.inputnumber();
         modulesFunction.gotolist();
        // 活動時間起迄
        $('#activityStart, #e-activityStart').datetimepicker({
            useCurrent: false,
            format: 'YYYY-MM-DD'
        });
        $('#activityFinish, #e-activityFinish').datetimepicker({
            useCurrent: false, //Important! See issue #1075
            format: 'YYYY-MM-DD'
        });
        $("#activityStart, #e-activityStart").on("dp.change", function (e) {
            $('#activityFinish').data("DateTimePicker").minDate(e.date);
            modulesFunction.validateEvent($(this));
        });
        $("#activityFinish, #e-activityFinish").on("dp.change", function (e) {
            $('#activityStart').data("DateTimePicker").maxDate(e.date);
            modulesFunction.validateEvent($(this));
        });
        //兌換時間起迄
        $('#redemptionStart, #e-redemptionStart').datetimepicker({
            useCurrent: false,
            format: 'YYYY-MM-DD'
        });
        $('#redemptionFinish, #e-redemptionFinish').datetimepicker({
            useCurrent: false, //Important! See issue #1075
            format: 'YYYY-MM-DD'
        });
        $("#redemptionStart, #e-redemptionStart").on("dp.change", function (e) {
            $('#redemptionFinish').data("DateTimePicker").minDate(e.date);
            modulesFunction.validateEvent($(this));
        });
        $("#redemptionFinish, #e-redemptionFinish").on("dp.change", function (e) {
            $('#redemptionStart').data("DateTimePicker").maxDate(e.date);
            modulesFunction.validateEvent($(this));
        });
        
        
        $('#addPrizeModal').on('hidden.bs.modal', function() {
            $('#addPrizeForm')[0].reset();
            $('#addPrizeForm .form-group').removeClass('has-error has-feedback');
            $('#addPrizeForm').find('span.text-danger').remove();
            $('#prizeContent').html("");
        });
        $('#editPrizeModal').on('hidden.bs.modal', function() {
            $('#editPrizeForm')[0].reset();
            $('#editPrizeForm .form-group').removeClass('has-error has-feedback');
            $('#editPrizeForm').find('span.text-danger').remove();
            $('#e-prizeContent').html("");
        });
        modulesFunction.checkInputRequired();
        $("#makeprize").click(function(){
            modulesFunction.makePrizeEvent();            
        });
        $("#e-makeprize").click(function(){
            modulesFunction.e_makePrizeEvent();
        });
        $('#actListTable').DataTable();
        $("#AddActivityBT").click(function(e){
            $("#addPrizeForm input[required]").each(function(){
                modulesFunction.validateEvent($(this));
            });
            if($("#addPrizeForm .form-group").hasClass('has-error has-feedback'))
            {
                return false;
            }
            //chcek 配置比例滿100%
            var a_percentSum = 0;
            for (var i = 0; i < $('#addPrizeForm .percent').length ; i++) {
                a_percentSum+=parseInt($('#addPrizeForm .percent')[i].value);
            }
            if(a_percentSum != 100)
            {
                swal({
                    title: "設定總比例須滿足100%。",
                    icon: "warning",
                    default: true
                });
                return false;
            }
            e.preventDefault();
            if($("#prizeContent").html())
            {
                modulesFunction.addEvent();
            }
            else
            {
                $("#prizenum").val(0);
                swal({
                  title: "注意！",
                  text: "獎品資料尚未新增，確定儲存？(後續可於編輯新增獎品資料)",
                  icon: "warning",
                  buttons: {
                      cancel: {
                        text: "取消",                          
                        value: null
                      },
                      confirm: {
                        text: "確定",                          
                        value: true
                      }

                  },
                })
                .then((isconfirmed) => {
                  if (isconfirmed) {
                    modulesFunction.addEvent();
                  } else {
                    return false;
                  }
                });
            }
            
          });
        $("#SaveActivityBT").click(function(e){
            $("#editPrizeForm input[required]").each(function(){
                modulesFunction.validateEvent($(this));
            });
            if($("#editPrizeForm .form-group").hasClass('has-error has-feedback'))
            {
                return false;
            }
            //chcek 配置比例滿100%
            var e_percentSum = 0;
            for (var i = 0; i < $('#editPrizeForm .percent').length ; i++) {
                e_percentSum+=parseInt($('#editPrizeForm .percent')[i].value);
            }
            if(e_percentSum != 100)
            {
                swal({
                    title: "設定總比例須滿足100%。",
                    icon: "warning",
                    default: true
                });
                return false;
            }
            e.preventDefault();
            if($("#e-prizeContent").html())
            {
                modulesFunction.saveEvent();
            }
            else
            {
                $("#e-prizenum").val(0);
                swal({
                  title: "注意！",
                  text: "獎品資料尚未新增，確定儲存？(後續可於編輯新增獎品資料)",
                  icon: "warning",
                  buttons: {
                      cancel: {
                        text: "取消",                          
                        value: null
                      },
                      confirm: {
                        text: "確定",                          
                        value: true
                      }

                  },
                })
                .then((isconfirmed) => {
                  if (isconfirmed) {
                    modulesFunction.saveEvent();
                  } else {
                    return false;
                  }
                });
            }
            
          });
    },
    gotolist: function(){
       if ( $.fn.dataTable.isDataTable( '#actListTable' ) ) {
            $('#actListTable').DataTable();
        }
        else
        {
            $('#actListTable').DataTable({
                "bProcessing": true,
                "sAjaxSource": "route/controller.php?action=getActList",
                "aoColumns": [
                    { "mData": "ACTSTATUS", "sWidth": '5%',
                        "mRender":function(val,type,row){
                            switch(val)
                            {
                                case '0':
                                    return '<span class="label label-info">尚未進行</span>';
                                    break;
                                case '1':
                                    return '<span class="label label-success">進行中</span>';
                                    break;
                                case '2':
                                    return '<span class="label label-default">已結束</span>';
                                    break;
                                default:
                                    return false;
                                    break;
                            }
                        }},
                    { "mData": "ACTTITLE", "sWidth": '20%'},
                    { "mData": "ACTTYPE", "sWidth": '5%',
                        "mRender":function(val,type,row){
                            switch(val)
                            {
                                case '0':
                                    return '<span class="label label-online">線上</span>';
                                    break;
                                case '1':
                                    return '<span class="label label-offline">線下</span>';
                                    break;
                                default:
                                    return false;
                                    break;
                            }
                        }},
                    { "mData": "ACTSTARTDATE", "sWidth": '10%'},
                    { "mData": "ACTENDDATE", "sWidth": '10%'},
                    { "mData": "AWARDSTARTDATE", "sWidth": '10%'},
                    { "mData": "AWARDENDDATE", "sWidth": '10%'},
                    { "mData": "ITEMS", "sWidth": '5%'},
                    { "mData": "SER", "sWidth": '15%' ,
                          "mRender":function(val,type,row) {
                            return "<button class='btn btn-info btn-circle simptip-position-top' data-toggle='modal' data-target='#infoPrizeModal' data-tooltip='資訊' onclick='modulesFunction.infoEvent(\""+val.toString()+"\")'><span class='fa fa-info-circle' aria-hidden='true'></span></button><button class='btn btn-default btn-circle simptip-position-top' data-tooltip='複製' onclick='modulesFunction.duplicateEvent(\""+val.toString()+"\")'><span class='fa fa-files-o' aria-hidden='true'></span></button><button class='btn btn-warning btn-circle simptip-position-top' data-toggle='modal' data-target='#editPrizeModal' data-tooltip='編輯' onclick='modulesFunction.loadEvent(\""+val.toString()+"\")'><span class='fa fa-pencil' aria-hidden='true'></span></button><button class='btn btn-danger btn-circle simptip-position-top' data-tooltip='刪除' onclick='modulesFunction.deleteEvent(\""+val.toString()+"\")'><span class='fa fa-trash' aria-hidden='true'></span></button><a href='#' class='btn btn-default' onclick='modulesFunction.setpagelink($(this))' target='_blank' >前往活動頁面</a>";
                          }
                    }
                ],
                "order": [[ 0, "desc" ]],
                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {

                    $(nRow).attr("data-id",aData.STORECODE);
                    $(nRow).attr("data-actser",aData.SER);
                    return nRow;
                },
                "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search":  "搜尋:",
                    "paginate": {
                          "first":      "第一頁",
                          "last":       "最後一頁",
                          "next":       "下一頁",
                          "previous":   "上一頁"
                      }
                }
            });
        }
    },
    setpagelink: function(obj)
    {
        var objTr = obj.parent().parent();
        var actser = objTr.attr('data-actser');
        
//        window.location.href = './FBGame?param1=FBGame&actser=' + actser;
        obj.attr("href",'./FBGame?param1=FBGame&actser=' + actser + "&timestamp=" + new Date().getTime() + "&UID=" + modulesFunction.randomString(30));
        
    },
    addEvent: function(){
        $("#AddActivityBT").button('loading');
        $('addPrizeModal').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
        $.ajax({
            type: 'POST',
            url: 'route/controller.php?action=addAct',
            data: new FormData($("#addPrizeForm")[0]),
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false
        }).complete(function(data) {
//                console.log(data);
//                console.log(JSON.stringify(data.responseText.replace(' ','')));
            $( "#loadingDiv" ).fadeOut(500, function() {
                  $( "#loadingDiv" ).remove(); //makes page more lightweight
            });
            switch(data.responseJSON.message){
                case "failed":
                  swal({
                        title: "新增失敗！",
                        text: "Oh...NO!"+data.responseJSON.errmsg,
                        icon: "error",
                        default: true
                    });
                break;
                case "success": 
                  swal({
                        title: "新增完成!",
                        text: "Good Job!"+data.responseJSON.errmsg,
                        icon: "success",
                        default: true
                    }).then((value) => {                            
                        $("#addPrizeForm")[0].reset();
                      $('#addPrizeModal').modal('hide'); $('#prizeContent').html("");
                    }); 
                    $('#actListTable').DataTable().destroy();
                    modulesFunction.gotolist();
                break;
            }

          });
    },
    loadEvent: function(_id){
      $("#SaveActivityBT").button('loading');
      var dataAry = {
          ser:_id
        };
        $.post('route/controller.php?action=loadAct', dataAry, function(data, textStatus, xhr) {
                postData=data.data;
                switch(data.message){
                  case "failed":
    //                alert(data.errmsg);
                    swal({
                        title: "資料載入失敗！",
                        text: "Oh...NO!"+data.errmsg,
                        icon: "error",
                        default: true
                    });
                  break;
                  case "success":   
                    $("#e-actid").val(postData[0].actSER);
                    switch(postData[0].ACTSTATUS)
                    {
                        case "0":
                            $("#e-actstatus1").attr("checked", true);
                            break;
                        case "1":
                            $("#e-actstatus2").attr("checked", true);
                            break;
                        case "2":
                            $("#e-actstatus3").attr("checked", true);
                            break;
                    }
                    $("#e-acttitle").val(postData[0].ACTTITLE);
                    switch(postData[0].ACTTYPE)
                    {
                        case "0":
                            $("#e-acttype1").attr("checked", true);
                            break;
                        case "1":
                            $("#e-acttype2").attr("checked", true);
                            break;
                    }
                    $("#e-activityStart").val(postData[0].ACTSTARTDATE);
                    $("#e-activityFinish").val(postData[0].ACTENDDATE);
                    $("#e-activitydesp").val(postData[0].ACTDESP);
                    $("#e-redemptionStart").val(postData[0].AWARDSTARTDATE);
                    $("#e-redemptionFinish").val(postData[0].AWARDENDDATE);
                    $("#e-redempdesp").val(postData[0].AWARDDESP);
                    $("#e-prizenum").val(parseInt(postData[0].ITEMS));
                    switch(postData[0].NOPRIZE)
                    {
                        case "0":
                            $("#e-noprize1").attr("checked", true);
                            break;
                        case "1":
                            $("#e-noprize2").attr("checked", true);
                            break;
                    }
                    
                    if(parseInt(postData[0].ITEMS) != 0)
                    {
                        $poststr = '';
                        for(var i=0 ; i < postData.length ; i++)
                        {
                            if(postData[0].NOPRIZE == "0")
                            {
                                $poststr += '<div class="form-group"> <label class="control-label col-md-2">'+(i+1)+'<input type="hidden" name="prizeid-'+(i+1)+'" value="'+postData[i].prizeSER+'"/></label> <div class="col-md-2"> <img id="preview_image" src="'+postData[i].IMGURL+'" class="img-thumbnail preview_image-'+(i+1)+'" style="max-width: 120px;max-height: 100px;"> </div> <div class="col-md-8"> <div class="row"> <div class="col-md-6"> <input type="file" class="form-control prize-'+(i+1)+'" name="prize-'+(i+1)+'" id="prize-'+(i+1)+'" accept="image/jpeg,image/gif,image/png" onchange="modulesFunction.previewEvent($(this));" required></div> <div class="col-md-6"> <span class="label label-danger">建議圖片最小為300*300正方形；大小<=500k。</span> </div> </div> <div class="row"> <div class="col-md-6"> <input type="text" class="form-control prizename-'+(i+1)+'" name="prizename-'+(i+1)+'" placeholder="獎品名稱" data-toggle="tooltip" data-placement="bottom" title="獎品名稱，中獎時顯示的獎品內容(客戶看)" value="'+postData[i].PRIZENAME+'" required> <div class="checkbox '+((i == (postData.length-1))?'hidden':'')+'"><label><input type="checkbox" name="showwintxt-'+(i+1)+'" '+((postData[i].HASWINTEXT == '0')?'checked':'')+'> 恭喜中獎(預設) </label> </div> </div> <div class="col-md-3"><input type="number" class="form-control prizenum-'+(i+1)+'" name="prizenum-'+(i+1)+'" min="0" placeholder="數量" data-toggle="tooltip" data-placement="bottom" title="該獎品總數量(廠商看)" value="'+parseInt(postData[i].TOTALS)+'" required> </div> <div class="col-md-3"> <div class="input-group"> <input type="number" class="form-control percent prizepercent-'+(i+1)+'" name="prizepercent-'+(i+1)+'" min="0" placeholder="佔比例" data-toggle="tooltip" data-placement="bottom" title="設定抽獎比例(廠商看)" onchange="modulesFunction.calcPercent()" value="'+parseInt(postData[i].PERCENT)+'" required><span class="input-group-addon">%</span> </div> </div> </div> </div></div>';
                            }
                            else
                            {
                                $poststr += '<div class="form-group"> <label class="control-label col-md-2">'+(i+1)+'<input type="hidden" name="prizeid-'+(i+1)+'" value="'+postData[i].prizeSER+'"/></label> <div class="col-md-2"> <img id="preview_image" src="'+postData[i].IMGURL+'" class="img-thumbnail preview_image-'+(i+1)+'" style="max-width: 120px;max-height: 100px;"> </div> <div class="col-md-8"> <div class="row"> <div class="col-md-6"> <input type="file" class="form-control prize-'+(i+1)+'" name="prize-'+(i+1)+'" id="prize-'+(i+1)+'" accept="image/jpeg,image/gif,image/png" onchange="modulesFunction.previewEvent($(this));" required></div> <div class="col-md-6"> <span class="label label-danger">建議圖片最小為300*300正方形；大小<=500k。</span> </div> </div> <div class="row"> <div class="col-md-6"> <input type="text" class="form-control prizename-'+(i+1)+'" name="prizename-'+(i+1)+'" placeholder="獎品名稱" data-toggle="tooltip" data-placement="bottom" title="獎品名稱，中獎時顯示的獎品內容(客戶看)" value="'+postData[i].PRIZENAME+'" required> <div class="checkbox "><label><input type="checkbox" name="showwintxt-'+(i+1)+'" '+((postData[i].HASWINTEXT == '0')?'checked':'')+'> 恭喜中獎(預設) </label> </div> </div> <div class="col-md-3"><input type="number" class="form-control prizenum-'+(i+1)+'" name="prizenum-'+(i+1)+'" min="0" placeholder="數量" data-toggle="tooltip" data-placement="bottom" title="該獎品總數量(廠商看)" value="'+parseInt(postData[i].TOTALS)+'" required> </div> <div class="col-md-3"> <div class="input-group"> <input type="number" class="form-control percent prizepercent-'+(i+1)+'" name="prizepercent-'+(i+1)+'" min="0" placeholder="佔比例" data-toggle="tooltip" data-placement="bottom" title="設定抽獎比例(廠商看)" onchange="modulesFunction.calcPercent()" value="'+parseInt(postData[i].PERCENT)+'" required><span class="input-group-addon">%</span> </div> </div> </div> </div></div>';
                            }                        
                        }
                        $poststr += '<div class="form-group"><div class="col-md-3"></div><div class="col-md-5"></div><div class="col-md-2"></div><div class="col-md-2"><span class="text-danger text-right calcPercentText"></span></div></div></div>';
                        $("#e-prizeContent").html($poststr);
                        modulesFunction.inputnumber();
                    }
                  break;
                  default:
                  break;
                }
          },"json").done(function() {
            $("#SaveActivityBT").button('reset');
          });  
    },
    saveEvent: function(_id){
        $("#SaveActivityBT").button('loading');
        $('editPrizeModal').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
        $.ajax({
            type: 'POST',
            url: 'route/controller.php?action=editAct',
            data: new FormData($("#editPrizeForm")[0]),
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false
        }).complete(function(data) {
//                console.log(data);
//                console.log(JSON.stringify(data.responseText.replace(' ','')));
            $( "#loadingDiv" ).fadeOut(500, function() {
                  $( "#loadingDiv" ).remove(); //makes page more lightweight 
            });
            switch(data.responseJSON.message){
                case "failed":
                  swal({
                        title: "儲存失敗！",
                        text: "Oh...NO!"+data.responseJSON.errmsg,
                        icon: "error",
                        default: true
                    });
                break;
                case "success": 
                  swal({
                        title: "儲存成功!",
                        text: "Good Job!"+data.responseJSON.errmsg,
                        icon: "success",
                        default: true
                    }).then((value) => {                            
                        $("#editPrizeForm")[0].reset();
                      $('#editPrizeModal').modal('hide'); $('#e-prizeContent').html("");
                    }); 
                    $('#actListTable').DataTable().destroy();
                    modulesFunction.gotolist();
                break;
            }
            $("#SaveActivityBT").button('reset');
          });
    },
    infoEvent: function(_id){
    $("#i-prizetbody").html("");
    $('body').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
      var dataAry = {
          ser:_id
        };
        $.post('route/controller.php?action=loadAct', dataAry, function(data, textStatus, xhr) {
                postData=data.data;
                switch(data.message){
                  case "failed":
    //                alert(data.errmsg);
                    swal({
                        title: "資料載入失敗！",
                        text: "Oh...NO!"+data.errmsg,
                        icon: "error",
                        default: true
                    });
                  break;
                  case "success":   
                    $("#i-actstatus").removeClass('label label-info label-success label-default');
                    switch(postData[0].ACTSTATUS)
                    {
                        case "0":
                            $("#i-actstatus").text("尚未進行").addClass('label label-info');
                            break;
                        case "1":
                            $("#i-actstatus").text("進行中").addClass('label label-success');
                            break;
                        case "2":
                            $("#i-actstatus").text("已結束").addClass('label label-default');
                            break;
                        default:
                            $("#i-actstatus").text("資料有誤！");
                            break;
                    }
                    $("#i-acttitle").text(postData[0].ACTTITLE);
                    $("#i-acttype").removeClass('label label-online label-offline');
                    switch(postData[0].ACTTYPE)
                    {
                        case "0":
                            $("#i-acttype").text("線上").addClass('label label-online');
                            break;
                        case "1":
                            $("#i-acttype").text("線下").addClass('label label-offline');
                            break;
                        default:
                            $("#i-acttype").text("資料有誤！");
                            break;
                    }
                    $("#i-activityStart").text(postData[0].ACTSTARTDATE);
                    $("#i-activityFinish").text(postData[0].ACTENDDATE);
                    $("#i-activitydesp").html(postData[0].ACTDESP.replace(/\r\n|\n|\r/g,"<br/>"));
                    $("#i-redemptionStart").text(postData[0].AWARDSTARTDATE);
                    $("#i-redemptionFinish").text(postData[0].AWARDENDDATE);
                    $("#i-redempdesp").html(postData[0].AWARDDESP.replace(/\r\n|\n|\r/g,"<br/>"));
                    $("#i-prizenum").text(parseInt(postData[0].ITEMS));
                    switch(postData[0].NOPRIZE)
                    {
                        case "0":
                            $("#i-noprize1").attr("checked", true);
                            break;
                        case "1":
                            $("#i-noprize2").attr("checked", true);
                            break;
                    }
                    
                    if(parseInt(postData[0].ITEMS) != 0)
                    {
                        $poststr = '';
                        for(var i=0 ; i < postData.length ; i++)
                        {
                            $poststr += '<tr>';
                            $poststr += '<td>'+(i+1)+'</td>'
                            $poststr += '<td><img src="'+postData[i].IMGURL+'" class="img-thumbnail" style="max-width: 120px;max-height: 120px;"/></td>';
                            $poststr += '<td>'+postData[i].PRIZENAME+'</td>';
                            $poststr += '<td>'+parseInt(postData[i].TOTALS)+'</td>';
                            $poststr += '<td>'+parseInt(postData[i].PERCENT)+'%</td>';      
                        }
                        $("#i-prizetbody").html($poststr);
                    }
                  break;
                  default:
                  break;
                }
          },"json").done(function() {
            $( "#loadingDiv" ).fadeOut(500, function() {
                  $( "#loadingDiv" ).remove(); //makes page more lightweight 
            });
          });  
    },
    deleteEvent: function(_id){
        swal({
          title: "你確定要刪除嗎?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
                $('body').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
                var dataAry = {
                  ser:_id
                };
                $.post('route/controller.php?action=deleteAct', dataAry, function(data, textStatus, xhr) {
                        $( "#loadingDiv" ).fadeOut(500, function() {
                              $( "#loadingDiv" ).remove(); //makes page more lightweight 
                        });
                        postData=data.data;
                        switch(data.message){
                          case "failed":
            //                alert(data.errmsg);
                            swal({
                                title: "刪除失敗！",
                                text: "Oh...NO!",
                                icon: "error",
                                default: true
                            });
                          break;
                          case "success":   
                            swal({
                                title: "刪除成功!",
                                text: "Good Job!",
                                icon: "success",
                                default: true
                            }); 
                            $('#actListTable').DataTable().destroy();
                            modulesFunction.gotolist();
                          break;
                          default:
                          break;
                        }
                  },"json");  
          }
        });        
    },
    duplicateEvent:function(_id){
        swal({
          title: "你確定要複製該筆活動嗎?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
                $('body').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
                var dataAry = {
                  ser:_id
                };
                $.post('route/controller.php?action=duplicateAct', dataAry, function(data, textStatus, xhr) {
                        $( "#loadingDiv" ).fadeOut(500, function() {
                              $( "#loadingDiv" ).remove(); //makes page more lightweight 
                        });
                        postData=data.data;
                        switch(data.message){
                          case "failed":
            //                alert(data.errmsg);
                            swal({
                                title: data.errmsg,
                                text: "Oh...NO!",
                                icon: "error",
                                default: true
                            });
                          break;
                          case "success":   
                            swal({
                                title: data.errmsg,
                                text: "Good Job!",
                                icon: "success",
                                default: true
                            }); 
                            $('#actListTable').DataTable().destroy();
                            modulesFunction.gotolist();
                          break;
                          default:
                          break;
                        }
                  },"json");  
          }
        });
    },
    calcPercent:function(){  
        var percentSum = 0;
        for (var i = 0; i < $('.percent').length ; i++) {
            percentSum+=parseInt($('.percent')[i].value);
        }
//        console.log(percentSum);
        if(percentSum != 100)
        {
            $('.calcPercentText').text("設定總比例須滿足100%。");
        }
        else
        {
            $('.calcPercentText').text("");
        }
    },
    previewEvent:function(obj){
//        console.log(obj[0].name.split('-')[1]);
//        return false;
        var tempnum = obj[0].name.split('-')[1];
        if(obj[0].files && obj[0].files[0]){
            var readerimg = new FileReader();
            var img = new Image();
            var w=0,h=0,s=0;
            readerimg.onload = function (e) {
                img.src = e.target.result
                s = e.total;
            }

            readerimg.readAsDataURL(obj[0].files[0]);

            img.onload = function() {
                w = this.width;
                h = this.height;

                if((w/h) <= 1 && (w/h) >= 1 && w >= 300 && h >= 300)
                {
                    if((s/500000) <= 1)
                    {
                        $(".preview_image-"+tempnum).attr('src', img.src);
                    }
                    else
                    {
                        swal({
                            title: "圖片大小("+(s/500000)+"MB)已超過1MB，請重新上傳。",
                            text: "Oh...Sorry!",
                            icon: "error",
                            default: true
                        });
                        obj.replaceWith(obj.val('').clone(true));
                    }

                }
                else 
                {            
                    swal({
                        title: "圖片長寬(長："+w+",寬："+h+")不符，請重新上傳。",
                        text: "Oh...Sorry!",
                        icon: "error",
                        default: true
                    });
                    obj.replaceWith(obj.val('').clone(true));
                }
            }

          }
    },
    makePrizeEvent:function(){
        if($("#prizenum").val())
        {
            $prizenum =  parseInt($("#prizenum").val());
            if($prizenum < 1)
            {
                swal({
                    title: "注意！",
                    text: "數量不可小於1項！",
                    icon: "error",
                    default: true
                });
                return false;
            }
              $str = '';

              if($("#noprize1").prop('checked'))
                  {$prizenum-=1;}
              for (var i = 0; i < $prizenum; i++) {
                $str += '<div class="form-group"> <label class="control-label col-md-2">'+(i+1)+'</label> <div class="col-md-2"> <img id="preview_image" src="https://via.placeholder.com/120x100/FFFFFF/000000?text=預覽圖片" class="img-thumbnail preview_image-'+(i+1)+'" style="max-width: 120px;max-height: 100px;"> </div> <div class="col-md-8"> <div class="row"> <div class="col-md-6"> <input type="file" class="form-control prize-'+(i+1)+'" name="prize-'+(i+1)+'" id="prize-'+(i+1)+'" accept="image/jpeg,image/gif,image/png" onchange="modulesFunction.previewEvent($(this));" required></div> <div class="col-md-6"> <span class="label label-danger">建議圖片最小為300*300正方形；大小<=500k。</span> </div> </div> <div class="row"> <div class="col-md-6"> <input type="text" class="form-control prizename-'+(i+1)+'" name="prizename-'+(i+1)+'" placeholder="獎品名稱" data-toggle="tooltip" data-placement="bottom" title="獎品名稱，中獎時顯示的獎品內容(客戶看)" required> <div class="checkbox"><label><input type="checkbox" name="showwintxt-'+(i+1)+'" checked> 恭喜中獎(預設) </label> </div> </div> <div class="col-md-3"><input type="number" class="form-control prizenum-'+(i+1)+'" name="prizenum-'+(i+1)+'" min="0" placeholder="數量" data-toggle="tooltip" data-placement="bottom" title="該獎品總數量(廠商看)" required> </div> <div class="col-md-3"> <div class="input-group"> <input type="number" class="form-control percent prizepercent-'+(i+1)+'" name="prizepercent-'+(i+1)+'" min="0" placeholder="佔比例" data-toggle="tooltip" data-placement="bottom" title="設定抽獎比例(廠商看)" onchange="modulesFunction.calcPercent()" required><span class="input-group-addon">%</span> </div> </div> </div> </div></div>';        
              }
              $str_byebye = '<div class="form-group"> <label class="control-label col-md-2">'+($prizenum+1)+'</label> <div class="col-md-2"> <img id="preview_image" src="https://via.placeholder.com/120x100/FFFFFF/000000?text=預覽圖片" class="img-thumbnail preview_image-'+($prizenum+1)+'" style="max-width: 120px;max-height: 100px;"> </div> <div class="col-md-8"> <div class="row"> <div class="col-md-6"> <input type="file" class="form-control prize-'+($prizenum+1)+'" name="prize-'+($prizenum+1)+'" id="prize-'+($prizenum+1)+'" accept="image/jpeg,image/gif,image/png" onchange="modulesFunction.previewEvent($(this));" required></div> <div class="col-md-6"> <span class="label label-danger">建議圖片最小為300*300正方形；大小<=500k。</span> </div> </div> <div class="row"> <div class="col-md-6"> <input type="text" class="form-control prizename-'+($prizenum+1)+'" name="prizename-'+($prizenum+1)+'" placeholder="獎品名稱" data-toggle="tooltip" data-placement="bottom" title="可更改文字或放置連結(客戶看)" value="銘謝惠顧" required> <div class="checkbox hidden"><label><input type="checkbox" name="showwintxt-'+($prizenum+1)+'"> 恭喜中獎(預設) </label> </div> </div> <div class="col-md-3"><input type="number" class="form-control prizenum-'+($prizenum+1)+'" name="prizenum-'+($prizenum+1)+'" min="0" placeholder="數量" data-toggle="tooltip" data-placement="bottom" title="該獎品總數量(廠商看)" required> </div> <div class="col-md-3"> <div class="input-group"> <input type="number" class="form-control percent prizepercent-'+($prizenum+1)+'" name="prizepercent-'+($prizenum+1)+'" min="0" placeholder="佔比例" data-toggle="tooltip" data-placement="bottom" title="設定抽獎比例(廠商看)" onchange="modulesFunction.calcPercent()" required><span class="input-group-addon">%</span> </div> </div> </div> </div></div>';

                $str3 = '<div class="form-group"><div class="col-md-3"></div><div class="col-md-5"></div><div class="col-md-2"></div><div class="col-md-2"><span class="text-danger text-right calcPercentText"></span></div></div></div>';
              $("#prizeContent").html(($("#noprize1").prop('checked'))?$str+$str_byebye+$str3:$str+$str3);

                $('[data-toggle="tooltip"]').tooltip();
                modulesFunction.checkInputRequired();
                modulesFunction.inputnumber();
        }
        else
        {
            swal({
                    title: "注意！",
                    text: "請輸入獎品項目數量！",
                    icon: "warning",
                    default: true
                });
            return false;
        }
    },
    e_makePrizeEvent:function(){
        if($("#e-prizenum").val())
        {
            $prizenum =  parseInt($("#e-prizenum").val());
            if($prizenum < 1)
            {
                swal({
                    title: "注意！",
                    text: "數量不可小於1項！",
                    icon: "error",
                    default: true
                });
                return false;
            }
              $str = '';

              if($("#e-noprize1").prop('checked'))
                  {$prizenum-=1;}
              for (var i = 0; i < $prizenum; i++) {
                $str += '<div class="form-group"> <label class="control-label col-md-2">'+(i+1)+'</label> <div class="col-md-2"> <img id="preview_image" src="https://via.placeholder.com/120x100/FFFFFF/000000?text=預覽圖片" class="img-thumbnail preview_image-'+(i+1)+'" style="max-width: 120px;max-height: 100px;"> </div> <div class="col-md-8"> <div class="row"> <div class="col-md-6"> <input type="file" class="form-control prize-'+(i+1)+'" name="prize-'+(i+1)+'" id="prize-'+(i+1)+'" accept="image/jpeg,image/gif,image/png" onchange="modulesFunction.previewEvent($(this));" required></div> <div class="col-md-6"> <span class="label label-danger">建議圖片最小為300*300正方形；大小<=500k。</span> </div> </div> <div class="row"> <div class="col-md-6"> <input type="text" class="form-control prizename-'+(i+1)+'" name="prizename-'+(i+1)+'" placeholder="獎品名稱" data-toggle="tooltip" data-placement="bottom" title="獎品名稱，中獎時顯示的獎品內容(客戶看)" required> <div class="checkbox"><label><input type="checkbox" name="showwintxt-'+(i+1)+'" checked> 恭喜中獎(預設) </label> </div> </div> <div class="col-md-3"><input type="number" class="form-control prizenum-'+(i+1)+'" name="prizenum-'+(i+1)+'" min="0" placeholder="數量" data-toggle="tooltip" data-placement="bottom" title="該獎品總數量(廠商看)" required> </div> <div class="col-md-3"> <div class="input-group"> <input type="number" class="form-control percent prizepercent-'+(i+1)+'" name="prizepercent-'+(i+1)+'" min="0" placeholder="佔比例" data-toggle="tooltip" data-placement="bottom" title="設定抽獎比例(廠商看)" onchange="modulesFunction.calcPercent()" required><span class="input-group-addon">%</span> </div> </div> </div> </div></div>';        
              }
              $str_byebye = '<div class="form-group"> <label class="control-label col-md-2">'+($prizenum+1)+'</label> <div class="col-md-2"> <img id="preview_image" src="https://via.placeholder.com/120x100/FFFFFF/000000?text=預覽圖片" class="img-thumbnail preview_image-'+($prizenum+1)+'" style="max-width: 120px;max-height: 100px;"> </div> <div class="col-md-8"> <div class="row"> <div class="col-md-6"> <input type="file" class="form-control prize-'+($prizenum+1)+'" name="prize-'+($prizenum+1)+'" id="prize-'+($prizenum+1)+'" accept="image/jpeg,image/gif,image/png" onchange="modulesFunction.previewEvent($(this));" required></div> <div class="col-md-6"> <span class="label label-danger">建議圖片最小為300*300正方形；大小<=500k。</span> </div> </div> <div class="row"> <div class="col-md-6"> <input type="text" class="form-control prizename-'+($prizenum+1)+'" name="prizename-'+($prizenum+1)+'" placeholder="獎品名稱" data-toggle="tooltip" data-placement="bottom" title="可更改文字或放置連結(客戶看)" value="銘謝惠顧" required> <div class="checkbox hidden"><label><input type="checkbox" name="showwintxt-'+($prizenum+1)+'"> 恭喜中獎(預設) </label> </div> </div> <div class="col-md-3"><input type="number" class="form-control prizenum-'+($prizenum+1)+'" name="prizenum-'+($prizenum+1)+'" min="0" placeholder="數量" data-toggle="tooltip" data-placement="bottom" title="該獎品總數量(廠商看)" required> </div> <div class="col-md-3"> <div class="input-group"> <input type="number" class="form-control percent prizepercent-'+($prizenum+1)+'" name="prizepercent-'+($prizenum+1)+'" min="0" placeholder="佔比例" data-toggle="tooltip" data-placement="bottom" title="設定抽獎比例(廠商看)" onchange="modulesFunction.calcPercent()" required><span class="input-group-addon">%</span> </div> </div> </div> </div></div>';

                $str3 = '<div class="form-group"><div class="col-md-3"></div><div class="col-md-5"></div><div class="col-md-2"></div><div class="col-md-2"><span class="text-danger text-right calcPercentText"></span></div></div></div>';
              $("#e-prizeContent").html(($("#e-noprize1").prop('checked'))?$str+$str_byebye+$str3:$str+$str3);

                $('[data-toggle="tooltip"]').tooltip();
                modulesFunction.checkInputRequired();
                modulesFunction.inputnumber();
        }
        else
        {
            swal({
                    title: "注意！",
                    text: "請輸入獎品項目數量！",
                    icon: "warning",
                    default: true
                });
            return false;
        }
    },
    checkInputRequired:function(){
        $("form input[required]").change(function(e){
            modulesFunction.validateEvent($(this));
        });
        $("form input[required]").focusin(function(e){
            modulesFunction.validateEvent($(this));
        });
    },
    validateEvent:function(obj){
        if(!obj.val())
        {
            obj.closest('.form-group').find('span.text-danger').remove();
            obj.after('<span class="text-danger">必填</span>');            
            obj.closest('.form-group').addClass('has-error has-feedback');
        } 
        else
        {
            obj.closest('.form-group').find('span.text-danger').remove();
            obj.closest('.form-group').removeClass('has-error has-feedback');   
        }
    },
    randomString: function(len){
            len = len || 30;
          var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
          var maxPos = $chars.length;
          var pwd ='';
          for (i = 0;i < len; i++ ) {
            pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
          }
          return pwd;
    },
    inputnumber: function(){
        $("input[type='number']").change(function(){
           if($(this).val() < 0)
            {
                $(this).val(0);
            }
            modulesFunction.calcPercent();
        });
        
    }
};
$(function(){
    modulesFunction.ui();
    
})

