var container_opt = $("#container-opt");
var container_header = $("#container-header");
var content_panel = $("#content-panel");
var container_header_dropdown =$("#container-header .dropdown");
var modules = {
    ui: function(){
        if ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            container_opt.animate({ left:'-200px' }, 200, 'swing').removeClass('optOpen');
                container_header.animate({ marginLeft:'0px' }, 200, 'swing').removeClass('optOpen');
                content_panel.animate({ marginLeft:'0px' }, 200, 'swing').removeClass('optOpen');
                container_header_dropdown.animate({ marginRight:'0px' }, 200, 'swing').removeClass('optOpen');
        }
        
        $(".dropdown>a").click(function (event) {
            if ($(this).next().css('display') == "none") {
                //展開
                $(".dropdown").children('ul').slideUp(500);
                $(this).next('ul').slideDown(300);
                $(this).parent('li').addClass('dropdown-show').siblings('li').removeClass('dropdown-show');
            }
            else {
                // 收起
                $(this).next('ul').slideUp(300);
                $('.dropdown.dropdown-show').removeClass('dropdown-show');
            }
        });
        modules.sildebar();
    },
    sildebar: function(){
        $("#optButton").click(function(event) {
            
            if(container_opt.hasClass('optOpen'))
            {
                container_opt.animate({ left:'-200px' }, 200, 'swing').removeClass('optOpen');
                container_header.animate({ marginLeft:'0px' }, 200, 'swing').removeClass('optOpen');
                content_panel.animate({ marginLeft:'0px' }, 200, 'swing').removeClass('optOpen');
                container_header_dropdown.animate({ marginRight:'0px' }, 200, 'swing').removeClass('optOpen');
            }
            else
            {
                container_opt.animate({ left:'0px' }, 200, 'swing').addClass('optOpen');
                container_header.animate({ marginLeft:'200px' }, 200, 'swing').addClass('optOpen');
                content_panel.animate({ marginLeft:'200px' }, 200, 'swing').addClass('optOpen');
                container_header_dropdown.animate({ marginRight:'200px' }, 200, 'swing').addClass('optOpen');
            }
        });
    }
};
function showtodaydate()
{
    var fullDate = new Date();
    var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    var weeks = ["Sun","Mon","Tue","Wed","Thur","Fri","Sat"];
    var twoDigitYear = fullDate.getFullYear();
    var twoDigitMonth = months[fullDate.getMonth()] + " ";
    var twoDigitDate = fullDate.getDate() + " ";
    var twoDigithour = fullDate.getHours().toString();
    var twoDigitmin = fullDate.getMinutes().toString();
    var twoDigitmday = weeks[fullDate.getDay()] + ", ";
    if (twoDigithour.length == 1) twoDigithour = "0" + twoDigithour;
    if (twoDigitmin.length == 1) twoDigitmin = "0" + twoDigitmin;
    var showTime = twoDigithour + ":" + twoDigitmin; 
    var showDate = twoDigitmday + twoDigitMonth + twoDigitDate + twoDigitYear;
    $(".showTime").text(showTime);
    $(".showDate").text(showDate);
}
function logoutEvent()
{
    localStorage.removeItem('_name');
    $.post('route/controller.php?action=logout', '', function(data, textStatus, xhr) {
            window.location.href=data.returnUrl;
      },"json");
}
window.onload = function () {
    setInterval(showtodaydate, 1000);
} 
$(function () {
    modules.ui();
    showtodaydate();
});