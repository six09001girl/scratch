var container_opt = $("#container-opt");
var container_header = $("#container-header");
var content_panel = $("#content-panel");
var container_header_dropdown =$("#container-header .dropdown");
var header_username = localStorage.getItem("username");
var heaer_level = localStorage.getItem("level");
var modules = {
    ui: function(){
        if ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            container_opt.animate({ left:'-200px' }, 200, 'swing').removeClass('optOpen');
                container_header.animate({ marginLeft:'0px' }, 200, 'swing').removeClass('optOpen');
                content_panel.animate({ marginLeft:'0px' }, 200, 'swing').removeClass('optOpen');
                container_header_dropdown.animate({ marginRight:'0px' }, 200, 'swing').removeClass('optOpen');
        }
        $(".logoutBT").click(function(event) {
             modules.logoutEvent();
          });
        modules.sildebar();
        $("#header_username").text("  "+header_username);
        $(".header-logo-icon").attr('src','pic/level' + heaer_level + '.jpg');
        
        
    },
    sildebar: function(){
        $("#optButton").click(function(event) {
            
            if(container_opt.hasClass('optOpen'))
            {
                container_opt.animate({ left:'-200px' }, 200, 'swing').removeClass('optOpen');
                container_header.animate({ marginLeft:'0px' }, 200, 'swing').removeClass('optOpen');
                content_panel.animate({ marginLeft:'0px' }, 200, 'swing').removeClass('optOpen');
                container_header_dropdown.animate({ marginRight:'0px' }, 200, 'swing').removeClass('optOpen');
            }
            else
            {
                container_opt.animate({ left:'0px' }, 200, 'swing').addClass('optOpen');
                container_header.animate({ marginLeft:'200px' }, 200, 'swing').addClass('optOpen');
                content_panel.animate({ marginLeft:'200px' }, 200, 'swing').addClass('optOpen');
                container_header_dropdown.animate({ marginRight:'200px' }, 200, 'swing').addClass('optOpen');
            }
        });
    },
    showtodaydate:function(){
        var fullDate = new Date();
        var twoDigitMonth = (fullDate.getMonth()+1)+"";
        if(twoDigitMonth.length==1)  twoDigitMonth="0" +twoDigitMonth;
        var twoDigitDate = fullDate.getDate()+"";
        var twoDigithour = fullDate.getHours()+":";
        var twoDigitmin = fullDate.getMinutes()+":";
        var twoDigitmsec = fullDate.getSeconds()+"";
        if(twoDigitDate.length==1) twoDigitDate="0" +twoDigitDate;
        if(twoDigithour.length==1) twoDigithour="0" +twoDigithour;
        if(twoDigitmin.length==1) twoDigitmin="0" +twoDigitmin;
        if(twoDigitmsec.length==1) twoDigitmsec="0" +twoDigitmsec;
        var currentDate = twoDigithour + twoDigitmin + twoDigitmsec + "\t" + twoDigitDate + "/" + twoDigitMonth + "/"  +fullDate.getFullYear();
        $("#today-date").text(currentDate);
    },
    logoutEvent: function(){
        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "/logout",
          "method": "POST"
        }

        $.ajax(settings).success(function(response){
          localStorage.clear();
          window.location.href="./";
          // console.log(response);
        });
    }
};
window.onload=function(){
    setInterval(modules.showtodaydate,1000);
}
$(function () {
    modules.ui();
});