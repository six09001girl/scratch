var modules = {
    ui: function(){
        $("#fanpagetitle").text(getargs().name);
        $(".bootstrap-tagsinput input").attr('placeholder', '關鍵字請以enter分隔');
        
        $('#setautoreplyModal').on('hidden.bs.modal', function (e) {
          $("#setautoreplyForm")[0].reset();
            $("#actid").val('-1');
            $("input[data-role=tagsinput]").tagsinput('removeAll');
            $("#ser").val("");
        });
        
        $("#setBT").click(function(){
            if($("#actid").val() == '-1') return false;
            if($("#ser").val())
            {
                setttingEvent('editfanspage');
            }
            else
            {
                setttingEvent('addfanspage');
            }
            
        });
        
        var dataAry = {
            id:getargs().id,
            accode:getargs().accode
          };
        $('body').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
          $.post('route/controller.php?action=loadArticlemsg', dataAry, function(data, textStatus, xhr) {
              console.log(data);
              str = "";
                  switch(data.message){
                    case "failed":
    //                  alert(data.errmsg);
                        swal({
                            title: "失敗!",
                            icon: "error",
                            default: true
                        }).then((value) => {                            
                            window.location.href="./Fanspages?param1=index";
                        });

                    break;
                    case "success":
                        
                        for(var i=0; i<data.data.postArticleData.length; i++)
                        {
                            str += "<tr data-id='"+data.data.postArticleData[i].id+"'>";
                            str += "<td>"+data.data.postArticleData[i].created_time.substr(0,19)+"</td>";
                            str += "<td>"+data.data.postArticleData[i].message+"</td>";
                            str += "<td><button class='btn btn-info autoreplyBT' data-id='"+data.data.postArticleData[i].id+"' data-toggle='modal' data-target='#setautoreplyModal' onclick='loadEvent(\" "+data.data.postArticleData[i].id+" \")'>設定自動回應</button></td>";
                            str += "</tr>";
                        }

                        $('#loadArticleTable tbody').html(str);
                    break;
                    default:
                    break;
                  }
        },"json").done(function(){
            $( "#loadingDiv" ).fadeOut(500, function() {
                  $( "#loadingDiv" ).remove(); //makes page more lightweight 
            });
        });
        
        $.post('route/controller.php?action=getActSelectList', function(data, textStatus, xhr) {            
            str="<option value='-1'>請選擇</option>";
            for(var i=0; i<data.data.length; i++)
            {
                str+="<option value='"+data.data[i].SER+"'>"+data.data[i].ACTTITLE+"</option>";
            }
            $("#actid").append(str);
        },"json");
        
    }
}
function getargs(){
    var url = window.location.search; 
    var args = new Object();   
    if (url.indexOf("?") != -1) {   
          var str = url.substr(1);   
          strs = str.split("&");   
          for(var i = 0; i < strs.length; i ++) { 
             args[strs[i].split("=")[0]]=decodeURI(strs[i].split("=")[1]); 
          }   
    } 
    return args;
}
function loadEvent(articleid)
{    
    $("#articleid").val(articleid.trim());
    var dataAry = {
        articleid:articleid.trim(),
        pageid:getargs().id
      };
    $('#setautoreplyModal').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
      $.post('route/controller.php?action=loadfanspage', dataAry, function(data, textStatus, xhr) {
          str = "";
              switch(data.message){
                case "failed":
//                  alert(data.errmsg);
                    swal({
                        title: "尚未設定過相關資料！",
                        icon: "warning",
                        default: true
                    });

                break;
                case "success":   
                    $("#ser").val(data.data[0].SER);
                    $("input#keyword[data-role=tagsinput]").tagsinput('add',data.data[0].KEYWORD);
                    $("#leavemsg").val(data.data[0].COMMENT);
                    $("#messager").val(data.data[0].MESSAGE);
                      $("#actid option").each(function(){
                            if($(this).val() == data.data[0].ACTID.trim())
                              $(this).attr("selected", true);
                            else
                              $(this).attr("selected", false);
                        });
                      $("#actid").val(data.data[0].ACTID.trim().toString());
                      $("input#msgkeyword[data-role=tagsinput]").tagsinput('add',data.data[0].MSGKEYWORD);
                      $("#callback").val(data.data[0].CALLBACK);
                default:
                break;
              }
    },"json").done(function(){
        $( "#loadingDiv" ).fadeOut(500, function() {
              $( "#loadingDiv" ).remove(); //makes page more lightweight 
        });
    });
    
    

}

function setttingEvent(action)
{    
    var dataAry = {
        pageid:getargs().id,        
        pagetoken:getargs().accode,
        articleid: $('#articleid').val(),
        keyword:$("input#keyword[data-role=tagsinput]").val(),
        comment:$("#leavemsg").val(),
        message:$("#messager").val(),
        msgkeyword:$("input#msgkeyword[data-role=tagsinput]").val(),
        callback:$("#callback").val(),
        actid:$("#actid").val()
      };
    $('#setautoreplyModal').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
      $.post('route/controller.php?action='+action, dataAry, function(data, textStatus, xhr) {
//          console.log(data);
          str = "";
              switch(data.message){
                case "failed":
//                  alert(data.errmsg);
                    swal({
                        title: data.errmsg,
                        icon: "error",
                        default: true
                    });

                break;
                case "success":
                    swal({
                        title: data.errmsg,
                        icon: "success",
                        default: true
                    }).then((value) => {                            
                        $('#setautoreplyModal').modal('hide');
                    });
                    
                break;
                default:
                break;
              }
    },"json").done(function(){
        $( "#loadingDiv" ).fadeOut(500, function() {
              $( "#loadingDiv" ).remove(); //makes page more lightweight 
        });
    });
}

$(function(){
    modules.ui();
    
});