var modules = {
  ui: function(){
      gotolist();
      checkInputRequired();
      
      // 關閉modal，清空表單內容
      $('#addUsersModal').on('hidden.bs.modal', function (event) {
          $("#addUserForm")[0].reset();
      });
      
      $("#addUserBT").click(function(event) {
          $("#addUserForm input[required]").each(function(){
                validateEvent($(this));
            });
            if($("#addUserForm .form-group").hasClass('has-error has-feedback'))
            {
                return false;
            }
            else
            {
                addUserEvent();
            }                 
        });
      
      
      $("#editUserBT").click(function(event) {
        saveEvent($("#e_h_usercode").val());
      });
  }
};
function gotolist(){
    if ( $.fn.dataTable.isDataTable( '#accountListTable' ) ) {
        $('#accountListTable').DataTable();
    }
    else
    {
        $('#accountListTable').DataTable({
            "bProcessing": true,
            "sAjaxSource": "route/controller.php?action=getSupplyList",
            "aoColumns": [
                { "mData": "ID"},
                { "mData": "industryname"},
                { "mData": "USERNAME"},
                { "mData": "ADDR"},
                { "mData": "TEL"},
                { "mData": "CONTACTOR"},
                { "mData": "STATUS",
                    "mRender":function(val,type,row){
                        return "<span class='label label-success'>" + changetoword('userstatus',val)+ "</span>";
                    }
                },
                { "mData": "RIGHTS",
                    "mRender":function(val,type,row){
                        return "<span class='label label-info'>" + changetoword('userrole',val)+ "</span>";
                    }
                },
                { "mData": "ID", "sWidth": '20%' ,
                      "mRender":function(val,type,row) {
                        return "<button class='btn btn-warning btn-circle' data-toggle='modal' data-target='#editUsersModal' onclick='loadUserEvent(\""+val.toString()+"\")'><span class='fa fa-pencil' aria-hidden='true'></span></button><button class='btn btn-danger btn-circle' onclick='deleteEvent(\""+val.toString()+"\")'><span class='fa fa-trash' aria-hidden='true'></span></button>";
                      }
                }
            ],
            "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
               
                $(nRow).attr("data-id",aData.ID);
                return nRow;
            },
            "language": {
                "processing": "資料載入中...",
                "lengthMenu": "每頁顯示 _MENU_ 筆",
                "zeroRecords": "資料庫中未有相關資料。",
                "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                "infoEmpty": "資料庫中未有相關資料。",
                "search":  "搜尋:",
                "paginate": {
                      "first":      "第一頁",
                      "last":       "最後一頁",
                      "next":       "下一頁",
                      "previous":   "上一頁"
                  }
            }
        });
//        $( "#loadingDiv" ).fadeOut(500, function() {
//              $( "#loadingDiv" ).remove(); //makes page more lightweight 
//        });
    }
    
}
  
function addUserEvent(){
  $("#addUserBT").button('loading');
    var dataAry = {
      usercode:$("#usercode").val(),
      username:$("#username").val(),
      industry:$("#industry").val(),
        worktime:$("#worktime").val(),
        addr:$("#addr").val(),
        tel:$("#tel").val(),
        contactor:$("#contactor").val(),
        fax:$("#fax").val()
    };
    $.post('route/controller.php?action=addSupply', dataAry, function(data, textStatus, xhr) {
            switch(data.message){
              case "failed":
                swal({
                    title: "新增失敗！",
                    text: "Oh...NO!",
                    icon: "error",
                    default: true
                }).then((value) => {                            
                    $("#addUserForm")[0].reset();
                });
              break;
              case "success": 
                swal({
                    title: "新增成功!",
                    text: "Good Job!",
                    icon: "success",
                    default: true
                }).then((value) => {                            
                    $('#addUsersModal').modal('hide'); 
                });                 
                $('#accountListTable').DataTable().destroy();
                gotolist();
              break;
              default:
              break;
            }
      },"json").done(function() {
        $("#addUserBT").button('reset');
      });
}
function loadUserEvent(user){
    $("#editUserBT").button('loading');
  var dataAry = {
      usercode:user
    };
    $.post('route/controller.php?action=loadSupply', dataAry, function(data, textStatus, xhr) {
            postData=data.data;
            switch(data.message){
              case "failed":
                swal({
                    title: "載入失敗！請聯繫管理員。",
                    text: "Oh...NO!",
                    icon: "error",
                    default: true
                });
              break;
              case "success":   
                $("#e_usercode,#e_h_usercode").text(postData[0].USERCODE);
                $("#e_h_usercode").val(postData[0].USERCODE);
                $("#e_username").val(postData[0].USERNAME);
                $("#e_industry").val(postData[0].INDUSTRY);
                $("#e_addr").val(postData[0].ADDR);
                $("#e_tel").val(postData[0].TEL);
                $("#e_contactor").val(postData[0].CONTACTOR);
                $("#e_fax").val(postData[0].FAX);
                $("#e_lock").val(postData[0].LOCK);
                $("#e_role").val(postData[0].RIGHTS);
              break;
              default:
              break;
            }
      },"json").done(function() {
        $("#editUserBT").button('reset');
      });
}
function saveEvent(user){
    $("#editUserBT").button('loading');
  var dataAry = {
      usercode:$("#e_h_usercode").val(),
      username:$("#e_username").val(),
      industry:$("#e_industry").val(),
        worktime:$("#e_worktime").val(),
        addr:$("#e_addr").val(),
        tel:$("#e_tel").val(),
        contactor:$("#e_contactor").val(),
        fax:$("#e_fax").val(),
        lock:$("#e_lock").val(),
        rights:$("#e_role").val()
    };
    $.post('route/controller.php?action=editSupply', dataAry, function(data, textStatus, xhr) {
            switch(data.message){
              case "failed":
                swal({
                    title: "修改失敗！",
                    text: "Oh...NO!",
                    icon: "error",
                    default: true
                });
              break;
              case "success":  
                swal({
                    title: "修改成功!",
                    text: "Good Job!",
                    icon: "success",
                    default: true
                }).then((value) => {      
                    $('#editUsersModal').modal('hide');  
                });
                   
                $('#accountListTable').DataTable().destroy();
                gotolist();  
                
              break;
              default:
              break;
            }
      },"json").done(function() {
        $("#editUserBT").button('reset');
      });
}
function deleteEvent(user){
  if(confirm("確定要刪除嗎?不能後悔喔!"))
  {
    var dataAry = {
      usercode:user,
    };
    $.post('route/controller.php?action=deleteSupply', dataAry, function(data, textStatus, xhr) {
            switch(data.message){
              case "failed":
                swal({
                    title: "刪除失敗！",
                    text: "Oh...NO!",
                    icon: "error",
                    default: true
                });
              break;
              case "success":  
                swal({
                    title: "刪除成功!",
                    text: "Good Job!",
                    icon: "success",
                    default: true
                });
                $('#accountListTable').DataTable().destroy();
                gotolist();
              break;
              default:
              break;
            }
      },"json");
  }
}
function changetoword(word, value){
  switch(word){
      case 'userstatus':
        switch(value){
          case '0':
            return '註冊中';
          break;
          case '1':
            return '未設(無)功能';
          break;
          case '2':
            return '系統停權';
          break;
          case '3':
            return '用戶暫停使用';
          break;
          default:
            return '永久停權';
          break;
        }
      break;
    case 'userrole':
        switch(value){
            case '':
          case '1':
            return '一般使用者';
          break;
          default:
            return '管理者';
          break;
        }
      break;
      default:
       return false;
      break;
    }
}
function checkInputRequired(){
    $("form input[required]").change(function(e){
        validateEvent($(this));
    });
    $("form input[required]").focusin(function(e){
        validateEvent($(this));
    });
}
function validateEvent(obj){
    if(!obj.val())
    {
        obj.closest('.form-group').find('span.text-danger').remove();
        obj.after('<span class="text-danger">required</span>');            
        obj.closest('.form-group').addClass('has-error has-feedback');
    } 
    else
    {
        obj.closest('.form-group').find('span.text-danger').remove();
        obj.closest('.form-group').removeClass('has-error has-feedback');   
    }
} 
$(function(){
    modules.ui();
})

