var modulesFunction = {
    ui: function(){
       // $("#cashBTGroup button").click(function(event) {
       //      if(!$(this).hasClass('disabled'))
       //      {
       //          $("#cashBTGroup button").removeClass('active').removeClass('disabled');
       //         $(this).addClass('active').addClass('disabled');
       //         switch($(this).attr('id')){
       //              case "PVBT":
       //                  $("#PVLB").text('PV').removeClass('label-success').addClass('label-warning');
       //                  $("#cashCoin").text("PV $"+ parseFloat($("#cashCoin").text().split('$')[1])/28);                    
       //              break;
       //              case "NTBT":
       //                  $("#PVLB").text('TWD').removeClass('label-warning').addClass('label-success');
       //                  $("#cashCoin").text("NT $"+ parseFloat($("#cashCoin").text().split('$')[1])*28);
       //              break;
       //         }
       //      }
           
       // });
        $('[data-toggle="tooltip"]').tooltip();
        // modulesFunction.queryequborrowcount();
        // modulesFunction.queryequunborrowcount();
        // modulesFunction.queryequscrappedcount();
		// modulesFunction.gotolist();
    },
    queryequtotcount: function(){
        $.post('route/route_event.php?action=equtotcount', function (data, textStatus, xhr) {
            switch (data.message) {
                case "failed":
                    $("#equtotcount").text('無');
                    break;
                case "success":
                    postData=data.data[0];
                    $("#equtotcount").text(postData.equtotcount);                    
                    break;
            }
        }, "json");
    },
    queryequborrowcount: function(){
        $.post('route/route_event.php?action=equborrowcount', function (data, textStatus, xhr) {
            switch (data.message) {
                case "failed":
                    $("#equborrowcount").text('無');
                    break;
                case "success":
                    postData=data.data[0];
                    $("#equborrowcount").text(postData.equborrowcount);                    
                    break;
            }
        }, "json");
    },
    queryequunborrowcount: function(){
        $.post('route/route_event.php?action=equunborrowcount', function (data, textStatus, xhr) {
            switch (data.message) {
                case "failed":
                    $("#equunborrowcount").text('無');
                    break;
                case "success":
                    postData=data.data[0];
                    $("#equunborrowcount").text(postData.equunborrowcount);                    
                    break;
            }
        }, "json");
    },
    queryequscrappedcount: function(){
        $.post('route/route_event.php?action=equscrappedcount', function (data, textStatus, xhr) {
            switch (data.message) {
                case "failed":
                    $("#equscrappedcount").text('無');
                    break;
                case "success":
                     postData=data.data[0];
                    $("#equscrappedcount").text(postData.equscrappedcount);                    
                    break;
            }
        }, "json");
    },
	gotolist: function(){
		$("#dashboard a").click(function(){
			localStorage.setItem("dashboardlist", $(this).attr("href").replace("#", ""));
			$.get("Views/dashboardListContent" , function(data) {
				// console.log(data);
				$("#content-wrapper").html(data);
			});
		})
	}
};

$(function(){
    modulesFunction.ui();
})

