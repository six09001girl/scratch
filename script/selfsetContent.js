var modules = {
  ui: function(){
      loadUserEvent();
    
      $("#saveBT").click(function(event) {
        if(!checkRegEx())
        {
            return false;
        }
        saveEvent();
      });
  }
};

function loadUserEvent(){
    $('body').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
    $.post('route/controller.php?action=loadLocalSupply', function(data, textStatus, xhr) {
            postData=data.data;
            switch(data.message){
              case "failed":
                swal({
                    title: "載入失敗！請聯繫管理員。",
                    text: "Oh...NO!",
                    icon: "error",
                    default: true
                });
              break;
              case "success":   
                $("#usercode").text(postData[0].USERCODE);
                $("#username").val(postData[0].USERNAME);
                $("#industry").val(postData[0].INDUSTRY);
                $("#worktime").val(postData[0].WORKTIME);
                $("#addr").val(postData[0].ADDR);
                $("#tel").val(postData[0].TEL);
                $("#contactor").val(postData[0].CONTACTOR);
                $("#fax").val(postData[0].FAX);
              break;
              default:
              break;
            }
      },"json").done(function() {
        $( "#loadingDiv" ).fadeOut(500, function() {
              $( "#loadingDiv" ).remove(); //makes page more lightweight 
        });
      });
}
function saveEvent(){
    $("#saveBT").button('loading');
  var dataAry = {
      username:$("#username").val(),
      industry:$("#industry").val(),
        worktime:$("#worktime").val(),
        addr:$("#addr").val(),
        tel:$("#tel").val(),
        contactor:$("#contactor").val(),
        fax:$("#fax").val()
    };
    $.post('route/controller.php?action=editLocalSupply', dataAry, function(data, textStatus, xhr) {
            switch(data.message){
              case "failed":
                swal({
                    title: "修改失敗！",
                    text: "Oh...NO!",
                    icon: "error",
                    default: true
                });
              break;
              case "success":  
                swal({
                    title: "修改成功!",
                    text: "Good Job!",
                    icon: "success",
                    default: true
                });
                loadUserEvent();
              break;
              default:
              break;
            }
      },"json").done(function() {
        $("#saveBT").button('reset');
      });
}
function checkRegEx(){
    var str = "";
    $("input[data-toggle]").each(function(index){
        var vals = $(this).val();
        var obj_text = $(this).parent().find('label').text();
       switch($("input[data-toggle]").attr('data-toggle')) 
        {
            case 'phone':
                if (!/^\(?\d{2}\)?[\s\-]?\d{4}\-?\d{4}$/.test(vals))
                {
                    str+='欄位:' + obj_text + '，聯絡電話格式錯誤！\n';
                }
                break;
        }
    });
    
    if(str)
    {
        swal({
            title: "錯誤！",
            text: str,
            icon: "error",
            default: true
        });
        return false;
    }
    else
    {
        return true;
    }
}
function checkInputRequired(){
    $("form input[required]").change(function(e){
        validateEvent($(this));
    });
    $("form input[required]").focusin(function(e){
        validateEvent($(this));
    });
}
function validateEvent(obj){
    if(!obj.val())
    {
        obj.closest('.form-group').find('span.text-danger').remove();
        obj.after('<span class="text-danger">required</span>');            
        obj.closest('.form-group').addClass('has-error has-feedback');
    } 
    else
    {
        obj.closest('.form-group').find('span.text-danger').remove();
        obj.closest('.form-group').removeClass('has-error has-feedback');   
    }
} 
$(function(){
    modules.ui();
})

