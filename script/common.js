var common_init_modules ={
    ui: function(){
        common_init_modules.getcmeta();
        common_init_modules.getbodyr();
    },
    getcmeta: function(){        
        $.get("Views/meta" , function(data) {
            $("head").prepend(data);
          });
    },
    getbodyr: function(){
        $.get("Views/body", function(data) {
            $("body").append(data);
          });
    }
}
$(function(){
   common_init_modules.ui();
});