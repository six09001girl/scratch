var container_opt = $("#container-opt");
var content_panel = $("#content-panel");
var website_init_modules = {
    ui: function(){
        website_init_modules.getheader();
        website_init_modules.followsildebar();
    },
    getconfig_module_style: function(){        
        $.get("Views/config_module_style" , function(data) {
            $("head").append(data);
          });
    },
    getheader: function(){
        $.get("Views/header", function(data) {
            $("body").prepend(data);

                // $.get("Views/footer", function(data) {
                  // $("#website-wrapper").after(data);
                 
                // });
          });
    },
    followsildebar: function(){
        $("#optButton,#optMobileButton").click(function(event) {
            
            if(container_opt.hasClass('optOpen'))
            {
                content_panel.animate({ marginLeft:'0px' }, 200, 'swing').removeClass('optOpen');
            }
            else
            {
                content_panel.animate({ marginLeft:'200px' }, 200, 'swing').addClass('optOpen');
            }
        });
    }
};
function showtodaydate()
{
    var fullDate = new Date();
    var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    var weeks = ["Sun","Mon","Tue","Wed","Thur","Fri","Sat"];
    var twoDigitYear = fullDate.getFullYear();
    var twoDigitMonth = months[fullDate.getMonth()] + " ";
    var twoDigitDate = fullDate.getDate() + " ";
    var twoDigithour = fullDate.getHours().toString();
    var twoDigitmin = fullDate.getMinutes().toString();
    var twoDigitmday = weeks[fullDate.getDay()] + ", ";
    if (twoDigithour.length == 1) twoDigithour = "0" + twoDigithour;
    if (twoDigitmin.length == 1) twoDigitmin = "0" + twoDigitmin;
    var showTime = twoDigithour + ":" + twoDigitmin; 
    var showDate = twoDigitmday + twoDigitMonth + twoDigitDate + twoDigitYear;
    $(".showTime").text(showTime);
    $(".showDate").text(showDate);
}
$(function() {
     website_init_modules.ui();
    setInterval(showtodaydate, 1000);

        $.post('route/controller.php?action=_checkauth', '', function(data, textStatus, xhr) {    
            window.location.href=data.returnUrl;
          },"json");
});
