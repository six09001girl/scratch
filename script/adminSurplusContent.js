var addTasksModalbackup = $('#addTasks').clone();
var modulesFunction = {
    ui: function(){             
		      modulesFunction.createCaptcha();   
          $('#keyinDate').datepicker({
              format: "yyyymmdd",
              calendarWeeks: true
          });
          var $pass = $('#pass'), 
                $show_pass = $('<input type="text" name="' + $pass.attr('name') + '" class="' + $pass.attr('class') + '"   />');
          $('#show_pass').click(function(){
              if(!$(this).hasClass('showedPass')){
                $pass.replaceWith($show_pass.val($pass.val()));
                $(this).addClass('showedPass');
              }else{
                $show_pass.replaceWith($pass.val($show_pass.val()));
                $(this).removeClass('showedPass');
              }
            });
          $("#SurplusBT").click(function(event) {
            if(modulesFunction.requiredInput().length > 8)
            {
                alert(modulesFunction.requiredInput());
            }
            else
            {
              if(modulesFunction.validCaptcha())
                {
                  modulesFunction.savedEvent();   
                }
                else
                {
                  alert("驗證碼輸入錯誤");
                }
            }
                
                                
          })
        },
    getTasksList: function(level){
       var settings = {
          "async": true,
          "crossDomain": true,
          "url": "/getAction",
          "method": "POST",
          "headers": {
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            "postman-token": "150fa44f-cf8c-0992-1192-832d5816a97f"
          }
        }

        $.ajax(settings).success(function(response){
            var str="";
            if(response.length <= 0)
            {
              str+="<tr><td colspan='5'>資料庫中未有相關資料。</td></tr>";
            }
            else
            {
              for (var i = 0; i < response.length; i++) {
                      str+="<tr data-id='"+ response[i]._id +"'>";
                      str+="<td>" + response[i].merchant+ "</td>";
                      str+="<td>" + response[i].title+ "</td>";
                      str+="<td>" + response[i].content+ "</td>";
                      str+="<td><a class='btn btn-default action_link' href='" + response[i].action_url+ "' target='_blank'>開啟連結</a></td>";
                      str+="<td><button class='btn btn-danger btn-lg' onclick='modulesFunction.deleteEvent(\""+response[i]._id.toString()+"\")'><span class='fa fa-trash' aria-hidden='true'></span></button> </td>";
                      str+="</tr>";
                }
            }
            
          $("#tasksListTable tbody").html(str);
          $("#tasksListTable").DataTable();
        }).error(function (jqXHR, textStatus, errorThrown) {
          switch(jqXHR.status){
            case 200:
              alert("資料讀取成功!");
            break;
            default:
              alert("資料載入錯誤!");
            break;
          }
        });
    },
    savedEvent: function(){
        var dataAry = {
            date:$("#keyinDate").val(),
            bonus:$("#Surplus").val(),
            bonus_pass:$("#pass").val()
        };
        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "/setSurplus",
          "method": "POST",
          "headers": {
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            "postman-token": "150fa44f-cf8c-0992-1192-832d5816a97f"
          },
          "data":dataAry
        };
                  $.ajax(settings).success(function(response){
                        alert('新增成功');
                        
                    }).error(function (jqXHR, textStatus, errorThrown) {
                          alert('新增失敗');
                    });
        
    },
    deleteEvent: function(_id){
        var dataAry = {
          action_id: _id
        };
        var settings = {
              "async": true,
              "crossDomain": true,
              "url": "/delAction",
              "method": "POST",
              "headers": {
                "content-type": "application/x-www-form-urlencoded",
                "cache-control": "no-cache",
                "postman-token": "150fa44f-cf8c-0992-1192-832d5816a97f"
              },
              "data":dataAry
            };

            if (confirm("是否確定進行刪除？")) {
                  $.ajax(settings).success(function(response){
                            alert('刪除成功');

                            modulesFunction.getTasksList();  
                            
                        }).error(function (jqXHR, textStatus, errorThrown) {
                              alert('刪除失敗');
                        });
              } else {
              }
                     
    },
    createCaptcha:function(){
     var alpha = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
    'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z', 
        '0','1','2','3','4','5','6','7','8','9');
     var i;
     for (i=0;i<6;i++){
         var a = alpha[Math.floor(Math.random() * alpha.length)];
         var b = alpha[Math.floor(Math.random() * alpha.length)];
         var c = alpha[Math.floor(Math.random() * alpha.length)];
         var d = alpha[Math.floor(Math.random() * alpha.length)];
                      }
         var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d;
         $("#captchaLB").text(code);
    },
    validCaptcha:function(){
       var string1 = modulesFunction.removeSpaces($('#captchaLB').text());
       var string2 = modulesFunction.removeSpaces($('#captcha').val());
       if (string1 == string2){
              return true;
       }else{        
              return false;
        }
    },
    removeSpaces:function(string){
         return string.split(' ').join('');
    },
    requiredInput: function() {
        var tempString= "";
        if ($("#keyinDate").val() == "") tempString += "選擇日期\n";
        if ($("#Surplus").val() == "" ) tempString += "當日營業額\n";
        if ($("#pass").val() == "") tempString += "分紅密碼\n";
        if ($("#captcha").val() == "") tempString += "驗證碼\n";
        return "以下資料為必填\n"+tempString;
    }
};

$(function(){
    modulesFunction.ui();
})

