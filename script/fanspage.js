var modules = {
    ui: function(){
        $("#loginFB").click(function(){
            loginFBEvent();
        });
    }   
};
function loginFBEvent()
{
    $.post('route/controller.php?action=getLoginURL', function(data, textStatus, xhr) {
            postData=data.data;
            switch(data.message){
              case "failed":
//                alert(data.errmsg);
                swal({
                    title: "系統錯誤！請聯繫管理員！",
                    text: "Oh...NO!",
                    icon: "error",
                    default: true
                });
              break;
              case "success":   
                window.location.href = postData.url;
              break;
              default:
              break;
            }
      },"json");
}
$(function(){
    modules.ui();
});