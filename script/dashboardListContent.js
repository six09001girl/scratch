var dashboardlistvalue = localStorage.getItem("dashboardlist");

var modulesFunction = {
	ui: function () {
		// modulesFunction.query.getActions();
		window.fbAsyncInit = function () {
			FB.init({
				appId: '1484604764929411',
				status: true,
				cookie: true,
				xfbml: true,
				oauth: true,
				version: 'v2.8'
			});

			FB.AppEvents.logPageView();
			// FB.Event.subscribe('edge.create', function(response) {
			//   alert('You liked the URL: ' + response);

			// });
		};

		(function (d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) { return; }
			js = d.createElement(s); js.id = id;
			js.src = "https://connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		$("#task_content").text().replace(/\r\n|\n|\r/g, '<br/>');
		$(".showmodal").each(function (index, el) {
			$(this).click(function (event) {
				$("#merchant").val($(this).attr('data-merchant'));
				$("#date").val($(this).attr('data-date'));
				$("#caption").val($(this).attr('data-caption'));
				$("#link").val($(this).attr('data-href'));
				$("#description").val($(this).attr('data-desp'));
				$("#oid").val($(this).attr('data-oid'));
			});
		});
		$('#dashboardlistmodal').on('hide.bs.modal', function (event) {
			$('#agreeBT').attr('checked', false);
		})
		$("#FBShareBt").click(function (event) {
			if ($("input:checked").length > 0) {
				modulesFunction.shareFB($("#link").val(), $("#caption").val(), $("#description").val());
			}
			else {
				alert("尚未勾選同意!");
			}

		});
	},
	shareFB: function (link, caption, description) {
		// FB.login(function(response) {
		// alert(response);
		// if (response.status === 'connected') {
		FB.ui({
			method: 'share',
			// mobile_iframe: true,
			href: link,
			caption: caption,
			description: description
			// method: 'share_open_graph',
			//   action_type: 'og.shares',
			//   action_properties: JSON.stringify({
			//       object : {
			//          'og:url': link,
			//          'og:title': caption,
			//          'og:description': description,
			//       }
			//   })
		}, function (resp) {
			switch (resp.error_message) {
				case 'User+canceled+the+Dialog+flow':
				case 'User canceled the Dialog flow':
					alert("尚未分享喔！");
					break;
				default:

					var dataAry = {
						oid: $("#oid").val(),
						date: $("#date").val(),
						content: $("#description").val(),
						title: $("#caption").val(),
						merchant: $("#merchant").val(),
						action_url: $("#link").val()
					};
					var settings = {
						"async": true,
						"crossDomain": true,
						"url": "/setPoint",
						"method": "POST",
						"headers": {
							"content-type": "application/x-www-form-urlencoded",
							"cache-control": "no-cache",
							"postman-token": "150fa44f-cf8c-0992-1192-832d5816a97f"
						},
						"data": dataAry
					};
					// $.post("/setPoint", dataAry, function (response) {
					// 	if (response == "limit out")
					// 		alert("分享成功！獲得 $" + Math.round(response.getcoin * 100) / 100 + "購物幣");
					// 	else if (response == "repeated")
					// 		alert("分享成功！獲得 $" + Math.round(response.getcoin * 100) / 100 + "購物幣");
					// 	else {
					// 		alert("分享成功！獲得 $" + Math.round(response.getcoin * 100) / 100 + "購物幣");
					// 		window.location.href = "./userDashBoard";
					// 	}
					// });
					$.ajax(settings).success(function (response) {
						alert("分享成功！獲得 $" + Math.round(response.getcoin * 100) / 100 + "購物幣");
						window.location.href = "./userDashBoard";
						// console.log(response);
					}).error(function (jqXHR, textStatus, errorThrown) {
						console.log('400 error')

						switch (jqXHR.responseText) {
							case 'NotyetSettlement':
								alert("今日系統尚未結算您的帳戶，請稍後數分鐘後再進行分享");
								break;
							case 'limit out':
								alert("今天分享次數獲得電商幣已達上限囉！");
								break;
							case 'repeated':
								alert("貼文重覆分享，請選取其他任務。");
								break;
							default:
								alert("異常錯誤，請通知管理員!");
								break;
						}
					});
					break;
			}
		});
		// } else {
		// alert("尚未登入Facebook進行分享！");
		// console.log("The person is not logged into this app or we are unable to tell. ");
		// }
		// }, {scope: 'public_profile,email'});

	},
	mobilecheck: function () {
		var check = false;
		(function (a, b) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true })(navigator.userAgent || navigator.vendor || window.opera);
		return check;
	},
	query: {
		getActions: function () {
			var settings = {
				"async": true,
				"crossDomain": true,
				"url": "/getAction",
				"method": "POST",
				"headers": {
					"content-type": "application/x-www-form-urlencoded",
					"cache-control": "no-cache",
					"postman-token": "150fa44f-cf8c-0992-1192-832d5816a97f"
				}
			}

			$.ajax(settings).success(function (response) {
				var str = "";
				for (var i = 0; i >= response.length; i++) {
					str += '<div class="col-md-4 col-sm-12"><div class="card text-white bg-info "><div class="card-body ">';
					str += '<h3 id="action_content">' + response[i].action_content + '</h3>';
					str += '<h3 id="task_content">' + response[i].task_content + '</h3></div>';
					str += '<a class="showmodal card-footer text-white clearfix" href="#" data-href="' + response[i].action_url + '" data-caption="' + response[i].action_content + '" data-desp="' + response[i].task_content + '" data-coin="' + response[i].gift[2].feedback + '" data-toggle="modal" data-target="#dashboardlistmodal">';
					str += '<span class="float-left">選用</span><span class="float-right"><i class="fa fa-angle-right"></i> </span></a></div></div>';
				}
				$("#dashboardlist").html(str);
			}).error(function (jqXHR, textStatus, errorThrown) {
				switch (jqXHR.status) {
					case 200:
						alert("資料讀取成功!");
						break;
					default:
						alert("資料載入錯誤!");
						break;
				}
			});
		}
	}
};

// $(function(){
modulesFunction.ui();
// })