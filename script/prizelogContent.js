var _id = localStorage.getItem("_id");
var modulesFunction = {
    ui: function(){      
        //兌換時間起迄
        $('#redemptionStart').datetimepicker({
            useCurrent: true,
            format: 'YYYY-MM-DD'
        });
        $('#redemptionFinish').datetimepicker({
            useCurrent: false, //Important! See issue #1075
            format: 'YYYY-MM-DD'
        });
        $("#redemptionStart").on("dp.change", function (e) {
            $('#redemptionFinish').val($('#redemptionStart').val());
            $('#redemptionFinish').data("DateTimePicker").minDate(e.date);
        });
        $("#redemptionFinish").on("dp.change", function (e) {
            $('#redemptionStart').data("DateTimePicker").maxDate(e.date);
        });
        
        
        $('#addPrizeModal').on('hidden.bs.modal', function() {
            $('#addPrizeForm')[0].reset();
            $('#addPrizeForm .form-group').removeClass('has-error has-feedback');
            $('#addPrizeForm').find('span.text-danger').remove();
            $('#prizeContent').html("");
        });
        
        $("#SearchBT").click(function(e){
            modulesFunction.searchEvent();
          });
        
    },
    gotolist: function(){
       if ( $.fn.dataTable.isDataTable( '#actListTable' ) ) {
            $('#actListTable').DataTable();
        }
        else
        {
            $('#actListTable').DataTable({
                "bProcessing": true,
                "sAjaxSource": "route/controller.php?action=getActList",
                "aoColumns": [
                    { "mData": "ACTSTATUS", "sWidth": '10%',
                        "mRender":function(val,type,row){
                            switch(val)
                            {
                                case '0':
                                    return '<span class="label label-info">尚未進行</span>';
                                    break;
                                case '1':
                                    return '<span class="label label-success">進行中</span>';
                                    break;
                                case '2':
                                    return '<span class="label label-default">已結束</span>';
                                    break;
                                default:
                                    return false;
                                    break;
                            }
                        }},
                    { "mData": "ACTTITLE", "sWidth": '20%'},
                    { "mData": "ACTTYPE",
                        "mRender":function(val,type,row){
                            switch(val)
                            {
                                case '0':
                                    return '<span class="label label-online">線上</span>';
                                    break;
                                case '1':
                                    return '<span class="label label-offline">線下</span>';
                                    break;
                                default:
                                    return false;
                                    break;
                            }
                        }},
                    { "mData": "ACTSTARTDATE", "sWidth": '10%'},
                    { "mData": "ACTENDDATE", "sWidth": '10%'},
                    { "mData": "AWARDSTARTDATE", "sWidth": '10%'},
                    { "mData": "AWARDENDDATE", "sWidth": '10%'},
                    { "mData": "ITEMS"},
                    { "mData": "SER", "sWidth": '15%' ,
                          "mRender":function(val,type,row) {
                            return "<button class='btn btn-info btn-circle' data-toggle='modal' data-target='#infoPrizeModal' onclick='modulesFunction.infoEvent(\""+val.toString()+"\")'><span class='fa fa-info-circle' aria-hidden='true'></span></button><button class='btn btn-warning btn-circle' data-toggle='modal' data-target='#editPrizeModal' onclick='modulesFunction.loadEvent(\""+val.toString()+"\")'><span class='fa fa-pencil' aria-hidden='true'></span></button><button class='btn btn-danger btn-circle' onclick='modulesFunction.deleteEvent(\""+val.toString()+"\")'><span class='fa fa-trash' aria-hidden='true'></span></button><a href='#' class='btn btn-default' onclick='modulesFunction.setpagelink($(this))' target='_blank' >前往活動頁面</a>";
                          }
                    }
                ],
                "order": [[ 0, "desc" ]],
                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {

                    $(nRow).attr("data-id",aData.STORECODE);
                    $(nRow).attr("data-actser",aData.SER);
                    return nRow;
                },
                "language": {
                    "processing": "資料載入中...",
                    "lengthMenu": "每頁顯示 _MENU_ 筆",
                    "zeroRecords": "資料庫中未有相關資料。",
                    "info": "第 _PAGE_ 頁，共 _PAGES_ 頁",
                    "infoEmpty": "資料庫中未有相關資料。",
                    "search":  "搜尋:",
                    "paginate": {
                          "first":      "第一頁",
                          "last":       "最後一頁",
                          "next":       "下一頁",
                          "previous":   "上一頁"
                      }
                }
            });
        }
    },
    searchEvent: function(){
        $('body').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
        $.ajax({
            type: 'POST',
            url: 'route/controller.php?action=searchAwardLogs',
            data: new FormData($("#searchForm")[0]),
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false
        }).complete(function(data) {
//                console.log(data);
//                console.log(JSON.stringify(data.responseText.replace(' ','')));
            $( "#loadingDiv" ).fadeOut(500, function() {
                  $( "#loadingDiv" ).remove(); //makes page more lightweight 
            });
            var str = "";
            switch(data.responseJSON.message){
                case "failed":
                    str+="<tr>";
                    str+="<td colspan='6'>資料庫中未有相關資料。</td>";
                    str+="</tr>";
                    $("#awardListTable tbody").html(str);
                break;
                case "success": 
                    for(var i=0; i<data.responseJSON.data.length; i++)
                    {
                        str+="<tr>";
                        str+="<td><img src='"+((data.responseJSON.data[i].IMGURL)?data.responseJSON.data[i].IMGURL:"https://via.placeholder.com/36x36")+"' style='width:36px;height:36px;'>"+data.responseJSON.data[i].PRIZENAME+"</td>";
                        str+="<td>1</td>";
                        str+="<td>"+data.responseJSON.data[i].ACTTITLE+"</td>";
                        str+="<td>"+data.responseJSON.data[i].CHANGER+"</td>";
                        str+="<td>"+data.responseJSON.data[i].REDEEMDATE+"</td>";
                        str+="<td><button class='btn btn-info btn-circle' data-toggle='modal' data-target='#infoModal' onclick='modulesFunction.loadEvent(\""+data.responseJSON.data[i].SER+"\")'><span class='fa fa-info-circle' aria-hidden='true'></span></button></td>"
                        str+="</tr>";
                    }
                    $("#awardListTable tbody").html(str);    
                break;
            }
            
            
          });
    },
    loadEvent: function(_id){
      var dataAry = {
          ser:_id
        };
        $.post('route/controller.php?action=loadAwardLog', dataAry, function(data, textStatus, xhr) {
                postData=data.data;
                switch(data.message){
                  case "failed":
    //                alert(data.errmsg);
                    swal({
                        title: "資料載入失敗！",
                        text: "Oh...NO!"+data.errmsg,
                        icon: "error",
                        default: true
                    });
                  break;
                  case "success":   
                    $("#changename").text(postData[0].CHANGER);
                    $("#changetel").text((postData[0].PHONE)?postData[0].PHONE:"");
                    $("#changeaddr").text((postData[0].ADDR)?postData[0].ADDR:"");
                    $("#redeemdate").text(postData[0].REDEEMDATE);
                    $("#acttitle").text(postData[0].ACTTITLE);
                    $("#prizeimg").attr('src',((postData[0].IMGURL)?postData[0].IMGURL:'https://via.placeholder.com/120x100'));
                    $("#prizename").text(postData[0].PRIZENAME);
                  break;
                  default:
                  break;
                }
          },"json").done(function() {
            $("#SaveActivityBT").button('reset');
          });  
    },
    saveEvent: function(_id){
        $('body').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
        $.ajax({
            type: 'POST',
            url: 'route/controller.php?action=editAct',
            data: new FormData($("#editPrizeForm")[0]),
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false
        }).complete(function(data) {
//                console.log(data);
//                console.log(JSON.stringify(data.responseText.replace(' ','')));
            $( "#loadingDiv" ).fadeOut(500, function() {
                  $( "#loadingDiv" ).remove(); //makes page more lightweight 
            });
            switch(data.responseJSON.message){
                case "failed":
                  swal({
                        title: "儲存失敗！",
                        text: "Oh...NO!"+data.responseJSON.errmsg,
                        icon: "error",
                        default: true
                    });
                break;
                case "success": 
                  swal({
                        title: "儲存成功!",
                        text: "Good Job!"+data.responseJSON.errmsg,
                        icon: "success",
                        default: true
                    }).then((value) => {                            
                        $("#editPrizeForm")[0].reset();
                      $('#editPrizeModal').modal('hide'); $('#e-prizeContent').html("");
                    }); 
                    $('#actListTable').DataTable().destroy();
                    modulesFunction.gotolist();
                break;
            }
            $("#SaveActivityBT").button('reset');
          });
    },
    infoEvent: function(_id){
    $("#i-prizetbody").html("");
    $('body').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
      var dataAry = {
          ser:_id
        };
        $.post('route/controller.php?action=loadAct', dataAry, function(data, textStatus, xhr) {
                postData=data.data;
                switch(data.message){
                  case "failed":
    //                alert(data.errmsg);
                    swal({
                        title: "資料載入失敗！",
                        text: "Oh...NO!"+data.errmsg,
                        icon: "error",
                        default: true
                    });
                  break;
                  case "success":   
                    $("#i-actstatus").removeClass('label label-info label-success label-default');
                    switch(postData[0].ACTSTATUS)
                    {
                        case "0":
                            $("#i-actstatus").text("尚未進行").addClass('label label-info');
                            break;
                        case "1":
                            $("#i-actstatus").text("進行中").addClass('label label-success');
                            break;
                        case "2":
                            $("#i-actstatus").text("已結束").addClass('label label-default');
                            break;
                        default:
                            $("#i-actstatus").text("資料有誤！");
                            break;
                    }
                    $("#i-acttitle").text(postData[0].ACTTITLE);
                    $("#i-acttype").removeClass('label label-online label-offline');
                    switch(postData[0].ACTTYPE)
                    {
                        case "0":
                            $("#i-acttype").text("線上").addClass('label label-online');
                            break;
                        case "1":
                            $("#i-acttype").text("線下").addClass('label label-offline');
                            break;
                        default:
                            $("#i-acttype").text("資料有誤！");
                            break;
                    }
                    $("#i-activityStart").text(postData[0].ACTSTARTDATE);
                    $("#i-activityFinish").text(postData[0].ACTENDDATE);
                    $("#i-activitydesp").html(postData[0].ACTDESP.replace(/\r\n|\n|\r/g,"<br/>"));
                    $("#i-redemptionStart").text(postData[0].AWARDSTARTDATE);
                    $("#i-redemptionFinish").text(postData[0].AWARDENDDATE);
                    $("#i-redempdesp").html(postData[0].AWARDDESP.replace(/\r\n|\n|\r/g,"<br/>"));
                    $("#i-prizenum").text(parseInt(postData[0].ITEMS));
                    switch(postData[0].NOPRIZE)
                    {
                        case "0":
                            $("#i-noprize1").attr("checked", true);
                            break;
                        case "1":
                            $("#i-noprize2").attr("checked", true);
                            break;
                    }
                    
                    if(parseInt(postData[0].ITEMS) != 0)
                    {
                        $poststr = '';
                        for(var i=0 ; i < postData.length ; i++)
                        {
                            $poststr += '<tr>';
                            $poststr += '<td>'+(i+1)+'</td>'
                            $poststr += '<td><img src="'+postData[i].IMGURL+'" class="img-thumbnail" style="max-width: 120px;max-height: 120px;"/></td>';
                            $poststr += '<td>'+postData[i].PRIZENAME+'</td>';
                            $poststr += '<td>'+parseInt(postData[i].TOTALS)+'</td>';
                            $poststr += '<td>'+parseInt(postData[i].PERCENT)+'%</td>';      
                        }
                        $("#i-prizetbody").html($poststr);
                    }
                  break;
                  default:
                  break;
                }
          },"json").done(function() {
            $( "#loadingDiv" ).fadeOut(500, function() {
                  $( "#loadingDiv" ).remove(); //makes page more lightweight 
            });
          });  
    },
    deleteEvent: function(_id){
        swal({
          title: "你確定要刪除嗎?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
                $('body').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
                var dataAry = {
                  ser:_id
                };
                $.post('route/controller.php?action=deleteAct', dataAry, function(data, textStatus, xhr) {
                        $( "#loadingDiv" ).fadeOut(500, function() {
                              $( "#loadingDiv" ).remove(); //makes page more lightweight 
                        });
                        postData=data.data;
                        switch(data.message){
                          case "failed":
            //                alert(data.errmsg);
                            swal({
                                title: "刪除失敗！",
                                text: "Oh...NO!",
                                icon: "error",
                                default: true
                            });
                          break;
                          case "success":   
                            swal({
                                title: "刪除成功!",
                                text: "Good Job!",
                                icon: "success",
                                default: true
                            }); 
                            $('#actListTable').DataTable().destroy();
                            modulesFunction.gotolist();
                          break;
                          default:
                          break;
                        }
                  },"json");  
          }
        });        
    },
    calcPercent:function(){  
        var percentSum = 0;
        for (var i = 0; i < $('.percent').length ; i++) {
            percentSum+=parseInt($('.percent')[i].value);
        }
//        console.log(percentSum);
        if(percentSum != 100)
        {
            $('.calcPercentText').text("設定總比例須滿足100%。");
        }
        else
        {
            $('.calcPercentText').text("");
        }
    },
    previewEvent:function(obj){
//        console.log(obj[0].name.split('-')[1]);
//        return false;
        var tempnum = obj[0].name.split('-')[1];
        if(obj[0].files && obj[0].files[0]){
            var readerimg = new FileReader();
            var img = new Image();
            var w=0,h=0,s=0;
            readerimg.onload = function (e) {
                img.src = e.target.result
                s = e.total;
            }

            readerimg.readAsDataURL(obj[0].files[0]);

            img.onload = function() {
                w = this.width;
                h = this.height;

                if((w/h) <= 1 && (w/h) >= 1 && w >= 300 && h >= 300)
                {
                    if((s/500000) <= 1)
                    {
                        $(".preview_image-"+tempnum).attr('src', img.src);
                    }
                    else
                    {
                        swal({
                            title: "圖片大小("+(s/500000)+"MB)已超過1MB，請重新上傳。",
                            text: "Oh...Sorry!",
                            icon: "error",
                            default: true
                        });
                        obj.replaceWith(obj.val('').clone(true));
                    }

                }
                else 
                {            
                    swal({
                        title: "圖片長寬(長："+w+",寬："+h+")不符，請重新上傳。",
                        text: "Oh...Sorry!",
                        icon: "error",
                        default: true
                    });
                    obj.replaceWith(obj.val('').clone(true));
                }
            }

          }
    },
    checkInputRequired:function(){
        $("form input[required]").change(function(e){
            modulesFunction.validateEvent($(this));
        });
        $("form input[required]").focusin(function(e){
            modulesFunction.validateEvent($(this));
        });
    },
    validateEvent:function(obj){
        if(!obj.val())
        {
            obj.closest('.form-group').find('span.text-danger').remove();
            obj.after('<span class="text-danger">必填</span>');            
            obj.closest('.form-group').addClass('has-error has-feedback');
        } 
        else
        {
            obj.closest('.form-group').find('span.text-danger').remove();
            obj.closest('.form-group').removeClass('has-error has-feedback');   
        }
    },
    randomString: function(len){
            len = len || 30;
          var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
          var maxPos = $chars.length;
          var pwd ='';
          for (i = 0;i < len; i++ ) {
            pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
          }
          return pwd;
    }
};
$(function(){
    modulesFunction.ui();
    
})

