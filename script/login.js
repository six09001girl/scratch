var modules = {
    ui: function(){      
      // if(localStorage.getItem('_id') != null)
      //   {
      //       window.location.href = "./dashboardContent";
      //   }
     
      $('#registerModal').on('hidden.bs.modal', function (e) {
        $("#registerForm")[0].reset();
      })
      $("form input").keydown(function(event) {
        modules.keyDownEvent(event);
      });
      $("#LoginBt").click(function(event) {
         modules.loginEvent();
      });
      $("#RegisterBt").click(function(event) {
          if(modules.requiredInput().length > 8)
          {
              swal({
                    title: "填寫錯誤!",
                    text: modules.requiredInput(),
                    icon: "error",
                });
          }
           else
           {
                if(modules.regexpInput().length > 10)
                {
                    swal({
                        title: "填寫錯誤!",
                        text: modules.regexpInput(),
                        icon: "error",
                    });
                }
                else
                {
                  modules.registerEvent();
                }
            }
      });
       
    },
    keyDownEvent: function(event){
          var x = event.keyCode;
            if (x == 13) {  // 13 is the Enter ke
                modules.loginEvent();
            }
    },
    loginEvent: function(){
        var dataAry = {
          usercode:$("#account").val(),
          userpsw:$("#password").val()
        };

        $.post('route/controller.php?action=login', dataAry, function(data, textStatus, xhr) {
                  switch(data.message){
                    case "failed":
    //                  alert(data.errmsg);
                        swal({
                            title: "登入失敗!",
                            text: data.data,
                            icon: "error",
                            default: true
                        });

                    break;
                    case "success":
                        swal({
                            title: "登入成功!",
                            text: "Welcome!",
                            icon: "success",
                            default: true,
                            button:false,
                            timer: 1000
                        }).then(function(){
                            localStorage.setItem("_name", data.data[0].USERNAME);
                            window.location.href = data.returnUrl;
                        }); 
                        
                    break;
                    default:
                    break;
                  }
        },"json");
    },  
    registerEvent: function(){
      if($("#pass").val() == $("#re_pass").val())
      {
          var dataAry = {
            industry:$("#industry option:selected").val(),
            usercode:$("#registerEmail").val(),
            fburl:$("#fb_url").val(),
            fbname:$("#fbname").val(),
            tel:$("#tel").val(),
            userpsw:$("#pass").val()
          };
          $.post('route/controller.php?action=addUser', dataAry, function(data, textStatus, xhr) {
                  switch(data.message){
                    case "failed":
    //                  alert(data.errmsg);
                        swal({
                            title: "註冊失敗!",
                            text: data.data,
                            icon: "error",
                            default: true
                        }).then((value) => {                            
                            $("#registerForm")[0].reset();
                        });

                    break;
                    case "success":
                        swal({
                            title: "註冊成功!",
                            text: "Welcome!",
                            icon: "success",
                            default: true
                        }).then((value) => {
                            $('#registerModal').modal('hide');
                            $("#registerForm")[0].reset();
                        });   
                        
                    break;
                    default:
                    break;
                  }
        },"json");
      }
      else
      {
          swal({
                title: "注意!",
                text: "確認密碼與密碼不符!",
                icon: "error",
            });
      }        
    },
    requiredInput: function() {
        var tempString= "";
       
        if ($("#registerEmail").val() == "") tempString += "Email\n";
        // if ($("#fb_url").val() == "") tempString += "FB粉絲專業網址\n";
        // if ($("#fb_id").val() == "") tempString += "FB粉絲專頁名稱\n";
        if ($("#tel").val() == "") tempString += "手機號碼\n";        
        if ($("#pass").val() == "") tempString += "密碼\n";
        if ($("#re_pass").val() == "") tempString += "確認密碼\n";
        
        return "以下資料為必填\n"+tempString;
    },
    regexpInput:function(){
        var tempString2= "";
        var rule1 =  /^[A-Z]\d{9}$/; //身分證
        var rule2 = /^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})*$/; //email
        var rule3 = /^0\d{3}\d{3}\d{3}$/; //手機號碼
        //if(!rule1.test($("#userID").val())) tempString2 += "身分證字號\n";
        // if(rule2.test($("#email").val())) tempString2+= "Email\n";
        if(!rule3.test($("#tel").val())) tempString2 += "手機號碼\n";
       
        return "以下資料未符合格式\n"+tempString2;
    }
};

$(function(){        
      modules.ui();

    // if(isMobile())
    // {
    //   alert("行動版會員登入維修中，請至電腦上進行做作業!")
    // }else{
    //   window.location.href = "http://www.comego.com.tw/service/54869597/web/index.html";
    // }
});