var modules = {
    ui: function(){
        $("#fanpagetitle").text(getargs().name);
        
        $('#articlemsgModal').on('hidden.bs.modal', function (e) {
          $("#loadmsg").html('loading...');
        });
        
        var dataAry = {
            id:getargs().id,
            accode:getargs().accode
          };
        $('body').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
          $.post('route/controller.php?action=loadArticlemsg', dataAry, function(data, textStatus, xhr) {
              console.log(data);
              str = "";
                  switch(data.message){
                    case "failed":
    //                  alert(data.errmsg);
                        swal({
                            title: "失敗!",
                            icon: "error",
                            default: true
                        }).then((value) => {                            
                            window.location.href="./Fanspages?param1=index";
                        });

                    break;
                    case "success":
                        
                        for(var i=0; i<data.data.postArticleData.length; i++)
                        {
                            str += "<tr data-id='"+data.data.postArticleData[i].id+"' onclick='loadLeavemsgEvent($(this))'>";
                            str += "<td>"+data.data.postArticleData[i].created_time.substr(0,19)+"</td>";
                            str += "<td>"+data.data.postArticleData[i].message+"</td>";
                            str += "</tr>";
                        }

                        $('#loadArticleTable tbody').html(str);
                    break;
                    default:
                    break;
                  }
        },"json").done(function(){
            $( "#loadingDiv" ).fadeOut(500, function() {
                  $( "#loadingDiv" ).remove(); //makes page more lightweight 
            });
        });
        
    }
}
function getargs(){
    var url = window.location.search; 
    var args = new Object();   
    if (url.indexOf("?") != -1) {   
          var str = url.substr(1);   
          strs = str.split("&");   
          for(var i = 0; i < strs.length; i ++) { 
             args[strs[i].split("=")[0]]=decodeURI(strs[i].split("=")[1]); 
          }   
    } 
    return args;
}
function loadLeavemsgEvent(obj)
{
    $('#articlemsgModal').modal('show');
    var dataAry = {
        articleid:obj.attr('data-id'),
        pageaccode:getargs().accode
      };
    $('#articlemsgModal').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
      $.post('route/controller.php?action=loadLeavemsg', dataAry, function(data, textStatus, xhr) {
          str = "";
              switch(data.message){
                case "failed":
//                  alert(data.errmsg);
                    swal({
                        title: "失敗!",
                        icon: "error",
                        default: true
                    }).then((value) => {                            
                        $("#loadmsg").html('');
                    });

                break;
                case "success":
                    if(data.data.postLeavemsgData.length == 0)
                    {
                        $("#loadmsg").html('<p>未有相關留言訊息！</p>');
                        break;
                    }
                    for(var i=0; i<data.data.postLeavemsgData.length; i++)
                    {
//                        str += "<tr data-id='"+data.data.postArticleData[i].id+"'>";
//                        str += "<td>"+data.data.postArticleData[i].created_time.substr(0,19)+"</td>";
//                        str += "<td>"+data.data.postArticleData[i].message+"</td>";
//                        str += "</tr>";
                        
                        str += '<div class="media">';
                        str += '<div class="media-left"><span class="fa fa-user-circle fa-2x img-circle"></span></div>';
                        str += '<div class="media-body">';
                        str += '<h4 class="media-heading">'+data.data.postLeavemsgData[i].from.name+'</h4>';
                        str += '<p>'+data.data.postLeavemsgData[i].message+'</p>';
                                                
                        if(data.data.postLeavemsgData[i].sec_msg)
                        {
                            for(var j=data.data.postLeavemsgData[i].sec_msg.data.length - 1; j>=0; j--)
                            {
                                str += '<div class="media">';
                                str += '<div class="media-left"><span class="fa fa-user-circle fa-2x img-circle"></span></div>';
                                str += '<div class="media-body">';
                                str += '<h4 class="media-heading">'+data.data.postLeavemsgData[i].sec_msg.data[j].from.name+'</h4>';
                                str += '<p>'+data.data.postLeavemsgData[i].sec_msg.data[j].message+'</p>';      
                                str += '</div></div>';
                                
                            }
                        }
                        str += '</div>';
                        if(data.data.postLeavemsgData[i].from.id != getargs().id)
                        {
                            str += '<div class="input-group"><input type="text" class="form-control replymsgtxt" placeholder="回覆..."><span class="input-group-btn"><button class="btn btn-default" type="button" style="height: 34px;" onclick="replymsgEvent(\''+data.data.postLeavemsgData[i].id+'\', $(this))"><span class="fa fa-paper-plane"></span></button></span></div>';
                        }
                        str += '</div>';
                        
                        
                    }
                    console.log(data);                 
                    $("#loadmsg").html(str);
                default:
                break;
              }
    },"json").done(function(){
        $( "#loadingDiv" ).fadeOut(500, function() {
              $( "#loadingDiv" ).remove(); //makes page more lightweight 
        });
    });

}

function replymsgEvent(msgid, obj)
{    
    var dataAry = {
        leavemsgid:msgid,        
        pageaccode:getargs().accode,
        message: obj.parent().parent().find('.replymsgtxt').val()
      };
    $('#articlemsgModal').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
      $.post('route/controller.php?action=replayLeavemsg', dataAry, function(data, textStatus, xhr) {
          console.log(data);
          str = "";
              switch(data.message){
                case "failed":
//                  alert(data.errmsg);
                    swal({
                        title: "訊息發送失敗!",
                        icon: "error",
                        default: true
                    });

                break;
                case "success":
                    swal({
                        title: "訊息發送成功!",
                        icon: "success",
                        default: true
                    }).then((value) => {                            
                        $('#articlemsgModal').modal('hide');
                    });
                    
                break;
                default:
                break;
              }
    },"json").done(function(){
        $( "#loadingDiv" ).fadeOut(500, function() {
              $( "#loadingDiv" ).remove(); //makes page more lightweight 
        });
    });
}

$(function(){
    modules.ui();
    
});