var addTasksModalbackup = $('#addTasks').clone();
var modulesFunction = {
    ui: function(){             
		modulesFunction.getTasksList();   
          
          $("#AddTasksBT").click(function(event) {
                 // $('#editUserForm').validator().submit(function(e) {
                 //    if (e.isDefaultPrevented()) {
                 //      alert("請依欄位指示填寫。");
                 //    } else {
                 //      modulesFunction.saveedEvent();
                 //    }
                 //  })
                    if(modulesFunction.requiredInput().length > 8)
                      {
                          alert(modulesFunction.requiredInput());
                      }
                       else
                       {
                            if(modulesFunction.regexpInput().length > 10)
                            {
                                alert(modulesFunction.regexpInput());
                            }
                            else
                            {
                              if($("#action_url").val().split("posts/").length < 2)
                              {
                                alert("連結錯誤，為符合格式。");
                              }
                              else
                              {
                                modulesFunction.savedEvent();
                              }
                            }
                        }
              })
        },
    getTasksList: function(level){
       var settings = {
          "async": true,
          "crossDomain": true,
          "url": "/getAction",
          "method": "POST",
          "headers": {
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            "postman-token": "150fa44f-cf8c-0992-1192-832d5816a97f"
          }
        }

        $.ajax(settings).success(function(response){
            var str="";
            if(response.length <= 0)
            {
              str+="<tr><td colspan='5'>資料庫中未有相關資料。</td></tr>";
            }
            else
            {
              for (var i = 0; i < response.length; i++) {
                      str+="<tr data-id='"+ response[i]._id +"'>";
                      str+="<td>" + response[i].merchant+ "</td>";
                      str+="<td>" + response[i].title+ "</td>";
                      str+="<td>" + response[i].content+ "</td>";
                      str+="<td><a class='btn btn-default action_link' href='" + response[i].action_url+ "' target='_blank'>開啟連結</a></td>";
                      str+="<td><button class='btn btn-danger btn-lg' onclick='modulesFunction.deleteEvent(\""+response[i]._id.toString()+"\")'><span class='fa fa-trash' aria-hidden='true'></span></button> </td>";
                      str+="</tr>";
                }
            }
            
          $("#tasksListTable tbody").html(str);
          $("#tasksListTable").DataTable();
        }).error(function (jqXHR, textStatus, errorThrown) {
          switch(jqXHR.status){
            case 200:
              alert("資料讀取成功!");
            break;
            default:
              alert("資料載入錯誤!");
            break;
          }
        });
    },
    savedEvent: function(){
        var dataAry = {
            merchant:$("#merchant").val(),
            title:$("#title").val(),
            content:$("#content").val(),
            action_url:$("#action_url").val()
        };
        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "/addAction",
          "method": "POST",
          "headers": {
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            "postman-token": "150fa44f-cf8c-0992-1192-832d5816a97f"
          },
          "data":dataAry
        };
                  $.ajax(settings).success(function(response){
                        alert('新增成功');
                        $('#addTasks').modal('hide');
                        $('#addTasks').on('hide.bs.modal', function (event) {
                          $(this).remove();
                        })
                        var addTasksModalclone = addTasksModalbackup.clone();
                        $('#TasksContent').append(addTasksModalclone);

                        modulesFunction.getTasksList();  
                        
                    }).error(function (jqXHR, textStatus, errorThrown) {
                          alert('新增失敗');
                    });
        
    },
    deleteEvent: function(_id){
        var dataAry = {
          action_id: _id
        };
        var settings = {
              "async": true,
              "crossDomain": true,
              "url": "/delAction",
              "method": "POST",
              "headers": {
                "content-type": "application/x-www-form-urlencoded",
                "cache-control": "no-cache",
                "postman-token": "150fa44f-cf8c-0992-1192-832d5816a97f"
              },
              "data":dataAry
            };

            if (confirm("是否確定進行刪除？")) {
                  $.ajax(settings).success(function(response){
                            alert('刪除成功');

                            modulesFunction.getTasksList();  
                            
                        }).error(function (jqXHR, textStatus, errorThrown) {
                              alert('刪除失敗');
                        });
              } else {
              }
                     
    },
    requiredInput: function() {
        var tempString= "";
        if ($("#merchant").val() == "") tempString += "廠商\n";
        if ($("#title").val() == "") tempString += "標題\n";
        if ($("#content").val() == "" ) tempString += "內容\n";
        if ($("#action_url").val() == "") tempString += "連結\n";
        return "以下資料為必填\n"+tempString;
    },
    regexpInput:function(){
        var tempString2= "";
        var rule1 =  /(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/; //URL
        if(!rule1.test($("#action_url").val())) tempString2 += "連結\n";
       
        return "以下資料未符合格式\n"+tempString2;
    }
};

$(function(){
    modulesFunction.ui();
})

