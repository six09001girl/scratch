function init()
{
    $('body').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
    $.post('route/controller.php?action=getFanspages', function(data, textStatus, xhr) {
            postData=data.data.data;
            str = "";
            switch(data.message){
              case "failed":
//                alert(data.errmsg);
                swal({
                    title: "系統錯誤！請聯繫管理員！",
                    text: "Oh...NO!",
                    icon: "error",
                    default: true
                });
              break;
              case "success": 
                for(var i=0 ; i<postData.length; i++)
                {
                    str += "<tr>";
                    str += "<td>"+postData[i].name+"</td>";
//                    str += "<td><button class='btn btn-warning' data-name='"+postData[i].name+"' data-id='"+postData[i].id+"' data-accode='"+postData[i].access_token+"'  data-toggle='modal' data-target='#postmsgModal'>發文</button></td>";
                    str += "<td><a href='Fanspages?param1=articlelist&id="+postData[i].id+"&accode="+postData[i].access_token+"&name="+postData[i].name+"' class='btn btn-warning' data-name='"+postData[i].name+"' id='loadArticleBT'>回覆留言</a>  <a href='Fanspages?param1=setautoreply&id="+postData[i].id+"&accode="+postData[i].access_token+"&name="+postData[i].name+"' class='btn btn-info' data-name='"+postData[i].name+"'>設定自動回覆訊息</a></td>";
                    str += "</tr>";
                }
                $("#fanspageListTable tbody").append(str);
                $("#fanspageListTable").DataTable();
//                    postData.forEach(function(element) {
//                      console.log("id:"+element.id + "\n name:" + element.name + "\n access_code:" + element.access_token);
//                    });
              break;
              default:
              break;
            }
      },"json").done(function(){
        $( "#loadingDiv" ).fadeOut(500, function() {
              $( "#loadingDiv" ).remove(); //makes page more lightweight 
        });
    });
}
var modules = {
    ui: function(){
        init();
        
        $('#postmsgModal').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget);
          var fansname = button.data('name');
            var fansid = button.data('id');
            var fanscode = button.data('accode');
          var modal = $(this)
          modal.find('#fanspagename').text(fansname);
            modal.find('#fansid').val(fansid);
            modal.find('#fanscode').val(fanscode);
        });
        
        $("#postBT").click(function(){
            postEvent();
        });
        
    }
}
function postEvent()
{
    var dataAry = {
        id:$("#fansid").val(),
        name:$("#fanspagename").text(),
        accesscode:$("#fanscode").val(),
        message:$("#postmessage").val()
      };
        $('#postBT').button('loading');
      $.post('route/controller.php?action=post2Fanspage', dataAry, function(data, textStatus, xhr) {
          console.log(data);
              switch(data.message){
                case "failed":
//                  alert(data.errmsg);
                    swal({
                        title: "失敗!",
                        text: data.errmsg.replace(/\r\n|\n|\r/g,"<br/>"),
                        icon: "error",
                        default: true
                    }).then((value) => {                            
                        $("#postmsgForm")[0].reset();
                    });

                break;
                case "success":
                    swal({
                        title: "成功!",
                        text: data.errmsg.replace(/\r\n|\n|\r/g,"<br/>"),
                        icon: "success",
                        default: true
                    }).then((value) => {
                        $('#postmsgModal').modal('hide');
                        $("#postmsgForm")[0].reset();
                    });   

                break;
                default:
                break;
              }
    },"json").done(function(){
          $('#postBT').button('reset');
      });

}

function loadArticleEvent(obj)
{
    var dataAry = {
        id:obj.attr('data-id'),
        accode:obj.attr('data-accode')
      };
    $('#loadArticle tbody').loading();
      $.post('route/controller.php?action=loadArticlemsg', dataAry, function(data, textStatus, xhr) {
          console.log(data);
          str = "";
              switch(data.message){
                case "failed":
//                  alert(data.errmsg);
                    swal({
                        title: "失敗!",
                        icon: "error",
                        default: true
                    }).then((value) => {                            
                        $("#postmsgForm")[0].reset();
                    });

                break;
                case "success":
                    for(var i=0; i<data.data.postArticleData.length; i++)
                    {
                        str += "<tr data-id='"+data.data.postArticleData[i].id+"'>";
                        str += "<td>"+data.data.postArticleData[i].created_time.substr(0,19)+"</td>";
                        str += "<td>"+data.data.postArticleData[i].message+"</td>";
                        str += "</tr>";
                    }
                                        
                    $('#loadArticle tbody').html(str);
                break;
                default:
                break;
              }
    },"json").done(function(){
        $('#loadArticle tbody').loading('stop');
    });

}
function unconnectEvent(){
    $.post('route/controller.php?action=unConnectFB', function(data, textStatus, xhr) {         window.location.href = data.errmsg;
    },"json");
}
$(function(){
    modules.ui();
    
});