var editUserModalbackup = $('#editUser').clone();

var modulesFunction = {
    ui: function(){             
		modulesFunction.getAccountList();   
    $('.updateUserModal').on('hidden.bs.modal', function (event) {
      $("#pass, #re_pass").val("");
    })
          var $pass = $('#pass'), 
                $show_pass = $('<input type="text" name="' + $pass.attr('name') + '" class="' + $pass.attr('class') + '"   />');
          $('#show_pass').click(function(){
              if(!$(this).hasClass('showedPass')){
                $pass.replaceWith($show_pass.val($pass.val()));
                $(this).addClass('showedPass');
              }else{
                $show_pass.replaceWith($pass.val($show_pass.val()));
                $(this).removeClass('showedPass');
              }
            });
         
          $("#SaveBT").click(function(event) {
                 // $('#editUserForm').validator().submit(function(e) {
                 //    if (e.isDefaultPrevented()) {
                 //      alert("請依欄位指示填寫。");
                 //    } else {
                 //      modulesFunction.saveedEvent();
                 //    }
                 //  })
                  if(modulesFunction.requiredInput().length > 8)
                    {
                        alert(modulesFunction.requiredInput());
                    }
                     else
                     {
                          if(modulesFunction.regexpInput().length > 10)
                          {
                              alert(modulesFunction.regexpInput());
                          }
                          else
                          {
                            modulesFunction.savedEvent();
                          }
                      }
              })
        },
    getAccountList: function(level){
       var settings = {
          "async": true,
          "crossDomain": true,
          "url": "/getAccountList",
          "method": "POST",
          "headers": {
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            "postman-token": "150fa44f-cf8c-0992-1192-832d5816a97f"
          }
        }

        $.ajax(settings).success(function(response){
            var str="";
            if(response.length <= 0)
            {
              str+="<tr><td colspan='6'>資料庫中未有相關資料。</td></tr>";
            }
            else
            {
              for (var i = 0; i < response.length; i++) {
                    if(response[i].level == level)
                    {
                      str+="<tr data-id='"+ response[i]._id +"'>";
                      str+="<td>" + response[i].fb_id+ "</td>";
                      str+="<td><a class='btn btn-default' target='_blank' href='" + response[i].fb_url+ "'>前往粉專</a></td>";
                      str+="<td>" + response[i].phone+ "</td>";
                      str+="<td>" + response[i].user + "/\n" + "</td>";
                      str+="<td><span class='label label-warning'>" + response[i].industry+ "</span></td>";
                      str+="<td><button class='btn btn-info btn-lg' data-toggle='modal' data-target='#editUser' onclick='modulesFunction.editEvent(\""+response[i]._id.toString()+"\")'><span class='fa fa-pencil' aria-hidden='true'></span></button>  <button class='btn btn-danger btn-lg' d><span class='fa fa-trash' aria-hidden='true'></span></button></td>";
                      str+="</tr>";
                    }
                }
            }
            
          $("#storeListTable tbody").html(str);
          $("#storeListTable").DataTable();
        }).error(function (jqXHR, textStatus, errorThrown) {
          switch(jqXHR.status){
            case 200:
              alert("資料讀取成功!");
            break;
            default:
              alert("資料載入錯誤!");
            break;
          }
        });
    },  
	editEvent: function(_id){
		var settings = {
          "async": true,
          "crossDomain": true,
          "url": "/getAccountList",
          "method": "POST",
          "headers": {
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            "postman-token": "150fa44f-cf8c-0992-1192-832d5816a97f"
          }
        }

        $.ajax(settings).success(function(response){
            var str="";
            for (var i = 0; i < response.length; i++) {
                if(response[i]._id === _id)
                {
                    $("#industry").val(response[i].industry)
                    $("#email").val(response[i].user);
                    $("#fb_url").val(response[i].fb_url);
                    $("#fb_id").val(response[i].fb_id);
                    $("#phone").val(response[i].phone);
                    break;
                }
            }
          
        })
	},
    savedEvent: function(){
      
        var dataAry = {
            industry:$("#industry option:selected").val(),
            email:$("#email").val(),
            fb_url:$("#fb_url").val(),
            fb_id:$("#fb_id").val(),
            phone:$("#phone").val(),
            pass:$("#pass").val()
        };
        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "/updateUser",
          "method": "POST",
          "headers": {
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            "postman-token": "150fa44f-cf8c-0992-1192-832d5816a97f"
          },
          "data":dataAry
        };
                  $.ajax(settings).success(function(response){
                        alert('修改成功');
                        $('#editUser').modal('hide');
                        $('#editUser').on('hide.bs.modal', function (event) {
                          $(this).remove();
                        })
                        var editUserModalclone = editUserModalbackup.clone();
                        $('#UserContent').append(editUserModalclone);

                        modulesFunction.getAccountList(currentLevel);  
                        
                    }).error(function (jqXHR, textStatus, errorThrown) {
                          alert('修改失敗');
                    });
        
    },
    checkPassedEvent: function(user,passed){
          var dataAry = {
                user:user,
                passed:((passed == 1)?0:1)
            };
            var settings = {
              "async": true,
              "crossDomain": true,
              "url": "/updatePassed",
              "method": "POST",
              "headers": {
                "content-type": "application/x-www-form-urlencoded",
                "cache-control": "no-cache",
                "postman-token": "150fa44f-cf8c-0992-1192-832d5816a97f"
              },
              "data":dataAry
            };
                      $.ajax(settings).success(function(response){
                            alert('更改成功');
                            modulesFunction.getAccountList(currentLevel); 
                            
                        }).error(function (jqXHR, textStatus, errorThrown) {
                              alert('更改失敗');
                              switch(jqXHR.status){
                                    case 400:
                                      console.log("error-updating-account");
                                    break;
                                  }
                        });
    },
    requiredInput: function() {
        var tempString= "";
        if ($("#email").val() == "") tempString += "Email\n";
        if ($("#fb_url").val() == "") tempString += "FB粉絲專業網址\n";
        if ($("#fb_id").val() == "") tempString += "FB粉絲專頁名稱\n";
        if ($("#phone").val() == "") tempString += "手機號碼\n";        
        if ($("#pass").val() == "") tempString += "密碼\n";
        if ($("#re_pass").val() == "") tempString += "確認密碼\n";
        return "以下資料為必填\n"+tempString;
    },
    regexpInput:function(){
        var tempString2= "";
        var rule1 =  /^[A-Z]\d{9}$/; //身分證
        var rule2 = /^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})*$/; //email
        var rule3 = /^09\d{2}\d{3}\d{3}$/; //手機號碼
        //if(!rule1.test($("#cardID").val())) tempString2 += "身分證字號\n";
        // if(rule2.test($("#email").val())) tempString2+= "Email\n";
        if(!rule3.test($("#phone").val())) tempString2 += "手機號碼\n";
       
        return "以下資料未符合格式\n"+tempString2;
    }
};

$(function(){
    modulesFunction.ui();
})

