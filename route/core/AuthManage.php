<?php 
class AuthManage
{    
    public static function init_iniread()
    {
        return self::iniread(__core . "scratch.ini");
    }
    public static function checkUser($usercode)
   {   
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['checkUser']['sql'];
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$usercode,PDO::PARAM_STR);
        $state->execute();

        if ($state->rowCount() < 1) {
            return true;            
        }
        else 
        {
            return false;
        }
   }
    
    public static function addUser($usercode, $userpsw, $username, $industry, $fburl, $fbname, $tel)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        if(self::checkUser($usercode))
        {
            $sql_inquery = $ini_result['data']['addUser']['sql'];

            $state = $conn->prepare($sql_inquery);
            $state->bindValue(1,$usercode,PDO::PARAM_STR);
            $state->bindValue(2,md5($userpsw),PDO::PARAM_STR);
            $state->bindValue(3,$username,PDO::PARAM_STR);
            $state->bindValue(4,'0',PDO::PARAM_STR);
            $state->bindValue(5,'0',PDO::PARAM_STR);
            $state->bindValue(6,date("Y-m-d H:i:s"),PDO::PARAM_STR);
            $state->bindValue(7,'system',PDO::PARAM_STR);
            $state->bindValue(8,date("Y-m-d H:i:s"),PDO::PARAM_STR);
            $state->bindValue(9,'system',PDO::PARAM_STR);
            $state->execute();

            if ($state->rowCount() < 1) {
//                return $state->errorInfo();   
                return false;
            }
            else 
            {
                return self::addUserInfo($usercode, $industry, $fburl, $fbname, $tel);
            }
        }
        else
        {
            return "此帳號已被使用！";
        }
    }
    
    public static function addUserInfo($usercode, $industry, $fburl, $fbname, $tel)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['addUserInfo']['sql'];

        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$usercode,PDO::PARAM_STR);
        $state->bindValue(2,$industry,PDO::PARAM_STR);
        $state->bindValue(3,$fburl,PDO::PARAM_STR);
        $state->bindValue(4,$fbname,PDO::PARAM_STR);
        $state->bindValue(5,$tel,PDO::PARAM_STR);
        $state->bindValue(6,date("Y-m-d H:i:s"),PDO::PARAM_STR);
        $state->bindValue(7,'system',PDO::PARAM_STR);
        $state->bindValue(8,date("Y-m-d H:i:s"),PDO::PARAM_STR);
        $state->bindValue(9,'system',PDO::PARAM_STR);
        $state->execute();
        
        if ($state->rowCount() < 1) {
//            return $state->errorInfo();   
            return false;
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    
    public static function login($usercode, $userpsw)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        
        if(!self::checkUser($usercode) && !empty($usercode))
        {
            $sql_inquery = $ini_result['data']['login']['sql'];
            $state = $conn->prepare($sql_inquery);
            $state->bindValue(1,$usercode,PDO::PARAM_STR);
            $state->bindValue(2,md5($userpsw),PDO::PARAM_STR);
            $state->execute();

            if ($state->rowCount() < 1) {
                return "密碼有誤！請重新確認。";            
            }
            else 
            {                
                return $state->fetchAll();
            }     
        }
        else
        {
            return "系統中無此帳號！請重新確認。";
        }
              
    }
    private static function iniread($filename) 
    {
        // $filename = __pageroot . __appsurl . $_REQUEST["para"];
        
        if ( file_exists($filename) )
        {
           $result = parse_ini_file($filename, true);
        }
        else 
        {
           $result = _error_message("general", "0003", "", $filename . " not found.");             //檔案不存在
        }
        
        $result = array("data" => $result );
        return $result;
    }
}

?>