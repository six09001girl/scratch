<?php 
class ActivityManage
{    
    public static function init_iniread()
    {
        return self::iniread(__core . "scratch.ini");
    }
    public static function checkBannerRepeat($s_ser)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['checkActBannerepeat']['sql'];
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$s_ser,PDO::PARAM_INT);
        $state->execute();

        if ($state->rowCount() < 1) {
            return false;            
        }
        else 
        {
            return true;
        }
    }
    public static function getStorecode($usercode)
   {   
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['getStoreCode']['sql'];
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$usercode,PDO::PARAM_STR);
        $state->execute();

        if ($state->rowCount() < 1) {
            return null;            
        }
        else 
        {
            return $state->fetchAll();
        }
   }
    public static function getPrizeSum($ser)
   {   
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['checkPrizeSum']['sql'];
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,(int)$ser,PDO::PARAM_INT);
        $state->execute();

        if ($state->rowCount() < 1) {
            return null;            
        }
        else 
        {
            return $state->fetchAll();
        }
   }
    public static function getScratch($ser, $prizeseer)
   {   
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['loadScratch']['sql'];
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$ser,PDO::PARAM_INT);
        $state->bindValue(2,$prizeseer,PDO::PARAM_INT);
        $state->execute();

        if ($state->rowCount() < 1) {
            return null;            
        }
        else 
        {
            return $state->fetchAll();
        }
   }
    public static function addScratchlog($args)
    {        
        $checkexists = self::checkScratchLog($args);
        
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        
        if(is_array($checkexists))
        {
            return array();
        }
        else
        {
            $sql_inquery = $ini_result['data']['addScratchLog']['sql'];

            $state = $conn->prepare($sql_inquery);
            $state->bindValue(1,$args['param']['timestamp'],PDO::PARAM_STR);
            $state->bindValue(2,date("Y-m-d H:i:s"),PDO::PARAM_STR);
            $state->bindValue(3,$args['p_ser'],PDO::PARAM_INT);
            $state->bindValue(4,$args['param']['UID'],PDO::PARAM_INT);
            $state->bindValue(5,date("Y-m-d H:i:s"),PDO::PARAM_STR);
            $state->bindValue(6,'system',PDO::PARAM_STR);
            $state->execute();

            if ($state->rowCount() < 1) {
    //                return $state->errorInfo();   
                return false;
            }
            else 
            {
    //            return $state->fetchAll();
                return $conn->lastInsertId();
            }
        }
        
    }
    public static function checkScratchLog($args)
    {        
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['checkScratchLog']['sql'];

        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$args['param']['timestamp'],PDO::PARAM_STR);
        $state->bindValue(2,$args['param']['UID'],PDO::PARAM_STR);
        $state->execute();

        if ($state->rowCount() < 1) {
//                return $state->errorInfo();   
            return false;
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    public static function getActSelectList()
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['actselectlist']['sql'];
        $storecode = self::getStorecode($_SESSION['usercode']);
        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$storecode[0]['SER'],PDO::PARAM_INT);
        $state->execute();

        if ($state->rowCount() < 1) {
            return false;            
        }
        else 
        {
            return $state->fetchAll();;
        }
    }
    public static function getStoreActList()
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['prizeactivityList']['sql'];
        $storecode = self::getStorecode($_SESSION['usercode']);
        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$storecode[0]['SER'],PDO::PARAM_INT);
        $state->execute();

        if ($state->rowCount() < 1) {
            return false;            
        }
        else 
        {
            return $state->fetchAll();;
        }
    }
    
    public static function loadActBanner($ser)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['loadActBanner']['sql'];
        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,(int)$ser,PDO::PARAM_INT);
        $state->execute();

        if ($state->rowCount() < 1) {
            return false;            
        }
        else 
        {
            return $state->fetchAll();;
        }
    }
    public static function getActBannerList($arg)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['actbannerList']['sql'];
        $storecode = self::getStorecode($_SESSION['usercode']);
        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$storecode[0]['SER'],PDO::PARAM_INT);
        $state->execute();

        if ($state->rowCount() < 1) {
            return false;            
        }
        else 
        {
            return $state->fetchAll();;
        }
    }
    public static function addActivity($acttitle, $acttype, $actsdate, $actedate, $actdesp, $awardsdate, $awardedate, $awarddesp, $items, $noprize, $actstatus)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['addPrizeActHead']['sql'];
        
        $storecode = self::getStorecode($_SESSION['usercode']);
        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$storecode[0]['SER'],PDO::PARAM_INT);
        $state->bindValue(2,$acttitle,PDO::PARAM_STR);
        $state->bindValue(3,(int)$acttype,PDO::PARAM_INT);
        $state->bindValue(4,$actsdate,PDO::PARAM_STR);
        $state->bindValue(5,$actedate,PDO::PARAM_STR);
        $state->bindValue(6,$actdesp,PDO::PARAM_STR);
        $state->bindValue(7,$awardsdate,PDO::PARAM_STR);
        $state->bindValue(8,$awardedate,PDO::PARAM_STR);
        $state->bindValue(9,$awarddesp,PDO::PARAM_STR);
        $state->bindValue(10,(int)$items,PDO::PARAM_INT);
        $state->bindValue(11,(int)$noprize,PDO::PARAM_INT);
        $state->bindValue(12,(int)$actstatus,PDO::PARAM_INT);
        $state->bindValue(13,date("Y-m-d H:i:s"),PDO::PARAM_STR);
        $state->bindValue(14,$_SESSION['username'],PDO::PARAM_STR);
        $state->bindValue(15,date("Y-m-d H:i:s"),PDO::PARAM_STR);
        $state->bindValue(16,$_SESSION['username'],PDO::PARAM_STR);
        $state->execute();

        if ($state->rowCount() < 1) {
//            var_dump($state->errorInfo());   
            return false;
        }
        else 
        {
            return $conn->lastInsertId();
        }
    }
    
    public static function addPrize($actcode, $prizename, $haswintxt, $totals, $percent, $filedir, $filename, $size, $type, $source)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['addPrizeActBody']['sql'];

        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$actcode,PDO::PARAM_INT);
        $state->bindValue(2,$prizename,PDO::PARAM_STR);
        $state->bindValue(3,$haswintxt,PDO::PARAM_INT);
        $state->bindValue(4,$totals,PDO::PARAM_INT);
        $state->bindValue(5,$totals,PDO::PARAM_INT);
        $state->bindValue(6,$percent,PDO::PARAM_INT);
        $state->bindValue(7,$filedir,PDO::PARAM_STR);
        $state->bindValue(8,$filename,PDO::PARAM_STR);
        $state->bindValue(9,$size,PDO::PARAM_INT);
        $state->bindValue(10,$type,PDO::PARAM_STR);
        $state->bindValue(11,$source,PDO::PARAM_STR);
        $state->bindValue(12,date("Y-m-d H:i:s"),PDO::PARAM_STR);
        $state->bindValue(13,$_SESSION['username'],PDO::PARAM_STR);
        $state->bindValue(14,date("Y-m-d H:i:s"),PDO::PARAM_STR);
        $state->bindValue(15,$_SESSION['username'],PDO::PARAM_STR);
        $state->execute();
        
        if ($state->rowCount() < 1) {
//            return $state->errorInfo();   
            return false;
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    public static function addactBanner($s_ser, $filedir, $filename, $size, $type, $source)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['addActBanner']['sql'];

        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$s_ser,PDO::PARAM_INT);
        $state->bindValue(2,$filedir,PDO::PARAM_STR);
        $state->bindValue(3,$filename,PDO::PARAM_STR);
        $state->bindValue(4,$size,PDO::PARAM_INT);
        $state->bindValue(5,$type,PDO::PARAM_STR);
        $state->bindValue(6,$source,PDO::PARAM_STR);
        $state->bindValue(7,date("Y-m-d H:i:s"),PDO::PARAM_STR);
        $state->bindValue(8,$_SESSION['username'],PDO::PARAM_STR);
        $state->execute();
        
        if ($state->rowCount() < 1) {
//            return $state->errorInfo();   
            return false;
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    public static function editactBanner($ser, $filedir, $filename, $size, $type, $source)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['editActBanner']['sql'];

        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$filedir,PDO::PARAM_STR);
        $state->bindValue(2,$filename,PDO::PARAM_STR);
        $state->bindValue(3,$size,PDO::PARAM_INT);
        $state->bindValue(4,$type,PDO::PARAM_STR);
        $state->bindValue(5,$source,PDO::PARAM_STR);
        $state->bindValue(6,date("Y-m-d H:i:s"),PDO::PARAM_STR);
        $state->bindValue(7,$_SESSION['username'],PDO::PARAM_STR);
        $state->bindValue(8,$ser,PDO::PARAM_INT);
        $state->execute();
        
        if ($state->rowCount() < 1) {
//            return $state->errorInfo();   
            return false;
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    public static function loadActivity($ser)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['loadPrizeActwithBody']['sql'];
        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$ser,PDO::PARAM_INT);
        $state->execute();

        if ($state->rowCount() < 1) {
            $sql_inquery2 = $ini_result['data']['loadPrizeActwithoutBody']['sql'];        
            $state2 = $conn->prepare($sql_inquery2);
            $state2->bindValue(1,$ser,PDO::PARAM_INT);
            $state2->execute();

            if ($state2->rowCount() < 1) {
                return false;            
            }
            else 
            {
                return $state2->fetchAll();;
            }            
        }
        else 
        {
            return $state->fetchAll();;
        }
    }
    
    public static function editActivity($ser, $acttitle, $acttype, $actsdate, $actedate, $actdesp, $awardsdate, $awardedate, $awarddesp, $items, $noprize, $actstatus)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['editPrizeActHead']['sql'];
        
        $storecode = self::getStorecode($_SESSION['usercode']);
        
        $state = $conn->prepare($sql_inquery);
        
        $state->bindValue(1,$acttitle,PDO::PARAM_STR);
        $state->bindValue(2,(int)$acttype,PDO::PARAM_INT);
        $state->bindValue(3,$actsdate,PDO::PARAM_STR);
        $state->bindValue(4,$actedate,PDO::PARAM_STR);
        $state->bindValue(5,$actdesp,PDO::PARAM_STR);
        $state->bindValue(6,$awardsdate,PDO::PARAM_STR);
        $state->bindValue(7,$awardedate,PDO::PARAM_STR);
        $state->bindValue(8,$awarddesp,PDO::PARAM_STR);
        $state->bindValue(9,(int)$items,PDO::PARAM_INT);
        $state->bindValue(10,(int)$noprize,PDO::PARAM_INT);
        $state->bindValue(11,(int)$actstatus,PDO::PARAM_INT);
        $state->bindValue(12,date("Y-m-d H:i:s"),PDO::PARAM_STR);
        $state->bindValue(13,$_SESSION['username'],PDO::PARAM_STR);
        $state->bindValue(14,$ser,PDO::PARAM_INT);
        $state->bindValue(15,$storecode[0]['SER'],PDO::PARAM_INT);
        $state->execute();

        if ($state->rowCount() < 1) {
//            var_dump($state->errorInfo());   
            return false;
        }
        else 
        {
            return 200;
        }
    }
    public static function editPrize($ser, $actcode, $prizename, $haswintxt, $totals, $percent, $filedir, $filename, $size, $type, $source)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['editPrizeActBody']['sql'];

        $state = $conn->prepare($sql_inquery);
        
        $state->bindValue(1,$prizename,PDO::PARAM_STR);
        $state->bindValue(2,$haswintxt,PDO::PARAM_INT);
        $state->bindValue(3,$totals,PDO::PARAM_INT);
        $state->bindValue(4,$totals,PDO::PARAM_INT);
        $state->bindValue(5,$percent,PDO::PARAM_INT);
        $state->bindValue(6,$filedir,PDO::PARAM_STR);
        $state->bindValue(7,$filename,PDO::PARAM_STR);
        $state->bindValue(8,$size,PDO::PARAM_INT);
        $state->bindValue(9,$type,PDO::PARAM_STR);
        $state->bindValue(10,$source,PDO::PARAM_STR);
        $state->bindValue(12,date("Y-m-d H:i:s"),PDO::PARAM_STR);
        $state->bindValue(12,$_SESSION['username'],PDO::PARAM_STR);
        $state->bindValue(13,$ser,PDO::PARAM_INT);
        $state->bindValue(14,$actcode,PDO::PARAM_INT);
        $state->execute();
        
        if ($state->rowCount() < 1) {
//            return $state->errorInfo();   
            return false;
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    public static function editPrizewithoutFile($ser, $actcode, $prizename, $haswintxt, $totals, $percent)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['editPrizeActBodywithoutfile']['sql'];

        $state = $conn->prepare($sql_inquery);
        
        $state->bindValue(1,$prizename,PDO::PARAM_STR);
        $state->bindValue(2,$haswintxt,PDO::PARAM_INT);
        $state->bindValue(3,$totals,PDO::PARAM_INT);
        $state->bindValue(4,$percent,PDO::PARAM_INT);
        $state->bindValue(5,date("Y-m-d H:i:s"),PDO::PARAM_STR);
        $state->bindValue(6,$_SESSION['username'],PDO::PARAM_STR);
        $state->bindValue(7,$ser,PDO::PARAM_INT);
        $state->bindValue(8,$actcode,PDO::PARAM_INT);
        $state->execute();
        
        if ($state->rowCount() < 1) {
//            return $state->errorInfo();   
            return false;
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    
    public static function deleteActivity($ser)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['deletePrizeActHead']['sql'];        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$ser,PDO::PARAM_INT);
        $state->execute();
        
        $sql_inquery2 = $ini_result['data']['deletePrizeActBody']['sql'];        
        $state2 = $conn->prepare($sql_inquery2);
        $state2->bindValue(1,$ser,PDO::PARAM_INT);
        $state2->execute();

        if ($state->rowCount() < 1) 
        {
            return false;   
        }
        else 
        {
            return $state->fetchAll();;
        }
    }
    
    public static function deleteActBanner($ser)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['deleteActBanner']['sql'];        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$ser,PDO::PARAM_INT);
        $state->execute();
        
        if ($state->rowCount() < 1) 
        {
            return false;   
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    public static function copyPrizeActHead($ser)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['copyPrizeActHead']['sql'];        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,date("Y-m-d H:i:s"),PDO::PARAM_STR);
        $state->bindValue(2,$_SESSION['username'],PDO::PARAM_STR);
        $state->bindValue(3,$ser,PDO::PARAM_INT);
        $state->execute();
        
//        return $state->errorInfo(); 
        if ($state->rowCount() < 1) 
        {
            return false;   
        }
        else 
        {
            return $conn->lastInsertId();
        }
    }
    public static function copyPrizeActBody($newser,$oldser)
    {
        $dir_user = explode ( "@", $_SESSION['usercode'] );
        
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['copyPrizeActBody']['sql'];        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$newser,PDO::PARAM_INT);
        $state->bindValue(2,'figure/'.$dir_user[0]. '/' ."ACT" . $newser .'/' ,PDO::PARAM_STR);
        $state->bindValue(3,date("Y-m-d H:i:s"),PDO::PARAM_STR);
        $state->bindValue(4,$_SESSION['username'],PDO::PARAM_STR);
        $state->bindValue(5,$oldser,PDO::PARAM_INT);
        $state->execute();
        
        if ($state->rowCount() < 1) 
        {
            return false;   
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    public static function copyActBanner($newser,$oldser)
    {
        $dir_user = explode ( "@", $_SESSION['usercode'] );
        
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['copyActBanner']['sql'];        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$newser,PDO::PARAM_INT);
        $state->bindValue(2,'figure/'.$dir_user[0]. '/' ."ACT" . $newser . '/Banner/'  ,PDO::PARAM_STR);
        $state->bindValue(3,date("Y-m-d H:i:s"),PDO::PARAM_STR);
        $state->bindValue(4,$_SESSION['username'],PDO::PARAM_STR);
        $state->bindValue(5,$oldser,PDO::PARAM_INT);
        $state->execute();
        
        if ($state->rowCount() < 1) 
        {
            return false;   
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    public static function checkStorePsd($args)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $params = json_decode($args['params']);
        
        $sql_inquery = $ini_result['data']['checkStorePsd']['sql']; 
        $state = $conn->prepare($sql_inquery);   
        $state->bindValue(1,(int)$params->actser,PDO::PARAM_INT);
        $state->bindValue(2,$args['changekeyword'],PDO::PARAM_STR);
        $state->execute();
        
        if ($state->rowCount() < 1) {
//            return $state->errorInfo();   
            return false;
        }
        else 
        {
            return true;
        }
    }
    
    public static function loadScratchstoredata($ser)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        
        $sql_inquery = $ini_result['data']['loadScratchStoredata']['sql']; 
        $state = $conn->prepare($sql_inquery);   
        $state->bindValue(1,$ser,PDO::PARAM_INT);
        $state->execute();
        
        if ($state->rowCount() < 1) {
//            return $state->errorInfo();   
            return false;
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    
    public static function exchangePrize($args)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $params = json_decode($args['params']);
        
        $sql_inquery = $ini_result['data']['exchangePrize']['sql']; 
        $state = $conn->prepare($sql_inquery);   
        $state->bindValue(1,date("Y-m-d H:i:s"),PDO::PARAM_STR);
        $state->bindValue(2,(($args['actype']=="0")?$params->UID:$args['changename']),PDO::PARAM_STR);
        $state->bindValue(3,$params->timestamp,PDO::PARAM_STR);
        $state->execute();
        
        if ($state->rowCount() < 1) 
        {
            return false;   
        }
        else 
        {
            if($args['actype'] == "1")
            {
                $sql_inquery1 = $ini_result['data']['exchangeLog']['sql']; 
                $state1 = $conn->prepare($sql_inquery1);   
                $state1->bindValue(1,(int)$args['aid'],PDO::PARAM_INT);
                $state1->bindValue(2,$args['changename'],PDO::PARAM_STR);
                $state1->bindValue(3,$args['changetel'],PDO::PARAM_STR);
                $state1->bindValue(4,$args['changeemail'],PDO::PARAM_STR);
                $state1->bindValue(5,$args['changeaddr'],PDO::PARAM_STR);
                $state1->bindValue(6,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                $state1->bindValue(7,'system',PDO::PARAM_STR);
                $state1->execute();  
            }            
            
            $curnum = (int)self::getPrizeSum((int)$args['pid'])[0]['CURTOTALS'];
            $sql_inquery2 = $ini_result['data']['updatePrizeSum']['sql']; 
            $state2 = $conn->prepare($sql_inquery2);   
            $state2->bindValue(1,$curnum-1,PDO::PARAM_INT);
            $state2->bindValue(2,(int)$args['pid'],PDO::PARAM_INT);
            $state2->execute();
            
            return $state->fetchAll();;
        }
    }
    public static function awardLogList($args)
    {
        $storecode = self::getStorecode($_SESSION['usercode']);
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['awardLogList']['sql'];
        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,(int)$args['acttype'],PDO::PARAM_INT);
        $state->bindValue(2,$storecode[0]['SER'],PDO::PARAM_STR);
        $state->bindValue(3,(($args['redemptionStart'])?$args['redemptionStart']:'1900-01-01').' 00:00:00',PDO::PARAM_STR);
        $state->bindValue(4,(($args['redemptionFinish'])?$args['redemptionFinish']:'3000-12-31').' 23:59:59',PDO::PARAM_STR);
        $state->execute();

        if ($state->rowCount() < 1) {
            return false;            
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    public static function loadAwardLog($ser)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        
        $sql_inquery = $ini_result['data']['loadAwardLog']['sql']; 
        $state = $conn->prepare($sql_inquery);   
        $state->bindValue(1,$ser,PDO::PARAM_INT);
        $state->execute();
        
        if ($state->rowCount() < 1) {
//            return $state->errorInfo();   
            return false;
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    private static function iniread($filename) 
    {
        // $filename = __pageroot . __appsurl . $_REQUEST["para"];
        
        if ( file_exists($filename) )
        {
           $result = parse_ini_file($filename, true);
        }
        else 
        {
           $result = _error_message("general", "0003", "", $filename . " not found.");             //檔案不存在
        }
        
        $result = array("data" => $result );
        return $result;
    }
}

?>