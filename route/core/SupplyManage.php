<?php 
class SupplyManage
{
    public static function init_iniread()
    {
        return self::iniread(__core . "scratch.ini");
    }
    
    public static function checkSupply($_id)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['checkSupplyAccount']['sql'];
        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$_id,PDO::PARAM_STR);
        $state->execute();

        if ($state->rowCount() < 1) {
            return true;            
        }
        else 
        {
            return false;
        }
    }
    
    
    public static function getSupplyList()
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['supplyList']['sql'];
        
        $state = $conn->prepare($sql_inquery);
        $state->execute();

        if ($state->rowCount() < 1) {
            return false;            
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    
    public static function addSupply($args)
    {
        if(self::checkSupply($args['usercode']))
        {
            include(__pageroot . "mod_db.php");
            $ini_result = self::init_iniread();
            $sql_inquery = $ini_result['data']['addSupplyAccount']['sql'];

            $state = $conn->prepare($sql_inquery);
            $state->bindValue(1,$args['usercode'],PDO::PARAM_STR);
            $state->bindValue(2,md5($args['tel']),PDO::PARAM_STR);
            $state->bindValue(3,$args['username'],PDO::PARAM_STR);
            $state->bindValue(4,date("Y-m-d H:i:s"),PDO::PARAM_STR);
            $state->bindValue(5,explode("@", $_SESSION['usercode'])[0],PDO::PARAM_STR);
            $state->execute();

            if ($state->rowCount() < 1) {
                return false;            
            }
            else 
            {
                $sql_inquery1 = $ini_result['data']['addSupplyData']['sql'];

                $state1 = $conn->prepare($sql_inquery1);
                $state1->bindValue(1,$args['usercode'],PDO::PARAM_STR);
                $state1->bindValue(2,$args['worktime'],PDO::PARAM_STR);
                $state1->bindValue(3,$args['addr'],PDO::PARAM_STR);
                $state1->bindValue(4,$args['industry'],PDO::PARAM_STR);
                $state1->bindValue(5,$args['tel'],PDO::PARAM_STR);
                $state1->bindValue(6,$args['fax'],PDO::PARAM_STR);
                $state1->bindValue(7,$args['contactor'],PDO::PARAM_STR);
                $state1->bindValue(8,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                $state1->bindValue(9,explode("@", $_SESSION['usercode'])[0],PDO::PARAM_STR);
                $state1->execute();
                return $state->fetchAll();
            }
        }
        else
        {
            return false;
        }
        
    }
    
    public static function loadSupply($args)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['loadSupply']['sql'];
        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$args['usercode'],PDO::PARAM_STR);
        $state->execute();

        if ($state->rowCount() < 1) {
            return false;            
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    
    public static function loadLocalSupply()
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['loadSupply']['sql'];
        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$_SESSION['usercode'],PDO::PARAM_STR);
        $state->execute();

        if ($state->rowCount() < 1) {
            return false;            
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    
    public static function editSupply($args)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['editSupplyAccount']['sql'];

        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$args['username'],PDO::PARAM_STR);
        $state->bindValue(2,$args['lock'],PDO::PARAM_STR);
        $state->bindValue(3,$args['rights'],PDO::PARAM_STR);
        $state->bindValue(4,date("Y-m-d H:i:s"),PDO::PARAM_STR);
        $state->bindValue(5,explode("@", $_SESSION['usercode'])[0],PDO::PARAM_STR);
        $state->bindValue(6,$args['usercode'],PDO::PARAM_STR);
        $state->execute();

        if ($state->rowCount() < 1) {
            return false;            
        }
        else 
        {
            $sql_inquery1 = $ini_result['data']['editSupplyData']['sql'];

            $state1 = $conn->prepare($sql_inquery1);            
            $state1->bindValue(1,$args['addr'],PDO::PARAM_STR);
            $state1->bindValue(2,$args['worktime'],PDO::PARAM_STR);
            $state1->bindValue(3,$args['industry'],PDO::PARAM_STR);
            $state1->bindValue(4,$args['tel'],PDO::PARAM_STR);
            $state1->bindValue(5,$args['fax'],PDO::PARAM_STR);
            $state1->bindValue(6,$args['contactor'],PDO::PARAM_STR);
            $state1->bindValue(7,date("Y-m-d H:i:s"),PDO::PARAM_STR);
            $state1->bindValue(8,explode("@", $_SESSION['usercode'])[0],PDO::PARAM_STR);
            $state1->bindValue(9,$args['usercode'],PDO::PARAM_STR);
            $state1->execute();
            return $state->fetchAll();
        }
        
    }
    
    public static function editLocalSupply($args)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['editLocalSupplyAccount']['sql'];

        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$args['username'],PDO::PARAM_STR);
        $state->bindValue(2,date("Y-m-d H:i:s"),PDO::PARAM_STR);
        $state->bindValue(3,explode("@", $_SESSION['usercode'])[0],PDO::PARAM_STR);
        $state->bindValue(4,$_SESSION['usercode'],PDO::PARAM_STR);
        $state->execute();

        if ($state->rowCount() < 1) {
            return false;            
        }
        else 
        {
            $sql_inquery1 = $ini_result['data']['editLocalSupplyData']['sql'];

            $state1 = $conn->prepare($sql_inquery1);            
            $state1->bindValue(1,$args['addr'],PDO::PARAM_STR);
            $state1->bindValue(2,$args['worktime'],PDO::PARAM_STR);
            $state1->bindValue(3,$args['industry'],PDO::PARAM_STR);
            $state1->bindValue(4,$args['tel'],PDO::PARAM_STR);
            $state1->bindValue(5,$args['fax'],PDO::PARAM_STR);
            $state1->bindValue(6,$args['contactor'],PDO::PARAM_STR);
            $state1->bindValue(7,date("Y-m-d H:i:s"),PDO::PARAM_STR);
            $state1->bindValue(8,explode("@", $_SESSION['usercode'])[0],PDO::PARAM_STR);
            $state1->bindValue(9,$_SESSION['usercode'],PDO::PARAM_STR);
            $state1->execute();
            return $state->fetchAll();
        }
        
    }
    
    public static function deleteSupply($args)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['deleteSupply-1']['sql'];

        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$args['usercode'],PDO::PARAM_STR);
        $state->execute();

        if ($state->rowCount() < 1) {
            return false;            
        }
        else 
        {
            $sql_inquery1 = $ini_result['data']['deleteSupply-2']['sql'];

            $state1 = $conn->prepare($sql_inquery1);   
            $state1->bindValue(1,$args['usercode'],PDO::PARAM_STR);
            $state1->execute();
            return $state->fetchAll();
        }
        
    }
    
    private static function iniread($filename) 
    {
        // $filename = __pageroot . __appsurl . $_REQUEST["para"];
        
        if ( file_exists($filename) )
        {
           $result = parse_ini_file($filename, true);
        }
        else 
        {
           $result = _error_message("general", "0003", "", $filename . " not found.");             //檔案不存在
        }
        
        $result = array("data" => $result );
        return $result;
    }
}
?>