<?php 
class FBManage
{
    public static function init_iniread()
    {
        return self::iniread(__core . "scratch.ini");
    }
    public static function updateFBtoken($token, $usercode)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['fblogin']['sql'];
        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$token,PDO::PARAM_STR);
        $state->bindValue(2,$usercode,PDO::PARAM_STR);
        $state->execute();

        if ($state->rowCount() < 1) {
            return false;            
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    
    public static function unconnectfb($usercode)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['unconnectfb']['sql'];
        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$usercode,PDO::PARAM_STR);
        $state->execute();

        if ($state->rowCount() < 1) {
            return false;            
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    
    public static function addFanspage($storecode, $pageid, $pagetoken, $articleid, $keyword, $comment, $message, $msgkeyword, $callback, $actid)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['addFanspage']['sql'];
        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$storecode,PDO::PARAM_INT);
        $state->bindValue(2,$pageid,PDO::PARAM_STR);
        $state->bindValue(3,$pagetoken,PDO::PARAM_STR);
        $state->bindValue(4,trim($articleid),PDO::PARAM_STR);
        $state->bindValue(5,$keyword,PDO::PARAM_STR);
        $state->bindValue(6,$comment,PDO::PARAM_STR);
        $state->bindValue(7,$message,PDO::PARAM_STR);
        $state->bindValue(8,$msgkeyword,PDO::PARAM_STR);
        $state->bindValue(9,$callback,PDO::PARAM_STR);
        $state->bindValue(10,$actid,PDO::PARAM_INT);
        $state->bindValue(11,date("Y-m-d H:i:s"),PDO::PARAM_STR);
        $state->bindValue(12,$_SESSION['username'],PDO::PARAM_STR);
        $state->execute();

        if ($state->rowCount() < 1) {
            return false;            
        }
        else 
        {
            return $conn->lastInsertId();
        }
    }
    
    public static function updateFanspage($storecode, $pageid, $pagetoken, $articleid, $keyword, $comment, $message, $msgkeyword, $callback, $actid)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['updateFanspage']['sql'];
        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$keyword,PDO::PARAM_STR);
        $state->bindValue(2,$comment,PDO::PARAM_STR);
        $state->bindValue(3,$message,PDO::PARAM_STR);
        $state->bindValue(4,$msgkeyword,PDO::PARAM_STR);
        $state->bindValue(5,$callback,PDO::PARAM_STR);
        $state->bindValue(6,$actid,PDO::PARAM_INT);
        $state->bindValue(7,date("Y-m-d H:i:s"),PDO::PARAM_STR);
        $state->bindValue(8,$_SESSION['username'],PDO::PARAM_STR);
        $state->bindValue(9,$pageid,PDO::PARAM_STR);
        $state->bindValue(10,$storecode,PDO::PARAM_INT);
        $state->bindValue(11,$articleid,PDO::PARAM_STR);
        
        $state->execute();

        if ($state->rowCount() < 1) {
            return false;            
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    
    public static function updateFanspageToken($storecode, $pageid, $pagetoken)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['updateFanspageToken']['sql'];
        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$pagetoken,PDO::PARAM_STR);
        $state->bindValue(2,date("Y-m-d H:i:s"),PDO::PARAM_STR);
        $state->bindValue(3,$_SESSION['username'],PDO::PARAM_STR);
        $state->bindValue(4,$pageid,PDO::PARAM_STR);
        $state->bindValue(5,$storecode,PDO::PARAM_INT);
        
        $state->execute();

        if ($state->rowCount() < 1) {
            return false;            
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    public static function loadFanspage($pageid, $ser)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['loadFanspage']['sql'];
        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$pageid,PDO::PARAM_STR);
        $state->bindValue(2,$ser,PDO::PARAM_STR);
        
        $state->execute();

        if ($state->rowCount() < 1) {
            return false;            
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    public static function loadFanspagePI($pageid)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['loadFanspagePI']['sql'];
        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$pageid,PDO::PARAM_STR);
        
        $state->execute();

        if ($state->rowCount() < 1) {
            return false;            
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    public static function leavemsgLog($uid, $pageid, $articleid, $actid)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['leavemsgLog']['sql'];
        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$uid,PDO::PARAM_STR);
        $state->bindValue(2,$pageid,PDO::PARAM_STR);
        $state->bindValue(3,$articleid,PDO::PARAM_STR);
        $state->bindValue(4,$actid,PDO::PARAM_STR);
        $state->bindValue(5,date("Y-m-d H:i:s"),PDO::PARAM_STR);
        
        $state->execute();

        if ($state->rowCount() < 1) {
            return false;            
        }
        else 
        {
            return $state->fetchAll();
        }
    }
    public static function getleavemsgLog($uid, $pageid)
    {
        include(__pageroot . "mod_db.php");
        $ini_result = self::init_iniread();
        $sql_inquery = $ini_result['data']['getleavemsgLog']['sql'];
        
        $state = $conn->prepare($sql_inquery);
        $state->bindValue(1,$uid,PDO::PARAM_STR);
        $state->bindValue(2,$pageid,PDO::PARAM_STR);
        
        $state->execute();

        if ($state->rowCount() < 1) {
            return false;            
        }
        else 
        {
            return $state->fetchAll();
        }
    }
     public static function getloginurl()
     {
         require_once "FBconfig.php";
         header("Content-Type:text/html; charset=utf-8");
         $helper = $fb->getRedirectLoginHelper();

        //取得使用者的access_token
        try{
            $accessToken = $helper->getAccessToken();
        }catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        }catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $_SESSION['user_token'] = (string) $accessToken; //存取使用者的accessToken
        $temp = array();
        if(isset($_SESSION['user_token'])){
            $temp['url'] = "./Fanspages?param1=list";//成功，粉絲頁列表             
        }else{
            $temp['url'] = "./Fanspages?param1=index"; //未獲取user_token
        }
        
         return $temp;
     }
    
    private static function iniread($filename) 
    {
        // $filename = __pageroot . __appsurl . $_REQUEST["para"];
        
        if ( file_exists($filename) )
        {
           $result = parse_ini_file($filename, true);
        }
        else 
        {
           $result = _error_message("general", "0003", "", $filename . " not found.");             //檔案不存在
        }
        
        $result = array("data" => $result );
        return $result;
    }
}
?>