-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 23, 2018 at 08:03 AM
-- Server version: 5.6.35
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scratch`
--
CREATE DATABASE IF NOT EXISTS `scratch` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `scratch`;

-- --------------------------------------------------------

--
-- Table structure for table `prizeactivity`
--

CREATE TABLE `prizeactivity` (
  `SER` int(8) NOT NULL COMMENT '流水號',
  `STORECODE` int(8) NOT NULL COMMENT '店家編號[userinfo.ser]',
  `ACTTITLE` varchar(150) NOT NULL COMMENT '活動名稱',
  `ACTTYPE` int(1) NOT NULL COMMENT '活動類型[0:線上1:線下]',
  `ACTSTARTDATE` char(20) NOT NULL COMMENT '活動開始時間',
  `ACTENDDATE` char(20) NOT NULL COMMENT '活動結束時間',
  `ACTDESP` text COMMENT '活動描述(客戶看)',
  `AWARDSTARTDATE` char(20) NOT NULL COMMENT '兌獎開始時間',
  `AWARDENDDATE` char(20) NOT NULL COMMENT '兌獎結束時間',
  `AWARDDESP` text COMMENT '兌獎說明(客戶看)',
  `ITEMS` int(5) NOT NULL COMMENT '獎品數量',
  `NOPRIZE` int(1) NOT NULL COMMENT '是否有銘謝惠顧0:銘謝惠顧1:無',
  `ACTSTATUS` int(1) NOT NULL COMMENT '活動狀態0:尚未進行1:進行中2:已結束',
  `CREATE_DATE` char(20) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '建立時間',
  `BUILDER` varchar(20) NOT NULL COMMENT '建立者',
  `LAST_DATE` char(20) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '最後編輯時間',
  `EDITOR` varchar(20) NOT NULL COMMENT '編輯者'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='廠商活動';

--
-- Dumping data for table `prizeactivity`
--

INSERT INTO `prizeactivity` (`SER`, `STORECODE`, `ACTTITLE`, `ACTTYPE`, `ACTSTARTDATE`, `ACTENDDATE`, `ACTDESP`, `AWARDSTARTDATE`, `AWARDENDDATE`, `AWARDDESP`, `ITEMS`, `NOPRIZE`, `ACTSTATUS`, `CREATE_DATE`, `BUILDER`, `LAST_DATE`, `EDITOR`) VALUES
(1, 1, '23456', 0, '2018/12/03', '2018/12/10', 'qweweweqw', '2018/12/17', '2018/12/24', 'sdsadas . fasfsaf', 2, 0, 1, '2018-12-20 00:56:59', 'six09001girl@gmail.c', '2018-12-20 00:56:59', 'six09001girl@gmail.c'),
(2, 1, 'mnbmjkhgjkhg', 1, '2018/12/02', '2018/12/09', 'cxzcxz45454', '2018/12/09', '2018/12/16', 'bbdges`121324', 3, 0, 2, '2018-12-20 00:59:19', 'six09001girl@gmail.c', '2018-12-20 00:59:19', 'six09001girl@gmail.c');

-- --------------------------------------------------------

--
-- Table structure for table `prizeset`
--

CREATE TABLE `prizeset` (
  `SER` int(8) NOT NULL COMMENT '流水號',
  `ACTIVITYCODE` int(8) NOT NULL COMMENT '活動編號prizeactivity.ser',
  `PRIZENAME` varchar(20) NOT NULL COMMENT '獎品名稱',
  `HASWINTEXT` int(1) NOT NULL COMMENT '是否出現恭喜中獎0:出現1:不出現',
  `TOTALS` int(10) NOT NULL COMMENT '抽獎總數量',
  `PERCENT` int(5) NOT NULL COMMENT '抽獎比例',
  `FILE_DIRECTORY` char(120) NOT NULL COMMENT '目錄',
  `FILE_NAME` char(120) NOT NULL COMMENT '圖檔名稱',
  `SIZE` int(8) NOT NULL DEFAULT '0' COMMENT '檔案大小',
  `TYPE` char(6) NOT NULL COMMENT '檔案類別',
  `SOURCE` char(120) NOT NULL COMMENT '來源圖 原檔名',
  `CREATE_DATE` char(20) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '建立時間',
  `BUILDER` varchar(20) NOT NULL COMMENT '建立者',
  `LAST_DATE` char(20) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '最後編輯時間',
  `EDITOR` varchar(20) NOT NULL COMMENT '編輯者'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='獎品設置';

--
-- Dumping data for table `prizeset`
--

INSERT INTO `prizeset` (`SER`, `ACTIVITYCODE`, `PRIZENAME`, `HASWINTEXT`, `TOTALS`, `PERCENT`, `FILE_DIRECTORY`, `FILE_NAME`, `SIZE`, `TYPE`, `SOURCE`, `CREATE_DATE`, `BUILDER`, `LAST_DATE`, `EDITOR`) VALUES
(1, 1, 'AAAA', 0, 50, 50, 'figure/six09001girl/ACT1/', 'Prize120181220005659', 239772, 'image/', '43779442_100538934313636_7122933137827044659_n.jpg', '2018-12-20 00:56:59', 'system', '2018-12-20 00:56:59', 'system'),
(2, 1, '銘謝惠顧', 1, 50, 50, 'figure/six09001girl/ACT1/', 'Prize220181220005659', 189509, 'image/', 'c05633460.png', '2018-12-20 00:56:59', 'system', '2018-12-20 00:56:59', 'system'),
(3, 2, '1121212', 0, 40, 40, 'figure/six09001girl/ACT2/', 'Prize120181220005919', 221202, 'image/', '46691015_2343399649214950_2983737950601071527_n.jpg', '2018-12-20 00:59:19', 'system', '2018-12-20 00:59:19', 'system'),
(4, 2, '', 1, 30, 30, 'figure/six09001girl/ACT2/', 'Prize220181220005919', 228835, 'image/', '46977229_572278513199519_2715217057155680895_n.jpg', '2018-12-20 00:59:19', 'system', '2018-12-20 00:59:19', 'system'),
(5, 2, '銘謝惠顧', 1, 30, 30, 'figure/six09001girl/ACT2/', 'Prize320181220005919', 189509, 'image/', 'c05633460.png', '2018-12-20 00:59:19', 'system', '2018-12-20 00:59:19', 'system');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `USERCODE` char(50) NOT NULL DEFAULT '',
  `USERPSW` varchar(255) NOT NULL DEFAULT '',
  `USERNAME` char(10) NOT NULL COMMENT '名',
  `CREATE_DATE` varchar(20) NOT NULL DEFAULT '' COMMENT '創建日期',
  `BUILDER` varchar(30) NOT NULL DEFAULT '' COMMENT '建立者',
  `LAST_DATE` varchar(20) NOT NULL DEFAULT '' COMMENT '最後編輯日期',
  `EDITOR` varchar(30) NOT NULL DEFAULT '' COMMENT '編輯者',
  `STATUS` char(1) NOT NULL DEFAULT '' COMMENT '用戶狀態:0:註冊 1:未設(無)功能  2:系統停權  3:用戶暫停使用 9:永久停權(用)',
  `LOCK` char(1) NOT NULL DEFAULT '' COMMENT '0:正常 1:鎖',
  `RIGHTS` char(3) DEFAULT '' COMMENT '系統權限'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`USERCODE`, `USERPSW`, `USERNAME`, `CREATE_DATE`, `BUILDER`, `LAST_DATE`, `EDITOR`, `STATUS`, `LOCK`, `RIGHTS`) VALUES
('six09001girl@gmail.com', '4a7d1ed414474e4033ac29ccb8653d9b', 'test', '2018-12-11 22:28:17', 'system', '2018-12-11 22:28:17', 'system', '0', '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `userinfo`
--

CREATE TABLE `userinfo` (
  `SER` int(8) NOT NULL COMMENT '流水號',
  `USERCODE` char(50) NOT NULL COMMENT '帳號',
  `INDUSTRY` char(2) NOT NULL COMMENT '行業別',
  `FBURL` text NOT NULL COMMENT 'FB粉專連結',
  `FBNAME` varchar(20) NOT NULL COMMENT 'FB粉專名稱',
  `TEL` char(10) DEFAULT NULL COMMENT '電話',
  `CREATE_DATE` char(20) NOT NULL COMMENT '建立時間',
  `BUILDER` varchar(20) NOT NULL COMMENT '建立者',
  `LAST_DATE` char(20) NOT NULL COMMENT '最後編輯時間',
  `EDITOR` varchar(20) NOT NULL COMMENT '編輯者'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='使用者資訊';

--
-- Dumping data for table `userinfo`
--

INSERT INTO `userinfo` (`SER`, `USERCODE`, `INDUSTRY`, `FBURL`, `FBNAME`, `TEL`, `CREATE_DATE`, `BUILDER`, `LAST_DATE`, `EDITOR`) VALUES
(1, 'six09001girl@gmail.com', 'C', 'test', 'test', '0911000000', '2018-12-11 22:28:17', 'system', '2018-12-11 22:28:17', 'system');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `prizeactivity`
--
ALTER TABLE `prizeactivity`
  ADD PRIMARY KEY (`SER`);

--
-- Indexes for table `prizeset`
--
ALTER TABLE `prizeset`
  ADD PRIMARY KEY (`SER`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`USERCODE`);

--
-- Indexes for table `userinfo`
--
ALTER TABLE `userinfo`
  ADD PRIMARY KEY (`SER`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `prizeactivity`
--
ALTER TABLE `prizeactivity`
  MODIFY `SER` int(8) NOT NULL AUTO_INCREMENT COMMENT '流水號', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `prizeset`
--
ALTER TABLE `prizeset`
  MODIFY `SER` int(8) NOT NULL AUTO_INCREMENT COMMENT '流水號', AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `userinfo`
--
ALTER TABLE `userinfo`
  MODIFY `SER` int(8) NOT NULL AUTO_INCREMENT COMMENT '流水號', AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
