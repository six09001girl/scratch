<?php 
/**
 * summary
 */


class Equipment_DB_event
{
    
    public static function DB_Event($method, $args){    
        include("mod_db.php");
        $ini_result = self::iniread(__pageroot . "htclab.ini");
        switch ($method) {
            case 'equtotcount':
                $sql_inquery = $ini_result['data']['equtotcount']['sql'][0];

                $state = $conn->prepare($sql_inquery);

                $state->execute();
                
                 if ($state->rowCount() < 1) {
                // if ($state->execute() === false) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
                break;
			case 'equborrowcount':
			   $sql_inquery = $ini_result['data']['equborrowcount']['sql'][0];
						$state = $conn->prepare($sql_inquery);
						$state->execute();
						
						 if ($state->rowCount() < 1) {
						// if ($state->execute() === false) {
							return false;            
						}
						else 
						{
							return $state->fetchAll();
						}
				break;
				case 'equunborrowcount':
				   $sql_inquery = $ini_result['data']['equunborrowcount']['sql'][0];
					$state = $conn->prepare($sql_inquery);
					$state->execute();
					
					 if ($state->rowCount() < 1) {
					// if ($state->execute() === false) {
						return false;            
					}
					else 
					{
						return $state->fetchAll();
					}
				break;
						
				case 'equscrappedcount':
					$sql_inquery = $ini_result['data']['equscrappedcount']['sql'][0];
					$state = $conn->prepare($sql_inquery);
					$state->execute();
					
					 if ($state->rowCount() < 1) {
					// if ($state->execute() === false) {
						return false;            
					}
					else 
					{
						return $state->fetchAll();
					}
				break;
				case 'equipments':
					$sql_inquery = $ini_result['data']['equipments']['sql'][0];
					$state = $conn->prepare($sql_inquery);
					$state->execute();
					
					 if ($state->rowCount() < 1) {
					// if ($state->execute() === false) {
						return false;            
					}
					else 
					{
						return $state->fetchAll();
					}
				break;
				case 'borrowequipments':
					$sql_inquery = $ini_result['data']['borrowequipments']['sql'][0];
					$state = $conn->prepare($sql_inquery);
					$state->execute();
					
					 if ($state->rowCount() < 1) {
					// if ($state->execute() === false) {
						return false;            
					}
					else 
					{
						return $state->fetchAll();
					}
				break;
				case 'uunborrowequipments':
					$sql_inquery = $ini_result['data']['uunborrowequipments']['sql'][0];
					$state = $conn->prepare($sql_inquery);
					$state->execute();
					
					 if ($state->rowCount() < 1) {
					// if ($state->execute() === false) {
						return false;            
					}
					else 
					{
						return $state->fetchAll();
					}
				break;
				case 'scrappedequipments':
					$sql_inquery = $ini_result['data']['scrappedequipments']['sql'][0];
					$state = $conn->prepare($sql_inquery);
					$state->execute();
					
					 if ($state->rowCount() < 1) {
					// if ($state->execute() === false) {
						return false;            
					}
					else 
					{
						return $state->fetchAll();
					}
				break;
				case 'computertype':
				case 'mobiletype':
				case 'bodytype':
				case '3dprintertype':
				case 'othertype':
					$sql_inquery = $ini_result['data'][$method]['sql'][0];
					$state = $conn->prepare($sql_inquery);
					$state->execute();
					
					 if ($state->rowCount() < 1) {
					// if ($state->execute() === false) {
						return false;            
					}
					else 
					{
						return $state->fetchAll();
					}
				break;
                case 'equdetail':
                    $sql_inquery = $ini_result['data'][$method]['sql'][0];
                    $v1 = $args[0];
                    $state = $conn->prepare($sql_inquery);
                    $state->bindValue(1,$v1,PDO::PARAM_STR);
                    $state->execute();
                    
                     if ($state->rowCount() < 1) {
                    // if ($state->execute() === false) {
                        return false;            
                    }
                    else 
                    {
                        return $state->fetchAll();
                    }
                    break;

            case 'titbitsAlbumImg':
                $sql_inquery = $ini_result['data']['titbits_image']['sql'][0];
                $v1 = $args[0];
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$v1,PDO::PARAM_STR);
                $state->execute();
                
                 if ($state->rowCount() < 1) {
                // if ($state->execute() === false) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
                break;
            case 'company':
                $sql_inquery = $ini_result['data']['company']['sql'][0];

                $state = $conn->prepare($sql_inquery);

                $state->execute();
                if ($state->rowCount() < 1) {
                // if ($state->execute() === false) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
                break;
            case 'documents':
                $sql_inquery = $ini_result['data']['documents']['sql'][0];

                $v1 = $args[0];
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$v1,PDO::PARAM_STR);
                $state->execute();
                if ($state->rowCount() < 1) {
                // if ($state->execute() === false) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
                break;
            default:
                # code...
                break;
        }
    }
    //base
    private static function iniread($filename) 
    {
        // $filename = __pageroot . __appsurl . $_REQUEST["para"];
        
        if ( file_exists($filename) )
        {
           $result = parse_ini_file($filename, true);
        }
        else 
        {
           $result = _error_message("general", "0003", "", $filename . " not found.");             //檔案不存在
        }
        
        $result = array("data" => $result );
        return $result;
    }
}
?>