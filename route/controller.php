<?php 

// include_once("mod_file.php");

define("__pageroot", dirname(__FILE__) . "/");
define("__core", dirname(__FILE__) . "/core/");
define("__memindexdir", "layout/memberIndex/0/");
define("__inidir", "route/");
define('DS', DIRECTORY_SEPARATOR); // PHP會根據所在的環境自動產生目錄分隔符號

ini_set("display_errors", "On");
date_default_timezone_set('Asia/Taipei');
session_start();

$action= $_GET["action"];
Action_Event($action);


function Action_Event($action){    
    try
    {
        include("mod_db.php");
        require("core/AuthManage.php"); 
        require("core/ActivityManage.php");
        require("core/FBManage.php");
        require("core/SupplyManage.php");
    }
    catch(Exception $e)
    {
        echo $e->getMessage();
    }   
    
    switch ($action) {
        case '_checkauth':           
            if(!empty($_SESSION['usercode'])  && !empty( $_SESSION['username']))
            {   
                $result = array();
                $res['returnUrl'] = '#'; 
            }
            else
            {
                $result = false;
                $res['returnUrl'] = 'Auth?param1=Login';  
            }
            break;
        case 'getStorecode':            
            $result =  explode ( "@", $_SESSION['usercode'] )[0];
            break;
        case 'addUser':              
            $result = AuthManage::addUser($_POST['usercode'], $_POST['userpsw'], 'Guest', $_POST['industry'], $_POST['fburl'], $_POST['fbname'], $_POST['tel']);
            
            break;
        case 'login':
            $result = AuthManage::login($_POST['usercode'], $_POST['userpsw']);
            if(is_array($result))
            {
                $res['returnUrl'] = './Dashboard?param1=Dashboard';
                $_SESSION['usercode'] = $result[0]['USERCODE'];
                $_SESSION['username'] = $result[0]['USERNAME'];
                $_SESSION['user_token'] = $result[0]['FBTOKEN'];
            }            
            break;
        case 'logout':
                $result = array();
                $res['returnUrl'] = './Auth?param1=Login';
                unset($_SESSION['usercode']);
                unset($_SESSION['username']);
                unset($_SESSION['user_token']);
            break;
        case 'getActSelectList':
            $result = ActivityManage::getActSelectList();
            break;
        case 'getActList':
            $result = ActivityManage::getStoreActList();
            break;
        case 'getActBannerList':
            $result = ActivityManage::getActBannerList($_POST);
            break;
        case 'addAct':
//            var_dump($_POST);
//            var_dump($_FILES);
//            exit();
            $actcode = ActivityManage::addActivity($_POST['acttitle'],$_POST['acttype'],$_POST['activityStart'],$_POST['activityFinish'],$_POST['activitydesp'],$_POST['redemptionStart'],$_POST['redemptionFinish'],$_POST['redempdesp'],$_POST['prizenum'],$_POST['noprize'],$_POST['actstatus']);
            
//            print_r($actcode);
            if(is_numeric($actcode))
            {
                $totalprize = (int)$_POST['prizenum'];
                if($totalprize == 0)
                {
                    $result = array();
                    $res['errmsg'] = '新增成功！';
                    break;
                }
                
               
                for($i=1;$i<=$totalprize;$i++)
                {                    
                    $dir_user = explode ( "@", $_SESSION['usercode'] );
                    $dir_act = "ACT" . $actcode;
                    $target = "figure/" . $dir_user[0] . "/" . $dir_act . "/";
                    $newfilename = 'Prize'. $i . date('YmdHis');
                    $oldfilename = $_FILES['prize-' . $i]['name'];
                    $filetype = $_FILES['prize-' . $i]['type'];
                    $filesize = $_FILES['prize-' . $i]['size'];
                    
                    $result = ActivityManage::addPrize($actcode, $_POST['prizename-' . $i], ((empty($_POST['showwintxt-' . $i]))?1:0), $_POST['prizenum-' . $i], $_POST['prizepercent-' . $i],$target , $newfilename, $filesize, $filetype, $oldfilename);
                    
                    if(!is_array($result))            
                    {
                        $res['errmsg'] = '活動獎品新增失敗！';
                    }  
                    else
                    {                
                        if (!file_exists("../figure/".$dir_user[0]) && !is_dir("../figure/".$dir_user[0])) {
                            mkdir("../figure/".$dir_user[0], 0777, true);
                        }
                        if (!file_exists("../figure/".$dir_user[0]. '/' .$dir_act) && !is_dir("../figure/".$dir_user[0]. '/' .$dir_act)) {
                            mkdir("../figure/".$dir_user[0]. '/' .$dir_act, 0777, true);
                        }
                        move_uploaded_file($_FILES['prize-' . $i]['tmp_name'], "../figure/".$dir_user[0]. '/' .$dir_act . '/' . $newfilename);
                        $result = array();
                        $res['errmsg'] = '新增成功！';
                    }
                }
            }
            else
            {
                $result = false;
                $res['errmsg'] = "活動新增失敗！";
            }
            break;
        case 'addActBanner':
//            var_dump($_POST);
//            var_dump($_FILES);
//            exit();
                if(ActivityManage::checkBannerRepeat($_POST['a_actname']))
                {
                    $result = false;
                    $res['errmsg'] = '同一活動請勿重複上傳Banner。';
                    break;
                }
            
                if(empty($_FILES['a_source']['tmp_name']))
                {
                    $result = false;
                    $res['errmsg'] = '請上傳活動Banner。';
                    break;
                }
                $dir_user = explode ( "@", $_SESSION['usercode'] );
                $dir_act = "ACT" . $_POST['a_actname'];
                $target = "figure/" . $dir_user[0] . "/" . $dir_act . "/Banner/";
                $newfilename = 'Banner' . date('YmdHis');
                $oldfilename = $_FILES['a_source']['name'];
                $filetype = $_FILES['a_source']['type'];
                $filesize = $_FILES['a_source']['size'];

                $result = ActivityManage::addactBanner($_POST['a_actname'], $target , $newfilename, $filesize, $filetype, $oldfilename);

                if(!is_array($result))            
                {
                    $res['errmsg'] = '活動Banner新增失敗！';
                }  
                else
                {                
                    if (!file_exists("../figure/".$dir_user[0]) && !is_dir("../figure/".$dir_user[0])) {
                        mkdir("../figure/".$dir_user[0], 0777, true);
                    }
                    if (!file_exists("../figure/".$dir_user[0]. '/' .$dir_act) && !is_dir("../figure/".$dir_user[0]. '/' .$dir_act)) {
                        mkdir("../figure/".$dir_user[0]. '/' .$dir_act, 0777, true);
                    }
                    if (!file_exists("../figure/".$dir_user[0]. '/' .$dir_act . '/Banner') && !is_dir("../figure/".$dir_user[0]. '/' .$dir_act . '/Banner')) {
                        mkdir("../figure/".$dir_user[0]. '/' .$dir_act . '/Banner', 0777, true);
                    }
                    move_uploaded_file($_FILES['a_source']['tmp_name'], "../figure/".$dir_user[0]. '/' .$dir_act . '/Banner/' . $newfilename);
                    $result = array();
                    $res['errmsg'] = '新增成功！';
                }
            break;
        case 'editActBanner':
//            var_dump($_POST);
//            var_dump($_FILES);
//            exit();
                if(empty($_FILES['e_source']['tmp_name']))
                {
                    $result = false;
                    $res['errmsg'] = '請上傳活動Banner。';
                    break;
                }
                $dir_user = explode ( "@", $_SESSION['usercode'] );
                $dir_act = "ACT" . $_POST['e_actser'];
                $target = "figure/" . $dir_user[0] . "/" . $dir_act . "/Banner/";
                $newfilename = 'Banner' . date('YmdHis');
                $oldfilename = $_FILES['e_source']['name'];
                $filetype = $_FILES['e_source']['type'];
                $filesize = $_FILES['e_source']['size'];

                $result = ActivityManage::editactBanner($_POST['e_ser'], $target, $newfilename, $filesize, $filetype, $oldfilename);

                if(!is_array($result))            
                {
                    $res['errmsg'] = '活動Bannert儲存失敗！';
                }  
                else
                {                
                    if (!file_exists("../figure/".$dir_user[0]) && !is_dir("../figure/".$dir_user[0])) {
                        mkdir("../figure/".$dir_user[0], 0777, true);
                    }
                    if (!file_exists("../figure/".$dir_user[0]. '/' .$dir_act) && !is_dir("../figure/".$dir_user[0]. '/' .$dir_act)) {
                        mkdir("../figure/".$dir_user[0]. '/' .$dir_act, 0777, true);
                    }
                    if (!file_exists("../figure/".$dir_user[0]. '/' .$dir_act . '/Banner') && !is_dir("../figure/".$dir_user[0]. '/' .$dir_act . '/Banner')) {
                        mkdir("../figure/".$dir_user[0]. '/' .$dir_act . '/Banner', 0777, true);
                    }
                    move_uploaded_file($_FILES['e_source']['tmp_name'], "../figure/".$dir_user[0]. '/' .$dir_act . '/Banner/' . $newfilename);
                    $result = array();
                    $res['errmsg'] = '儲存成功！';
                }
            break;
        case 'editAct':
            $httpcode = ActivityManage::editActivity($_POST['actid'], $_POST['e-acttitle'],$_POST['e-acttype'],$_POST['e-activityStart'],$_POST['e-activityFinish'],$_POST['e-activitydesp'],$_POST['e-redemptionStart'],$_POST['e-redemptionFinish'],$_POST['e-redempdesp'],$_POST['e-prizenum'],$_POST['e-noprize'],$_POST['e-actstatus']);
            
//            print_r($actcode);
            if(is_numeric($httpcode))
            {
                $totalprize = (int)$_POST['e-prizenum'];
                if($totalprize == 0)
                {
                    $result = array();
                    $res['errmsg'] = '儲存成功！';
                    break;
                }
                
                for($i=1;$i<=$totalprize;$i++)
                {                    
                    $dir_user = explode ( "@", $_SESSION['usercode'] );
                    $dir_act = "ACT" . $_POST['actid'];
                    $target = "figure/" . $dir_user[0] . "/" . $dir_act . "/";
                    $newfilename = 'Prize'. $i . date('YmdHis');
                    $oldfilename = $_FILES['prize-' . $i]['name'];
                    $filetype = $_FILES['prize-' . $i]['type'];
                    $filesize = $_FILES['prize-' . $i]['size'];
                    
                    if(isset($_POST['prizeid-1']))
                    {
//                        var_dump($_FILES['prize-' . $i]);
                        if(!empty($_FILES['prize-' . $i]['tmp_name']))
                        {
                            $result = ActivityManage::editPrize($_POST['prizeid-' . $i], $_POST['actid'], $_POST['prizename-' . $i], ((empty($_POST['showwintxt-' . $i]))?1:0), $_POST['prizenum-' . $i], $_POST['prizepercent-' . $i],$target , $newfilename, $filesize, $filetype, $oldfilename);

                            move_uploaded_file($_FILES['prize-' . $i]['tmp_name'], "../figure/".$dir_user[0]. '/' .$dir_act . '/' . $newfilename);
                            $result = array();
                        }
                        else
                        {
                            $result = ActivityManage::editPrizewithoutFile($_POST['prizeid-' . $i], $_POST['actid'], $_POST['prizename-' . $i], ((empty($_POST['showwintxt-' . $i]))?1:0), $_POST['prizenum-' . $i], $_POST['prizepercent-' . $i]);
                        }


                        if(!is_array($result))            
                        {
                            $res['errmsg'] = '活動獎品更新失敗！';
                        }  
                        else
                        {     
                            $res['errmsg'] = '儲存成功！';
                        }
                    }
                    else
                    {
                        $result = ActivityManage::addPrize($_POST['actid'], $_POST['prizename-' . $i], ((empty($_POST['showwintxt-' . $i]))?1:0), $_POST['prizenum-' . $i], $_POST['prizepercent-' . $i],$target , $newfilename, $filesize, $filetype, $oldfilename);
                    
                        if(!is_array($result))            
                        {
                            $res['errmsg'] = '活動獎品新增失敗！';
                        }  
                        else
                        {                
                            if (!file_exists("../figure/".$dir_user[0]) && !is_dir("../figure/".$dir_user[0])) {
                                mkdir("../figure/".$dir_user[0], 0777, true);
                            }
                            if (!file_exists("../figure/".$dir_user[0]. '/' .$dir_act) && !is_dir("../figure/".$dir_user[0]. '/' .$dir_act)) {
                                mkdir("../figure/".$dir_user[0]. '/' .$dir_act, 0777, true);
                            }
                            move_uploaded_file($_FILES['prize-' . $i]['tmp_name'], "../figure/".$dir_user[0]. '/' .$dir_act . '/' . $newfilename);
                            $result = array();
                            $res['errmsg'] = '新增成功！';
                        }  
                    }
                    
                    
                }
            }
            else
            {
                $result = false;
                $res['errmsg'] = "活動儲存失敗！";
            }
            break;
        case 'deleteAct':
            $result = ActivityManage::deleteActivity($_POST['ser']);
            
            if(is_array($result))
            {
                $dir_user = explode ( "@", $_SESSION['usercode'] );
                $dir_act = "ACT" . $_POST['ser'];
                removeActFiles("../figure/".$dir_user[0]. '/' .$dir_act );
            }
            break;
        case 'deleteActBanner':
            $result = ActivityManage::deleteActBanner($_POST['ser']);
            
            if(is_array($result))
            {
                $dir_user = explode ( "@", $_SESSION['usercode'] );
                $dir_act = "ACT" . $_POST['s_ser'];
                removeActFiles("../figure/".$dir_user[0]. '/' .$dir_act . '/Banner' );
            }
            break;
        case 'duplicateAct':
            $actcode = ActivityManage::copyPrizeActHead($_POST['ser']);
            
//            print_r($actcode);
//            exit();
            if(is_numeric($actcode))
            {
                ActivityManage::copyPrizeActBody($actcode, $_POST['ser']);
                ActivityManage::copyActBanner($actcode, $_POST['ser']);
               
                $dir_user = explode ( "@", $_SESSION['usercode'] );
                $newpath = "../figure/".$dir_user[0]. '/' ."ACT" . $actcode;
                $oldpath = "../figure/".$dir_user[0]. '/' ."ACT" . $_POST['ser'];
                if(copy_r($oldpath, $newpath))
                {
                    $result = array();
                    $res['errmsg'] = '複製成功！';
                }
                else
                {
                    $result = false;
                    $res['errmsg'] = "複製失敗！";
                }
                
            }
            else
            {
                $result = false;
                $res['errmsg'] = "複製失敗！";
            }
            break;
        case 'loadAct':
            $result = ActivityManage::loadActivity($_POST['ser']);            
            break;
        case 'loadActBanner':
            $result = ActivityManage::loadActBanner($_POST['ser']);            
            break;
        case "loadscratch":
            //檢查是否已抽過
            $checkresult = ActivityManage::checkScratchLog($_POST);
            if($checkresult[0]['REDEEMDATE'])
            {
                $result = $checkresult;
                $res['errmsg'] = "./FBGame?param1=FBGameOver";
                $res['errcode'] = "03";
                break;
            }
            if(is_array($checkresult))
            {
                $result = $checkresult;
                $res['errmsg'] = "您已經刮過囉！中獎者請記得在兌換期間內完成獎品兌換喔！";
                $res['errcode'] = "01";
                
                //兌獎期間判斷
                if(!empty($result[0]['AWARDSTARTDATE']) && time()<strtotime($result[0]['AWARDSTARTDATE'])){
                    $res['suberrmsg'] = "兌獎活動尚未開始,開始時間為:" . $result[0]['AWARDSTARTDATE'];
                    $res['suberrcode'] = "021";
                }

                if(!empty($result[0]['AWARDENDDATE']) && time()>strtotime($result[0]['AWARDENDDATE'])){
                    
                    $res['suberrmsg'] = "本次兌獎活動已經結束,結束時間為:" . $result[0]['AWARDENDDATE'];
                    $res['suberrcode'] = "022";
                }
                
                break;                
            }
            $result = ActivityManage::loadActivity($_POST['param']['actser']);
            $actstartdate = $result[0]['ACTSTARTDATE'];
            $actenddate = $result[0]['ACTENDDATE'];
            $awardstartdate = $result[0]['AWARDSTARTDATE'];
            $awardenddate = $result[0]['AWARDENDDATE'];
            
           
             if(!empty($result[0]['ACTSTARTDATE']) && time()<strtotime($result[0]['ACTSTARTDATE'])){
//                $result = $result;
                $res['errmsg'] = "活動尚未開始,開始時間為:" . $actstartdate;
                $res['errcode'] = "02";
                break;
            }
            
            if(!empty($result[0]['ACTENDDATE']) && time()>strtotime($result[0]['ACTENDDATE'])){
//                $result = $result;
                $res['errmsg'] = "本次活動已經結束,結束時間為:" . $actenddate;
                $res['errcode'] = "02";
                
                
                //兌獎期間判斷
                if(!empty($result[0]['AWARDSTARTDATE']) && time()<strtotime($result[0]['AWARDSTARTDATE'])){
                    $res['suberrmsg'] = "兌獎活動尚未開始,開始時間為:" . $awardstartdate;
                    $res['suberrcode'] = "021";
                }

                if(!empty($result[0]['AWARDENDDATE']) && time()>strtotime($result[0]['AWARDENDDATE'])){
                    
                    $res['suberrmsg'] = "本次兌獎活動已經結束,結束時間為:" . $awardenddate;
                    $res['suberrcode'] = "022";
                }
                
                break;
            }
            
            
            foreach($result as $key=>$val)
            {
                $arr[$val['prizeSER']] = $val['PERCENT'];
            }
            
            $win = get_prizerand($arr);
            $i=0;
            //獎品數量等於零，重新rand
            while(!check_prizesum($win))
            {
                $i++;
                $win = get_prizerand($arr);
                if($i == 10)break;
            }
            
            $result = ActivityManage::getScratch($_POST['param']['actser'], $win);
//            echo $win. ",num:" . check_prizesum($win)[0]['CURTOTALS']  . "\n";
//            echo var_dump(check_prizesum($win));
            
//            echo var_dump($arr);
//            exit();
            break;
        case 'addScratchLog':              
            $result = ActivityManage::addScratchlog($_POST);
            if(is_numeric($result))
            {
                $res['msg'] = $result;
                $result = array();
            }
            break;
        case 'getLoginURL': 
            if(!session_id()) {
                session_start();
            }
            require_once "core/FBconfig.php";
            $permissions = ['email','manage_pages','pages_show_list','publish_pages','pages_messaging','pages_messaging_subscriptions'];
            $fbpermissionlink = $helper->getLoginUrl('https://demo5.biz/route/controller.php?action=FBcallBack', $permissions);
            $result = array();
            $result['url'] = $fbpermissionlink;
//            echo var_dump($result);
//            exit();
            break;
        case "FBcallBack":
            if(!session_id()) {
                session_start();
            }

            require_once "core/FBconfig.php";
             header("Content-Type:text/html; charset=utf-8");
             $helper = $fb->getRedirectLoginHelper();

            if (isset($_GET['state'])) {
                $helper->getPersistentDataHandler()->set('state', $_GET['state']);
            }

            //取得使用者的access_token
            try{
                $accessToken = $helper->getAccessToken();
            }catch(Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            }catch(Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }
            
            FBManage::updateFBtoken((string) $accessToken, $_SESSION['usercode']);
            $_SESSION['user_token'] = (string) $accessToken; //存取使用者的accessToken
            $temp = array();
            if(isset($_SESSION['user_token']) && !empty($_SESSION['user_token'])){
                header("Location: ../Fanspages?param1=list");//成功，粉絲頁列表             
            }else{
                header("Location: ../Fanspages?param1=index"); //未獲取user_token
            }

            break;
        case "unConnectFB":
            if(!session_id()) {
                session_start();
            }
            $result = FBManage::unconnectfb($_SESSION['usercode']);
            if(is_array($result))
            {
                $res['errmsg'] = 'Fanspages?param1=index';
            }
            else
            {
                $res['errmsg'] = '#';
            }            
            unset($_SESSION['user_token']);
            break;
        case "getFanspages":
            if(!session_id()) {
                session_start();
            }
            require_once "core/FBconfig.php";
            
            
            try{
                $res = $fb->get('me/accounts', $_SESSION['user_token']);
            }catch(\Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            }catch(\Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }
            $res = $res->getDecodedBody();
            $name_arr = array();
            //取得選擇的紛絲團資料
            foreach($res['data'] as $page){
                array_push($name_arr,$page['name']);
            }
//            echo var_dump($res);
//            exit();
            $result = $res;
            break;
        case "post2Fanspage":
            header("Content-Type:text/html; charset=utf-8");
            require "core/FBconfig.php";
            $id = $_POST['id'];
            $name = $_POST['name'];
            $accestoken = $_POST['accesscode'];
            $message = $_POST['message'];

            $data = array(
                'message' => $message
            );
            
            try{
                $res = $fb->get('/me/accounts', $_SESSION['user_token']);
            }catch(\Facebook\Exceptions\FacebookResponseException $e) {
                $res['errmsg'] = "發文失敗，Graph returned an error: " . $e->getMessage();
                $result = false;
//                exit;
            }catch(\Facebook\Exceptions\FacebookSDKException $e) {
                $res['errmsg'] = "發文失敗，Facebook SDK returned an error: " . $e->getMessage();
                $result = false;
//                exit;
            }
            $res = $res->getDecodedBody();

            //取得選擇的紛絲團資料
            foreach($res['data'] as $page){
                if($page['name'] == $name){
                    $id = $page['id'];
                    $accesstoken = $page['access_token'];
                }
            }
            
            //發文
            try{
                $resfeed = $fb->post($id.'/feed/', $data, $accestoken);
                $res['errmsg'] = "發文成功";
                $result = array();
            }catch(\Facebook\Exceptions\FacebookResponseException $e) {
                $res['errmsg'] = "發文失敗，Graph returned an error: " . $e->getMessage();
                $result = false;
//                exit;
            }catch(\Facebook\Exceptions\FacebookSDKException $e) {
                $res['errmsg'] = "發文失敗，Facebook SDK returned an error: " . $e->getMessage();
                $result = false;
//                exit;
            }
            break;
        case "loadArticlemsg":
            require_once "core/FBconfig.php";
            
//            $storecode = ActivityManage::getStorecode($_SESSION['usercode']);
//            FBManage::updateFanspageToken($storecode, $_POST['id'], $_POST['accode']);
            
            //粉絲團id, token
            $id = $_POST['id'];
            $accesstoken = $_POST['accode'];
            
            //取得粉絲團文章
            try{
                $res = $fb->get($id.'/feed', $accesstoken);
            }catch(\Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            }catch(\Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }
            $res = $res->getDecodedBody();
            $post_data = array();
            $post_id = array();
            //取得選擇的紛絲團資料
            foreach($res['data'] as $page){
                if(isset($page['story'])){
                    array_push($post_data,$page['story']);
                    array_push($post_id,$page['id']);
                }else if(isset($page['message'])){
                    array_push($post_data,$page);
                    array_push($post_id,$page['id']);
                }else{
                    continue;
                }
            }
            
            $result = array('postArticleID' => $post_id, 'postArticleData' => $post_data);
            break;
        case "loadLeavemsg":
            require_once "core/FBconfig.php";
            
//            文章id, 粉絲團token
            $postid = $_POST['articleid'];
            $posttoken = $_POST['pageaccode'];

            try {
            // Returns a `FacebookFacebookResponse` object
            $res = $fb->get($postid.'/comments?order=reverse_chronological',$posttoken);
            } catch(FacebookExceptionsFacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch(FacebookExceptionsFacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }
            $res = $res->getDecodedBody();
            $post_message = array();
            $post_message_sec = array();
            $add_id = array();
//            foreach($res['data'] as $page){
//                array_push($post_message,$page['message']);
//                array_push($add_id,$page['id']);
//                
//                $res_sec = $fb->get($page['id'].'/comments?order=reverse_chronological',$posttoken)->getDecodedBody();
//                if(is_array($res_sec))
//                {
//                    
//                }
//            }
            
            for($i=0; $i<count($res['data']); $i++)
            {
                array_push($post_message,$res['data'][$i]['message']);
                array_push($add_id,$res['data'][$i]['id']);
                
                $res_sec = $fb->get($res['data'][$i]['id'].'/comments?order=reverse_chronological',$posttoken)->getDecodedBody();
                
                $res['data'][$i]['sec_msg'] = ((is_array($res_sec))?$res_sec:null);
            }
            
            
            $result = array('postLeavemsgID' => $add_id, 'postLeavemsgData' => $res['data']);
            break;
        case 'replayLeavemsg':
            require_once "core/FBconfig.php";
            
//            留言id, 粉絲團token, 訊息message
            $posttoken = $_POST['pageaccode'];
            $postid = $_POST['leavemsgid'];
            $message = $_POST['message'];
            
            if(!isset($_POST['message']) || empty($_POST['message']))
            {
                $result = false;
                break;
            }
            
            $data = array(
                'message' => $message
            );
            
            try {
                $resdata = $fb->post($postid.'/comments',$data,$posttoken);
                $res['errmsg'] = "回覆成功！";
                $result = array();
            } catch(FacebookExceptionsFacebookResponseException $e) {
                $res['errmsg'] = 'Graph returned an error: ' . $e->getMessage();
                $result = false;
            }catch(FacebookExceptionsFacebookSDKException $e) {
                $res['errmsg'] = 'Facebook SDK returned an error: ' . $e->getMessage();
                $result = false;
            }
            
            break;
        case 'addfanspage':
            $storecode = ActivityManage::getStorecode($_SESSION['usercode'])[0]['SER'];
            
            $res_s = FBManage::addFanspage($storecode, $_POST['pageid'], $_POST['pagetoken'], $_POST['articleid'], $_POST['keyword'], $_POST['comment'], $_POST['message'],$_POST['msgkeyword'],$_POST['callback'], $_POST['actid']);
            
            if(is_numeric($res_s))
            {
                //訂閱粉絲專頁事件的 Webhook
                $response = [
                    'subscribed_fields' => 'feed'
                ];
                $ch = curl_init('https://graph.facebook.com/v3.2/' . $_POST['pageid'] . '/subscribed_apps?access_token=' . $_POST['pagetoken']);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1); // 不直接出現回傳值
                curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);

                $response = curl_exec($ch); 
                curl_close($ch);
                
                $result = array();
                $res['errmsg'] = '設定成功！';
                
            }
            else
            {
                $result = false;
                $res['errmsg'] = '設定失敗！';
            }
            
            break;
        case 'loadfanspage':
            $result = FBManage::loadFanspage($_POST['pageid'], $_POST['articleid']);
            break;
        case 'editfanspage':
            $storecode = ActivityManage::getStorecode($_SESSION['usercode'])[0]['SER'];
            
            $result = FBManage::updateFanspage($storecode, $_POST['pageid'], $_POST['pagetoken'], $_POST['articleid'], $_POST['keyword'], $_POST['comment'], $_POST['message'],$_POST['msgkeyword'],$_POST['callback'], $_POST['actid']);
            
            if(!is_array($result))
            {
                $res['errmsg'] = '設定失敗！';
                $result = false;
            }
            else
            {
                $res['errmsg'] = '設定成功！';
                $result = array();
            }
            break;
        case 'bot':
            if( $_REQUEST['hub_challenge']){
                echo $_REQUEST['hub_challenge'];
                }
            
//            $raw = file_get_contents('php://input');
//            $handle1 = fopen('test.txt','w');
//            fwrite($handle1,$raw);
//            fclose($handle1);
//            $data = json_decode($raw, true);
//
//            if( $data['object'] == 'page'){
//                foreach($data['entry'] as $entry)
//                {
//                    foreach($entry['messaging'] as $messaging)
//                    {
//                        if(!empty($messaging['message']['text'])){
//                            $data['messaging_type'] = "RESPONSE";
//                            $data['recipient'] = array('id' => $messaging['sender']['id']); 
//                            $data['message'] = array('text' => 'Text received, echo: '.$messaging['message']['text']);
//                            echo json_encode($data)."\n";
//                        }
//                    }
//                }
//            }
//            
//            
//            exit();
            $input = json_decode(file_get_contents('php://input'),true);
            $hubVerifyToken = 'scratch';
            $pageid = $input['entry'][0]['id'];
            
            $feedData1 = file_get_contents("php://input");
            $handle1 = fopen('test.txt','w');
            fwrite($handle1,$feedData1);
            fclose($handle1);
            
            if(isset($input['entry'][0]['messaging'][0]['message']['text'])) //從message傳來
            {
                $feedData1 = file_get_contents("php://input");
                $handle1 = fopen('test3.txt','w');
                fwrite($handle1,$feedData1);
                fclose($handle1);
                
                $senderId = $input['entry'][0]['messaging'][0]['sender']['id'];
                $messageText = $input['entry'][0]['messaging'][0]['message']['text'];
                $accessToken = FBManage::loadFanspagePI($pageid)[0]['PAGETOKEN'];
                $getlog = FBManage::getleavemsgLog($senderId, $pageid);
                
                $result = FBManage::loadFanspage($pageid, $getlog[0]['ARTICLEID']);
                
                $link_resp = getActivityLink($messageText, $result[0]['ACTID'], $senderId, $result[0]['MSGKEYWORD']);
                $response = null;
                $response = [
                    'recipient' => [ 'id' => $senderId ],
                    'message' => [ 'text' => str_replace('{{page_link}}', $link_resp ,$result[0]['CALLBACK'])]
                ];
                
                if(isset($result[0]['CALLBACK'])) //有設定才發送callback
                {
                    $ch = curl_init('https://graph.facebook.com/v3.2/me/messages?access_token='.$accessToken);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
                    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                    curl_exec($ch);
                    curl_close($ch);
                }
            }
            else //從文章留言傳來
            {
                if($input['entry'][0]['changes'][0]['value']['from']['id'] == $pageid)
                {
                    exit();
                }
                
                $articleid = $input['entry'][0]['changes'][0]['value']['post_id'];
                $result = FBManage::loadFanspage($pageid, $articleid);
                setcookie("post_id", $articleid, time()+3600);

                if(is_array($result))
                {
                    $accessToken = $result[0]['PAGETOKEN'];

                    $handle = fopen('test2.txt','w');
                    fwrite($handle,"access controller...");

                    $feedData = file_get_contents("php://input");
                    fwrite($handle,$feedData);
                    fclose($handle);

                    require_once "core/FBconfig.php";
                    $senderfeedId = $input['entry'][0]['changes'][0]['value']['comment_id'];
                    $sendername = $input['entry'][0]['changes'][0]['value']['from']['name'];
                    $commentmsg = $input['entry'][0]['changes'][0]['value']['message'];
                    $senderId = $input['entry'][0]['changes'][0]['value']['from']['id'];
                    $response = null;
                    FBManage::leavemsgLog($senderId, $pageid, $articleid, $result[0]['ACTID']);
                    //Comment 文章留言

        //            留言id, 粉絲團token, 訊息message
                    $posttoken = $accessToken;
                    $postid = $senderfeedId;
                    $message = str_replace('{{sender_name}}',$sendername,$result[0]['COMMENT']);
                    
                    $data = array(
                        'message' => $message
                    );
                    if(isset($result[0]['COMMENT'])) //有設定才發送comment
                    {
                        try {
                            $resdata = $fb->post($postid.'/comments',$data,$posttoken);

                        } catch(FacebookExceptionsFacebookResponseException $e) {
                            exit;
                        }catch(FacebookExceptionsFacebookSDKException $e) {
                            exit;
                        }
                    }
                    
                    //Message  留言後第一次message用戶，於message輸入關鍵字回傳
                    $response1 = [
                        'message' => 
                        str_replace('{{sender_name}}',$sendername,$result[0]['MESSAGE'])
                    ];
                    
                    if(isset($result[0]['MESSAGE'])) //有設定才發送message
                    {
                        $ch1 = curl_init('https://graph.facebook.com/v3.2/'.$senderfeedId.'/private_replies?access_token='.$posttoken);
                        curl_setopt($ch1, CURLOPT_POST, 1);
                        curl_setopt($ch1, CURLOPT_POSTFIELDS, json_encode($response1));
                        curl_setopt($ch1, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                        curl_exec($ch1);
                        curl_close($ch1);
                    }
                    
                }
            }
            
//            unset($_SESSION['articleid']);
            return 200;
            exit();
            break;
        case 'logoSet':
            $dir_user = explode ( "@", $_SESSION['usercode'] );
            $target = "../figure/" . $dir_user[0] . "/";
            $newfilename = 'logo';            
            
            if($_FILES['logoimginput'])
            {
                if($_FILES['logoimginput']['size'] > 1000000)
                {
                    $res['errmsg'] = "檔案大小不可以大於1M喔！";
                    $result = false;
                }
                else
                {
                    if (!file_exists("../figure/".$dir_user[0]) && !is_dir("../figure/".$dir_user[0])) {
                        mkdir("../figure/".$dir_user[0], 0777, true);
                    }
                    move_uploaded_file($_FILES['logoimginput']['tmp_name'], $target . $newfilename);
                    $res['errmsg'] = '圖片上傳成功！';
                    $result = array();
                }
            }
            else
            {
                $res['errmsg'] = "請重新上傳圖片！";
                $result = false;
            }
            
            
            break;
        case "getSupplyList":
            $result = SupplyManage::getSupplyList();
            break;
        case "addSupply":
            $result = SupplyManage::addSupply($_POST);
            break;
        case "loadSupply":
            $result = SupplyManage::loadSupply($_POST);
            break;
        case "loadLocalSupply":
            $result = SupplyManage::loadLocalSupply();
            break;
        case "editSupply":
            $result = SupplyManage::editSupply($_POST);
            break;
        case "editLocalSupply":
            $result = SupplyManage::editLocalSupply($_POST);
            break;
        case "deleteSupply":
            $result = SupplyManage::deleteSupply($_POST);
            break;
        case "exchangePrize":
            if($_POST['actype'] == "0")
            {
                if(ActivityManage::checkStorePsd($_POST))
                {
                    $result = ActivityManage::exchangePrize($_POST);
                    if(is_array($result))
                    {
                        $res['errmsg'] = "./FBGame?param1=FBGameOver";
                    }
                }
                else
                {
                    $result = false;
                    $res['errmsg'] = "兌換密碼輸入錯誤！";
                }
            }
            else
            {
                $result = ActivityManage::exchangePrize($_POST);
                if(is_array($result))
                {
                    $res['errmsg'] = "./FBGame?param1=FBGameOver";
                }
                else
                {
                    $res['errmsg'] = "兌換動作未成功！";
                }
            }
            
            
            break;
        case "searchAwardLogs":
            $result = ActivityManage::awardLogList($_POST);
            break;
        case "loadAwardLog":
            $result = ActivityManage::loadAwardLog($_POST['ser']);
            break;
        case "loadScratchsd":
            $result = ActivityManage::loadScratchstoredata($_POST['ser']);
            break;
        default:
            # code...
            break;
        
    }
    if(!is_array($result))
    {
        $res["message"] = "failed";
        $res["data"] = $result;
    }
    else
    {
        $res["message"] = "success";
        $res["data"] = $result;
    }

    echo json_encode($res);
}

//base
function iniread($filename) 
{
    // $filename = __pageroot . __appsurl . $_REQUEST["para"];
    try
    {
        if ( file_exists($filename) )
        {
           $result = parse_ini_file($filename, true);
        }
        else 
        {
           $result = _error_message("general", "0003", "", $filename . " not found.");             //檔案不存在
        }

        $result = array("data" => $result );
        return $result;
    }catch(Exception $e)
    {
        echo $e->errorInfo();
    }

    
}
function removeActFiles($dir) {
    error_reporting(0);
    foreach (glob($dir) as $file) {
        if (is_dir($file)) { 
            removeActFiles("$file/*");
            rmdir($file);
        } else {
            unlink($file);
        }
    }
}
function get_prizerand($probability) {
    // 概率數組的總概率精度
    $max = array_sum($probability);
    foreach ($probability as $key => $val) {
        $rand_number = mt_rand(1, $max);//從1到max中隨機一個值

        if ($rand_number <= $val) {//如果這個值小於等於當前中獎項的概率，我們就認為已經中獎
            return $key;
        } else {
            $max -= $val;//否則max減去當前中獎項的概率，然後繼續參與運算
        }
    }
}
function check_prizesum($prizeser)
{
    $cursum = ActivityManage::getPrizeSum($prizeser);
    if((int)$cursum[0]['CURTOTALS'] == 0)
    {
        return false;
    }
    else
    {
        return true;
    }
    
}
//複製檔案
function copy_r( $path, $dest )
{
    if( is_dir($path) )
    {
        mkdir($dest, 0777, true);
        $objects = scandir($path);
        if( sizeof($objects) > 0 )
        {
            foreach( $objects as $file )
            {
                if( $file == "." || $file == ".." )
                    continue;
                // go on
                if( is_dir( $path.DS.$file ) )
                {
                    copy_r( $path.DS.$file, $dest.DS.$file );
                }
                else
                {
                    copy( $path.DS.$file, $dest.DS.$file );
                }
            }
        }
        return true;
    }
    elseif( is_file($path) )
    {
        return copy($path, $dest);
    }
    else
    {
        return false;
    }
}
function getActivityLink($leavecomment, $actid, $senderid, $keyword)
{
    $webdir = 'https://demo5.biz/';
    $href = 'FBGame?param1=FBGame&actser=' . $actid . '&timestamp=' . time() . '&UID=' . $senderid;
    
    $keyword_ary = explode(",",$keyword);
    $match = false;
    
    for($i=0; $i<count($keyword_ary); $i++)
    {
        if(is_numeric(strpos($leavecomment,$keyword_ary[$i])))
        {
            $match = true;
        }
    }
    
    return (($match)?($webdir . $href):null);
    
    
}
?>
